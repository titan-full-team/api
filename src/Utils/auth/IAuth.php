<?php

namespace Titan\Utils\auth;

use Nextras\Orm\Entity\IEntity;

interface IAuth
{
    /**
     * @param array $data
     * @param string|null $tokenExpiration
     * @return string
     */
    public function login(array $data, $tokenExpiration = '+1 day'): string;

    /**
     * @param IEntity $user
     * @param array $data
     * @param bool $autoLogin
     * @param string $tokenExpiration
     * @return mixed
     */
    public function register(IEntity $user, array $data, bool $autoLogin = true, $tokenExpiration = '+1 day');

    /**
     * @param IEntity $account
     * @param array $data
     * @return string
     */
    public function changeCredentials(IEntity $account, array $data): string;

    /**
     * @param IEntity $account
     * @param bool $validateOldToken
     * @param string $newToken
     * @param string $oldToken
     * @return string
     */
    public function setCredentials(
        IEntity $account,
        bool $validateOldToken,
        string $newToken,
        string $oldToken = null
    ): string;
}
