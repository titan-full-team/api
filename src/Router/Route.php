<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 08/11/2018
 * Time: 11:33 PM
 */

namespace Titan\Router;

use Nette\PhpGenerator\Method;
use Titan\Generator\Annotation;
use Titan\Utils\Strings;

class Route
{
    /**
     * @var Method
     */
    protected Method $method;

    /**
     * @var string
     */
    protected string $presenterName;

    /**
     * @param Method $method
     * @param string $presenterName
     */
    public function __construct(Method $method, string $presenterName)
    {
        $this->method = $method;
        $this->presenterName = $presenterName;
    }

    /**
     * @return null
     * @throws \Exception
     */
    public function getMask()
    {
        if (str_starts_with($this->method->getName(), 'action')
            && $this->method->getVisibility() === 'public'
        ) {
            // custom route definition
            $route = Annotation::getValue($this->method->getComment(), 'route');
            if ($route !== null) {
                return $route;
            }

            $action = $this->getAction();
            $presenter = preg_replace('/[A-Z]/', ' $0', $this->getPresenterName());
            $mask = Strings::webalize($presenter) . '/';
            $mask .= is_null($action) ? '' : $action . '/';
            $mask .= $this->getMaskParameters($this->method->getParameters());
            return substr($mask, -1) === '/' ? substr($mask, 0, -1) : $mask;
        }
        return null;
    }

    /**
     * @return string
     */
    public function getActionName(): string
    {
        return substr($this->method->getName(), strlen('action'));
    }

    /**
     * @return string
     */
    public function getPresenterName(): string
    {
        return substr($this->presenterName, 0, strlen('Presenter') * -1);
    }

    /**
     * @return null|string
     * @throws \Exception
     */
    protected function getAction()
    {
        $action = Strings::webalize(preg_replace('/[A-Z]/', ' $0', $this->getActionName()));
        $methods = $this->getMethods();
        if (
            ($action === 'detail' && sizeof($methods) === 1 && strtoupper($methods[0]) === 'GET')
            || ($action === 'create' && sizeof($methods) === 1 && strtoupper($methods[0]) === 'POST')
            || ($action === 'update' && sizeof($methods) === 1 && strtoupper($methods[0]) === 'PUT')
            || ($action === 'delete' && sizeof($methods) === 1 && strtoupper($methods[0]) === 'DELETE')
        ) {
            return null;
        }
        return $action;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getMethods(): array
    {
        $methods = Annotation::getValue($this->method->getComment(), 'methods');
        $exploded = is_null($methods) ? [] : explode(',', strtoupper($methods));

        if (sizeof($exploded) === 0) {
            throw new \Exception(
                'Add `@methods <method>` annotation to action' . $this->method->getName() . ' in '.$this->presenterName.'Presenter',
                501
            );
        }
        return $exploded;
    }

    /**
     * @param array $parameters
     * @return string
     * @throws \Exception
     */
    protected function getMaskParameters(array $parameters): string
    {
        $required = [];
        $optional = [];

        /** @var \Nette\PhpGenerator\Parameter $parameter */
        foreach ($parameters as $parameter) {
            if ($parameter->hasDefaultValue()) {
                $optional[] = $this->getParameterMask($parameter);
            } else if (!empty($optional)) {
                $required = array_merge($required, $optional);
                $required[] = $this->getParameterMask($parameter);
                $optional = [];
            } else {
                $required[] = $this->getParameterMask($parameter);
            }
        }

        $mask = implode('/', $required);
        if (!empty($optional)) {
            $mask .= empty($mask) ? '' : '/';
            $mask .= '[' . implode('][/', $optional) . ']';
        }
        return $mask;
    }

    /**
     * @param \Nette\PhpGenerator\Parameter $parameter
     * @return string
     */
    protected function getParameterMask(\Nette\PhpGenerator\Parameter $parameter): string
    {
        $out = '<' . $parameter->getName();

        // Types like int, string only, ...
        if ($parameter->getType() === 'int') {
            // todo: add type validation
        }

        // default values
        if ($parameter->hasDefaultValue() === true && $parameter->getDefaultValue() !== null) {
            $out .= '=' . $parameter->getDefaultValue();
        }
        return $out . '>';

    }
}