<?php

namespace Titan\Utils;

class Paginator
{
    /**
     * @var \Nette\Utils\Paginator
     */
    protected $paginator;

    public function __construct(int $page, int $perPage, int $totalCount)
    {
        $this->paginator = new \Nette\Utils\Paginator();
        $this->paginator->setPage($page);
        $this->paginator->setItemsPerPage($perPage);
        $this->paginator->setItemCount($totalCount);
        return $this;
    }

    /**
     * @return array
     */
    public function getPaginatorData(): array
    {
        return [
            'pages' => $this->paginator->getPageCount(),
            'page' => $this->paginator->getPage(),
            'items' => $this->paginator->getItemCount(),
            'perPage' => $this->paginator->getItemsPerPage(),
        ];
    }

    /**
     * @return \Nette\Utils\Paginator
     */
    public function getPaginator(): \Nette\Utils\Paginator
    {
        return $this->paginator;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->paginator->getLength();
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->paginator->getOffset();
    }
}