<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 04/11/2018
 * Time: 10:45 AM
 */

namespace Titan\Generator\Swagger;

use Nextras\Orm\Model\IModel;
use Titan\Generator\Swagger\Entity\EntityReadSchema;
use Titan\Generator\Swagger\Entity\EntityWriteSchema;

class Model
{
    protected IModel $model;

    protected array $requiredModels = [];

    public function __construct(IModel $model, array $requiredModels = [])
    {
        $this->model = $model;
        $this->requiredModels = $requiredModels;
    }

    /**
     * @throws \Exception
     */
    public function generate(): array
    {
        if ($this->hasNotGeneratedEntity() === true) {
            /** @var EntityReadSchema $entity */
            foreach ($this->requiredModels as $i => $entity) {
                if ($entity->isGenerated() === false) {
                    $entity->generate($this->model);
                    $this->requiredModels[$i] = $entity;
                    $this->addDependencies($entity->getDependencies());
                }
            }
            $this->generate();
        }
        return $this->getSwaggerData();
    }

    protected function getSwaggerData(): array
    {
        $out = [];
        /** @var EntityReadSchema $entity */
        foreach ($this->requiredModels as $entity) {
            $out[$entity->getEntityName()] = $entity->getData();
        }
        $out[Swagger::PAGINATOR_MODEL_NAME] = $this->getPaginatorSchema();
        $out[Swagger::MESSAGE_MODEL_NAME] = $this->getMessageSchema();
        return $out;
    }

    protected function getMessageSchema(): array
    {
        return [
            'type' => 'object',
            'required' => ['status'],
            'properties' => [
                'status' => ['type' => 'string'],
            ]
        ];
    }

    protected function getPaginatorSchema(): array
    {
        return [
            'type' => 'object',
            'required' => [],
            'properties' => [
                'pages' => ['type' => 'integer'],
                'page' => ['type' => 'integer'],
                'items' => ['type' => 'integer'],
                'perPage' => ['type' => 'integer'],
            ]
        ];
    }

    protected function hasNotGeneratedEntity(): bool
    {
        /** @var EntityReadSchema $model */
        foreach ($this->requiredModels as $model) {
            if ($model->isGenerated() === false) {
                return true;
            }
        }
        return false;
    }

    protected function addDependencies(array $dependencies): void
    {
        /** @var EntityReadSchema $dependency */
        foreach ($dependencies as $dependency) {
            if ($this->isSetDependency($dependency) === false) {
                $this->requiredModels[] = $dependency;
            }
        }
    }

    protected function isSetDependency(EntityReadSchema|EntityWriteSchema $entity): bool
    {
        foreach ($this->requiredModels as $model) {
            if ($entity->getIdentificator() === $model->getIdentificator()) {
                return true;
            }
        }
        return false;
    }
}