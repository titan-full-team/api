<?php

namespace Titan\Utils\Input;

use Nextras\Dbal\Utils\DateTimeImmutable;

class InputDateTime implements IInput
{
    use InputTrait;

    protected function __construct()
    {
        $this->type = 'datetime';
    }

    public function get()
    {
        try {
            return $this->hasValue === true ? new DateTimeImmutable($this->value) : $this->default;
        } catch (\Exception) {
            $this->throwException('invalid date format');
        }
    }
}