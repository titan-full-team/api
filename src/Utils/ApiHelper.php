<?php

namespace Titan\Utils;

use Nette\Application\BadRequestException;
use Nextras\Dbal\Drivers\Exception\UniqueConstraintViolationException;
use Nextras\Orm\Collection\DbalCollection;
use Nextras\Orm\Collection\ICollection;
use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Entity\IEntity;
use Nextras\Orm\Exception\InvalidArgumentException;
use Titan\Model\Helper;
use Titan\Model\Orm\ITitanCollection;
use Titan\Model\Orm\TitanCollection;
use Tracy\Debugger;

class ApiHelper
{
    /**
     * @throws BadRequestException
     */
    public static function prepareResponse($response, null|array|string $method = 'toMain'): array
    {
        if (is_string($response)) {
            return ['status' => $response];

        } else if ($response instanceof Entity) {
            return self::entityToArray($response, $method);

        } else if ($response instanceof DbalCollection
            || $response instanceof TitanCollection
            || $response instanceof ITitanCollection
            || $response instanceof ICollection
        ) {
            return Helper::collectionToArray($response, $method);

        } else if (is_array($response) === true) {
            return $response;
        }
        throw new BadRequestException('Unknown response format', 501);
    }

    /**
     * Return is usually array but can be number, string or anything
     */
    public static function entityToArray(IEntity $entity, array|string $method = 'toMain')
    {
        $args = [];

        if (is_array($method)) {
            $methodName = $method[0];
            array_shift($method);
            $args = $method;
        } else {
            $methodName = $method;
        }

        return call_user_func_array([$entity, $methodName], $args);
    }


    /**
     * @param string $jwtName
     * @return null|string
     * @deprecated use User:getJWTToken instead
     */
    public static function getJWTToken(string $jwtName): ?string
    {
        return User::getJWTToken($jwtName);
    }

    /**
     * @param $exception
     * @return array
     */
    public static function handleError($exception): array
    {
        if ($exception instanceof BadRequestException
            && in_array($exception->getCode(), [200, 400, 401, 403, 404, 405, 409, 410, 501])
        ) {
            Debugger::log(
                "HTTP code {$exception->getCode()}: {$exception->getMessage()} in {$exception->getFile()}:{$exception->getLine()}",
                'access'
            );
            return [
                'status' => $exception->getMessage(),
                'code' => $exception->getCode(),
            ];
        } else if ($exception instanceof UniqueConstraintViolationException) {
            return [
                'status' => $exception->getMessage(),
                'code' => 409,
            ];
        } else if ($exception instanceof InvalidArgumentException) {
            return [
                'status' => $exception->getMessage(),
                'code' => 400,
            ];
        }
        Debugger::log($exception, Debugger::EXCEPTION);
        return [
            'status' => 'Internal server error',
            'code' => 500,
        ];
    }
}