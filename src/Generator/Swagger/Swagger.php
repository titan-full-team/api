<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 04/11/2018
 * Time: 10:41 AM
 */

namespace Titan\Generator\Swagger;

use Nextras\Orm\Model\IModel;

class Swagger
{
    const PAGINATOR_MODEL_NAME = '_paginator';
    const MESSAGE_MODEL_NAME = '_messageResponse';

    protected IModel $model;

    protected string $presenterDir;

    protected array $swaggerConfig;

    public function __construct(IModel $model, string $presenter, array $swaggerConfig = [], $endpoints = [])
    {
        $this->model = $model;
        $this->presenterDir = $presenter;
        $this->swaggerConfig = $swaggerConfig;
    }

    public function generate(): array
    {
        $endpoint = new Endpoint($this->presenterDir);
        $model = new Model($this->model, $endpoint->getRequiredModels());

        $out['openapi'] = '3.0.0';
        $out['info'] = $this->swaggerConfig['info'] ?? [];
        $out['servers'] = [];
        $out['servers'][] = [
            'url' => $this->getSchemes()[0] . '://' . $this->getHost() . $this->getBasePath(),
            'description' => 'API url'
        ];
        $out['tags'] = $endpoint->tags();
        $out['paths'] = $endpoint->paths($this->swaggerConfig);
        $out['security'] = Security::getSecurityDefinitions($this->swaggerConfig);
        $out['components']['schemas'] = $model->generate();
        $out['externalDocs'] = $this->swaggerConfig['externalDocs'] ?? [];

        return $out;
    }


    /**
     * todo: check this out
     */
    protected function getSchemes(): array
    {
        return $_SERVER['SERVER_PORT'] == 80 ? ['http'] : ['https'];
    }

    protected function getHost(): string
    {
        $domain = parse_url($_SERVER['HTTP_HOST']);
        return $domain['path'] ?? '';
    }

    protected function getBasePath(): string
    {
        if (isset($this->swaggerConfig['pathInfo'])) {
            return $this->swaggerConfig['pathInfo'];
        } else if (!isset($_SERVER['PATH_INFO'])) {
            return '/api';
        }
        return str_replace($_SERVER['PATH_INFO'], '', $_SERVER['REQUEST_URI']);
    }
}
