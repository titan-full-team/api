<?php
/**
 * Created by PhpStorm.
 * User: norbertspanik
 * Date: 13/03/2018
 * Time: 16:31
 */

namespace Titan\Model\Functions;

use App\Model\BaseMapper;
use Nette\Application\BadRequestException;
use Nette\Utils\Strings;
use Nextras\Dbal\QueryBuilder\QueryBuilder;
use Nextras\Orm\Collection\Functions\IArrayFunction;
use Nextras\Orm\Collection\Functions\IQueryBuilderFunction;
use Nextras\Orm\Collection\Helpers\ArrayCollectionHelper;
use Nextras\Orm\Collection\Helpers\DbalExpressionResult;
use Nextras\Orm\Collection\Helpers\DbalQueryBuilderHelper;
use Nextras\Orm\Entity\IEntity;

/**
 * $users->findBy([LikeFilterFunction::class, 'name', 'Jon']);
 */
class LikeFilterFunction extends BaseFunction implements IArrayFunction, IQueryBuilderFunction
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var int
     */
    private $minLength;

    /**
     * @var string
     */
    private $conjunction;

    /**
     * @description explode string to small parts
     * @var bool
     */
    private $explode;

    /**
     * @var string
     */
    private $likeType;

    /**
     * @var DbalExpressionResult
     */
    private $expression;

    /**
     * @var string
     */
    private $value;

    /**
     * @param array $args
     */
    protected function checkArgs(array $args)
    {
        $count = count($args);
        assert(($count === 2 || $count === 3 || $count === 4 || $count === 5 || $count === 6 || $count === 7)
            && is_string($args[0])
            && is_string($args[1])
        );
    }


    /**
     * @param DbalQueryBuilderHelper $helper
     * @param QueryBuilder $builder
     * @param array $args
     * @throws BadRequestException
     */
    protected function initArgs(DbalQueryBuilderHelper $helper, QueryBuilder $builder, array $args)
    {
        $this->checkArgs($args);

        $this->expression = $helper->processPropertyExpr($builder, $args[0]);
        $this->value = trim($args[1]);
        $this->type = isset($args[2]) && in_array($args[2], ['%_like', '%like_', '%_like_']) ? $args[2] : '%_like_';
        $this->minLength = isset($args[3]) ? $args[3] : 2;
        $this->conjunction = $this->getConjuction($args); // 4
        $this->explode = isset($args[5]) ? $args[5] : true;
        $this->likeType = $this->getLikeType($args); // 6
    }


    /**
     * @return array
     * @throws BadRequestException
     */
    protected function build(): array
    {
        $likeExpresion = $this->likeType . ' ' . $this->type . ' COLLATE utf8mb4_general_ci';

        if ($this->explode === true) {
            $sql = [];
            $sqlToExecute[0] = '';
            $values = explode(' ', $this->value);

            foreach ($values as $value) {
                if (strlen($value) >= $this->minLength) {
                    $sql[] = '%ex ' . $likeExpresion;
                    $sqlToExecute[] = $this->expression->args;
                    $sqlToExecute[] = $value;
                }
            }

            $sqlToExecute[0] = implode(' ' . $this->conjunction . ' ', $sql);
        } else {
            $sqlToExecute[] = '%ex ' . $likeExpresion;
            $sqlToExecute[] = $this->expression->args;
            $sqlToExecute[] = $this->value;
        }

        if (strlen($sqlToExecute[0]) === 0) {
            throw new BadRequestException('Cannot search empty string');
        }
        $sqlToExecute[0] = '(' . $sqlToExecute[0] . ')';
        return $sqlToExecute;
    }


    /**
     * @param array $args
     * @return string
     * @throws BadRequestException
     */
    protected function getLikeType(array $args): string
    {
        $likeType = isset($args[6]) ? strtoupper($args[6]) : 'LIKE';
        if (in_array($likeType, ['LIKE', 'NOT LIKE']) === false) {
            throw new BadRequestException('Unsupported like parameter: ' . $likeType, 403);
        }
        return $likeType;
    }


    /**
     * @param array $args
     * @return string
     * @throws BadRequestException
     */
    protected function getConjuction(array $args): string
    {
        $conjunction = isset($args[4]) ? $args[4] : 'OR';
        if (in_array($conjunction, ['OR', 'AND']) === false) {
            throw new BadRequestException('Unknown like conjunction: ' . $conjunction, 403);
        }
        return $conjunction;
    }


    /**
     * Returns true if entity should stay in the result collection; the condition is evaluated in database and this
     * method just returns appropriate Nextras Dbal's filtering expression for passed args.
     * @phpstan-param array<int|string, mixed> $args
     * @param DbalQueryBuilderHelper $helper
     * @param QueryBuilder $builder
     * @param array $args
     * @return DbalExpressionResult
     * @throws BadRequestException
     */
    public function processQueryBuilderExpression(
        DbalQueryBuilderHelper $helper,
        QueryBuilder $builder,
        array $args
    ): DbalExpressionResult
    {
        $this->initArgs($helper, $builder, $args);
        return new DbalExpressionResult($this->build());
    }

    /**
     * Returns a value depending on values of entity; the expression passed by args is evaluated during this method
     * execution.
     * Usually returns a boolean for filtering evaluation.
     * @phpstan-param array<int|string, mixed> $args
     * @param ArrayCollectionHelper $helper
     * @param IEntity $entity
     * @param array $args
     * @return mixed
     */
    public function processArrayExpression(ArrayCollectionHelper $helper, IEntity $entity, array $args)
    {
        // todo: implement array function
        $value = $helper->getValue($entity, $args[0])->value;
        return Strings::startsWith($value, $args[1]);
    }
}