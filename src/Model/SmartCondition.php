<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 10/10/2018
 * Time: 7:12 PM
 */

namespace Titan\Model;

use Nette\Application\BadRequestException;
use Nextras\Orm\Collection\DbalCollection;
use Nextras\Orm\Collection\Helpers\DbalExpressionResult;
use Nextras\Orm\Collection\ICollection;
use Nextras\Orm\Entity\IEntity;
use Nextras\Orm\Entity\IProperty;
use Nextras\Orm\Entity\Reflection\PropertyMetadata;
use Nextras\Orm\Exception\InvalidArgumentException;
use Titan\Model\Functions\LikeFilterFunction;
use Tracy\Debugger;
use Tracy\ILogger;

class SmartCondition extends SmartRepository
{
    /**
     * @param DbalCollection $collection
     * @param array $parameters
     * @param string $entityClassName
     * @param array $disabledProperties
     * @param string $propertyPrefix
     * @return DbalCollection
     * @throws BadRequestException
     */
    public static function set(
        DbalCollection $collection,
        array $parameters,
        string $entityClassName,
        array $disabledProperties = [],
        string $propertyPrefix = ''
    ): DbalCollection
    {
        $conditions = self::prepareConditions(
            $collection,
            $parameters,
            $entityClassName,
            $disabledProperties,
            $propertyPrefix
        );

        foreach ($conditions as $condition) {
            // Simple condition
            if (sizeof($condition) === 1) {
                $collection = $collection->findBy($condition[0]);
                // Multiple condition (one column OR second column) firstname + lastname => search in both
            } else if (sizeof($condition) > 1) {
                $condition = array_merge([ICollection:: OR], $condition);
                $collection = $collection->findBy($condition);
            }
        }

        return $collection;
    }

    /**
     * @param DbalCollection $collection
     * @param array $parameters
     * @param string $entityClassName
     * @param array $disabledProperties
     * @param string $propertyPrefix
     * @return DbalCollection
     * @throws BadRequestException
     */
    protected static function prepareConditions(
        DbalCollection $collection,
        array $parameters,
        string $entityClassName,
        array $disabledProperties = [],
        string $propertyPrefix = ''
    ): array
    {
        $conditions = [];
        foreach ($parameters as $key => $parameter) {
            $type = isset($parameter['type']) ? $parameter['type'] : null;

            if (!is_null($type) && strtoupper($type) === 'OR') {
                $out = [];
                $groupConditions = self::prepareConditions(
                    $collection,
                    $parameter['conditions'],
                    $entityClassName,
                    $disabledProperties,
                    $propertyPrefix
                );

                foreach ($groupConditions as $groupCondition) {
                    foreach ($groupCondition as $subCondition) {
                        $out[] = $subCondition;
                    }
                }

                $conditions[$key] = $out;
            } else {
                $conditions[$key] = self::getConditions($parameter, $entityClassName, $disabledProperties);
            }
        }

        return $conditions;
    }


    /**
     * @param array $parameter
     * @param string $entityClassName
     * @param array $disabledProperties
     * @return array
     * @throws BadRequestException
     */
    protected static function getConditions(
        array $parameter,
        string $entityClassName,
        array $disabledProperties = []): array
    {
        $keys = is_string($parameter['key']) ? [$parameter['key']] : $parameter['key'];
        $conditions = [];

        foreach ($keys as $key) {
            $property = self::getProperty($entityClassName, $key, $disabledProperties);

            if (!is_null($property)) {
                $newCondition = self::getCondition($key, $property, $parameter);
                if (!is_null($newCondition)) {
                    $conditions[] = $newCondition;
                }
            }
        }
        return $conditions;
    }


    /**
     * @param string $dbPropertyName
     * @param PropertyMetadata $property
     * @param $parameterValue
     * @return array|null
     * @throws BadRequestException
     */
    protected static function getCondition(string $dbPropertyName, PropertyMetadata $property, $parameterValue): ?array
    {
        if (!is_array($parameterValue)) {
            return [$dbPropertyName => $parameterValue];
        } else if (array_key_exists('value', $parameterValue) === false) {
            throw new BadRequestException('Value in your search is not found', 404);
        } else if ($parameterValue['value'] === 'NOT NULL') {
            return [$dbPropertyName . '!=' => NULL];
        } else if ($parameterValue['value'] === NULL) {
            return [$dbPropertyName . self::getOperator($parameterValue) => NULL];
        } else if (self::isType($property, ['int', 'float']) === true) {
            $condition = self::numberSearch($dbPropertyName, $parameterValue, $property->isNullable);
            if (!empty($condition)) {
                return $condition;
            }
        } else if (self::isType($property, ['string']) === true) {
            return self::stringSearch($dbPropertyName, $parameterValue);
        } else if (self::isType($property, ['Nextras\Dbal\Utils\DateTimeImmutable']) === true) {
            return self::dateSearch($dbPropertyName, $parameterValue);
        } else if (self::isType($property, ['bool', 'boolean']) === true) {
            return [$dbPropertyName => $parameterValue['value']];
        }
        return null;
    }


    /**
     * @param array $parameterValue
     * @return string
     * @throws BadRequestException
     */
    protected static function getOperator(array $parameterValue): string
    {
        if (array_key_exists('type', $parameterValue)) {
            $operator = $parameterValue['type'];
        } else if (array_key_exists('operator', $parameterValue)) {
            Debugger::log('Operator parameter in smart condition is deprecated, use type instead', ILogger::WARNING);
            $operator = $parameterValue['operator'];
        } else {
            $operator = '=';
        }
        return self::validateOperator($operator);
    }


    /**
     * @param PropertyMetadata $property
     * @param array $types
     * @return bool
     */
    protected static function isType(PropertyMetadata $property, array $types): bool
    {
        foreach ($types as $type) {
            if (isset($property->types[$type]) && $property->types[$type] === true) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $propertyName
     * @param $value
     * @return array
     * @throws BadRequestException
     */
    protected static function numberSearch(string $propertyName, $value, bool $isNullable = false): array
    {
        if (!empty($value['value'])) {
            $operator = self::validateOperator(isset($value['type']) ? $value['type'] : '=');
            return [$propertyName . $operator => $value['value']];
        } else if ($isNullable === true && is_null($value['value'])) {
            return [$propertyName => null];
        }
        return [];
    }

    /**
     * @param $operator
     * @return string
     * @throws BadRequestException
     */
    public static function validateOperator($operator): string
    {
        if (in_array($operator, ['<=', '<', '>=', '>', '=', '!=']) === false) {
            throw new BadRequestException('Unknown operator: ' . $operator, 403);
        }
        return $operator === '=' ? '' : $operator;
    }


    /**
     * @param string $propertyName
     * @param $value
     * @return array
     * @throws BadRequestException
     */
    protected static function stringSearch(string $propertyName, $value): array
    {
        $type = isset($value['type']) ? $value['type'] : null;

        if (in_array($type, ['contains', 'notContains', 'fulltext'])) {
            return [
                LikeFilterFunction::class,
                $propertyName,
                $value['value'],
                isset($value['likeType']) ? $value['likeType'] : '%_like_',
                isset($value['minLength']) ? $value['minLength'] : 2,
                isset($value['conjunction']) ? strtoupper($value['conjunction']) : 'AND',
                isset($value['explode']) ? strtoupper($value['explode']) : true,
                $type === 'notContains' ? 'NOT LIKE' : 'LIKE'
            ];
        } else if (in_array($type, ['!=', '!==', 'notEqual'])) { // Not equals
            return [$propertyName . '!=' => $value['value']];
        } else if (in_array($type, ['=', '', null]) || is_null($type)) { // equals
            return [$propertyName => $value['value']];
        }
        throw new BadRequestException('Unknown search type:' . $type, 400);
    }

    /**
     * @param string $propertyName
     * @param $value
     * @return array
     * @throws BadRequestException
     */
    protected static function dateSearch(string $propertyName, $value): array
    {
        $operator = self::getOperator($value);
        if (strlen($value['value']) === 10) {
            return [\Titan\Model\Functions\DateFunction::class, $propertyName, $value['value'], $operator];
        }
        return [$propertyName . $operator => new \DateTimeImmutable($value['value'])];
    }


    /**
     * @param string $entityClassName
     * @param string $key
     * @param array $disabledProperties
     * @return PropertyMetadata|null
     */
    public static function getProperty(
        string $entityClassName,
        string $key,
        array $disabledProperties
    ): ?PropertyMetadata
    {
        /** @var IEntity $entity */
        $entity = new $entityClassName();
        $entityDisabledProperties = isset($entity->notAllowedParameters) ? $entity->notAllowedParameters : [];
        $disabledProperties = array_merge($entityDisabledProperties, $disabledProperties); // todo: is it okay ?

        if (stristr($key, '->') === false) {
            if (array_search($key, $disabledProperties) === false) {
                $property = self::getMetadataProperty($entity, $key);
                if (!is_null($property)) {
                    return is_null($property->relationship) && $property->isVirtual === false
                        ? $property
                        : $property->relationship;
                }
            }
            return null;
        }

        $keyParts = explode('->', $key);
        $column = $keyParts[0];
        unset($keyParts[0]);
        $property = self::getMetadataProperty($entity, $column);

        return is_null($property->relationship)
            ? null
            : self::getProperty(
                $property->relationship->entity,
                implode('->', $keyParts),
                isset($disabledProperties[$column]) ? $disabledProperties[$column] : []
            );
    }

    /**
     * @param IEntity $entity
     * @param string $name
     * @return IProperty|null
     */
    protected static function getMetadataProperty(IEntity $entity, string $name): ?PropertyMetadata
    {
        try {
            return $entity->getMetadata()->getProperty($name);
        } catch (InvalidArgumentException $exception) {
            return null;
        }
    }
}
