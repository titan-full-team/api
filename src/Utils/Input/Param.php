<?php

namespace Titan\Utils\Input;

use Exception;
use Nette\Application\BadRequestException;
use Nextras\Dbal\Utils\DateTimeImmutable;

class Param
{
    /**
     * @throws BadRequestException
     */
    public static function get(array $data, string $name, bool $required = false, $default = null, bool $nullable = true): mixed
    {
        if (array_key_exists($name, $data)) {
            if ($nullable === false && $data[$name] === null) {
                if ($default === null) {
                    throw new BadRequestException('Parameter ' . $name . ' cannot be null', 400);
                }

                return $default;
            }

            return $data[$name];
        }

        if ($required === true) {
            throw new BadRequestException('Parameter ' . $name . ' not found', 400);
        }

        return $default;
    }

    /**
     * @throws BadRequestException
     */
    public static function manyHasOne(array $data, string $name, bool $required = false, mixed $default = null): mixed
    {
        $value = self::get($data, $name, $required, $default);

        if (is_array($value)) {
            return self::get($value, 'id', true);
        }

        return $value;
    }

    /**
     * @throws BadRequestException
     */
    public static function oneHasMany(array $data, string $name, bool $required = false, mixed $default = []): array
    {
        return array_map(
            fn($item) => is_int($item) ? (int)$item : self::int($item, 'id', true),
            Param::get($data, $name, $required, $default)
        );
    }

    /**
     * @throws BadRequestException
     * @throws Exception
     */
    public static function date(array $data, string $name, bool $required = false, mixed $default = null): mixed
    {
        $value = self::get($data, $name, $required, $default);
        return $value === $default ? $default : new DateTimeImmutable($value);
    }

    /**
     * @throws BadRequestException
     * @throws Exception
     */
    public static function constant(array $data, string $name, array $options, $default = null): mixed
    {
        $value = self::get($data, $name, $default !== null, $default);

        if (in_array($value, $options) === false && $value !== null) {
            throw new BadRequestException(
                'Parameter ' . $name . ' is not supported. Supported are: ' . implode(', ', $options),
                400
            );
        }

        return $value;
    }

    /**
     * @throws BadRequestException
     * @throws Exception
     */
    public static function array(array $data, string $name, bool $required = false, $default = []): ?array
    {
        $value = self::get($data, $name, $required, $default);

        if (is_array($value) === false && $value !== null) {
            throw new BadRequestException('Parameter ' . $name . ' is not array', 400);
        }

        return $value === null ? null : $value;
    }

    /**
     * @throws BadRequestException
     * @throws Exception
     */
    public static function int(array $data, string $name, bool $required = false, $default = null): mixed
    {
        $value = self::get($data, $name, $required, $default);

        if ($value === $default) {
            return $default;
        }

        if (is_integer($value) === false && $value !== null) {
            throw new BadRequestException('Parameter ' . $name . ' is not integer', 400);
        }

        return $value === null ? null : (int)$value;
    }

    /**
     * @throws BadRequestException
     * @throws Exception
     */
    public static function float(array $data, string $name, bool $required = false, $default = null): mixed
    {
        $value = self::get($data, $name, $required, $default);

        if ($value === $default) {
            return $default;
        }

        if (is_integer($value)) {
            $value = (float)$value;
        }

        if (is_float($value) === false && $value !== null) {
            throw new BadRequestException('Parameter ' . $name . ' is not float', 400);
        }

        return $value === null ? null : (float)$value;
    }

    /**
     * @throws BadRequestException
     * @throws Exception
     */
    public static function bool(array $data, string $name, bool $required = false, ?bool $default = null): ?bool
    {
        $value = self::get($data, $name, $required, $default);

        if (is_bool($value) === false && $value !== null) {
            throw new BadRequestException('Parameter ' . $name . ' is not boolean', 400);
        }

        return $value === null ? $default : (bool)$value;
    }

    /**
     * @throws BadRequestException
     */
    public static function getSort(array $data, array $allowedSort, array $default): array
    {
        $out = [];
        $value = self::get($data, 'sort', false, $default);
        $value = empty($value) ? $default : $value;

        # Fixed allowed sort
        foreach ($allowedSort as $sortKey => $sortValue) {
            if (is_array($sortValue) === false) {
                $allowedSort[$sortValue] = [$sortValue];
                unset($allowedSort[$sortKey]);
            }
        }

        foreach ($value as $key => $direction) {
            $direction = strtoupper($direction);

            if (in_array($direction, ['DESC', 'ASC']) === false) {
                throw new BadRequestException(
                    'Unknown sort direction: ' . $direction . '. Supported are: DESC, ASC',
                    400
                );
            }

            if (array_key_exists($key, $allowedSort) === false) {
                throw new BadRequestException(
                    'Unknown sort parameter: ' . $key . '. Supported are: ' . implode(', ', array_keys($allowedSort)),
                    400
                );
            }

            foreach ($allowedSort[$key] as $sortColumn) {
                $out[$sortColumn] = $direction;
            }
        }

        return $out;
    }
}
