<?php

namespace Titan\Services;

use Nextras\Orm\Entity\IEntity;

interface IAuthService
{
    public function setUser(?IEntity $user): void;

    public function disableValidation(string $class): void;

    public function isValidationDisabled(string $class): bool;

    public function enableValidation(string $class): void;

    public function isLogged();

    public function getUser(): ?IEntity;
}