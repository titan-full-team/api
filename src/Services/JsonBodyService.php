<?php

namespace Titan\Services;

final class JsonBodyService
{
    public function __construct(private readonly \Nette\Http\Request $httpRequest)
    {
    }

    public function get(string $parameter, $default = null): mixed
    {
        $data = $this->all([$parameter]);
        return array_key_exists($parameter, $data) ? $data[$parameter] : $default;
    }

    public function all(array $params = NULL, bool $nullUndefined = false): array
    {
        $body = $this->httpRequest->getRawBody();
        $data = json_decode($body, true);
        $data = is_array($data) ? $data : [];

        if (is_null($params)) {
            return $data;
        }
        $out = [];
        foreach ($params as $param) {
            if (array_key_exists($param, $data) !== false) {
                $out[$param] = $data[$param];
            } elseif ($nullUndefined === true) {
                $out[$param] = NULL;
            }
        }
        return $out;
    }
}