<?php

namespace Titan\Presenter;

use Nette\Application\BadRequestException;
use Nette\Utils\Random;
use Titan\Utils\File;

/** @deprecated */
class FilePresenter extends CorePresenter
{
    /**
     * @var string
     */
    protected $fileDir;

    public function startup()
    {
        parent::startup();
        $this->fileDir = $this->getConfigParameter('appDir') . '/../file/';
    }


    /**
     * @param string $entity
     * @param string $value
     * @param string $name
     * @throws BadRequestException
     * @swagger-ignore true
     * @out-200 todo
     * @route file/<entity>/<value>/<name>
     * @methods get
     */
    public function actionGet(string $entity, string $value, string $name)
    {
        $folder = $this->fileDir . File::cleanFolderName($entity) . '/' . File::cleanFolderName($value) . '/';
        File::printFile($folder, $name);
    }

    /**
     * todo: swagger
     * @param string|null $folder
     * @throws \Exception
     * @out-201 todo
     * @methods post
     */
    public function actionCreate(string $folder = null)
    {
        $appDir = $this->fileDir;
        $file = File::saveFile(
            $appDir,
            $this->getJsonBodyParameter('base64'),
            $this->getJsonBodyParameter('fileName', Random::generate()),
            File::cleanFolderName($folder)
        );
        $file['link'] = $this->link('//File:Image', $file['path'], $file['name']);
        $this->sendJson($file);
    }
}
