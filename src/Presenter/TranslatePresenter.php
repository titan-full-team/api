<?php

namespace Titan\Presenter;

use App\Model\Translate\Locale\TranslateLocale;
use App\Model\Translate\Translate;
use Nette\Application\BadRequestException;
use Nette\Utils\DateTime;
use Nextras\Orm\Collection\ICollection;

/** @deprecated */
class TranslatePresenter extends CorePresenter
{
    /**
     * todo: swagger
     * @param string $type
     * @throws BadRequestException
     * @throws \Exception
     * @methods POST
     * @out-201 Strings were added
     */
    public function actionCreate(string $type)
    {
        foreach ($this->getJsonBodyParameter('slugs') as $slug) {
            $data = [
                'slug' => $slug,
                'type' => $type,
            ];
            $entity = $this->model->translate->patchNewEntity($data);
            try {
                $this->model->prepareAndFlush($entity);
            } catch (\Nextras\Dbal\Drivers\Exception\UniqueConstraintViolationException $exception) {
            }
        }
        $this->sendJson('Strings were added');
    }

    /**
     * @param string $site
     * @param string $project
     * @throws BadRequestException
     * @methods GET
     * @out-201 Strings were sync
     */
    public function actionSync(string $site, string $project = 'ADM')
    {
        $site .= '/api/translate/to-sync/' . $project;
        $data = json_decode(file_get_contents(urldecode($site)), true);

        foreach ($data as $row) {
            /** @var Translate $translate */
            $translate = $this->model->translate->getBy(['slug' => $row['slug']]);

            if (is_null($translate)) {
                $translate = new Translate();
                $translate->html = $row['html'];
                $translate->slug = $row['slug'];
                $translate->type = $row['type'];
                $translate = $this->model->prepareAndFlush($translate);
            }

            foreach ($row['translateLocale'] as $item) {
                /** @var TranslateLocale $translateLocale */
                $translateLocale = $translate->translateLocale->toCollection()->getBy(['lang' => $item['lang']]);

                if (is_null($translateLocale)) {
                    $newEntity = new TranslateLocale();
                    $newEntity->translate = $translate;
                    $newEntity->value = $item['value'];
                    $newEntity->updatedOn = 'now';
                    $newEntity->lang = $item['lang'];
                    $this->model->prepareAndFlush($newEntity);

                } else if ($translateLocale->updatedOn->getTimestamp() < DateTime::from($item['updatedOn'])->getTimestamp()) {
                    $translateLocale->value = $item['value'];
                    $translateLocale->updatedOn = 'now';
                    $this->model->prepareAndFlush($translateLocale);
                }
            }
        }

        $this->sendJson('Strings were sync');
    }


    /**
     * @param string $project
     * @throws BadRequestException
     * @methods GET
     * @out-200 {}
     */
    public function actionToSync(string $project)
    {
        $data = $this->model->translate
            ->findAll()
            ->findBy(['type' => strtoupper($project)])
            ->orderBy('translateLocale->updatedOn', ICollection::DESC);

        $this->sendJson($data);
    }


    /**
     * @param int $id
     * @throws BadRequestException
     * @methods PUT
     * @out-200 String was updated
     */
    public function actionToggleHtml(int $id)
    {
        /** @var Translate $entity */
        $entity = $this->model->translate->getByIdWithValidation($id);
        $entity->html = !$entity->html;
        $this->model->prepareAndFlush($entity);
        $this->sendJson('String was updated');
    }


    /**
     * @param int $page
     * @param int $perPage
     * @throws BadRequestException
     * @out-200 Translate::toArray
     * @methods post
     */
    public function actionList(int $page = 1, int $perPage = 10)
    {
        $this->acl->checkSectionPermission('Translate', 'r');
        $data = $this->model->translate
            ->findAll()
            ->findByApi($this->getJsonBodyParameter('conditions', []))
            ->orderByApi($this->getJsonBodyParameter('sort', []))
            ->orderBy('id', ICollection::DESC);
        $this->sendPaginatedListJson($data, $page, $perPage);
    }


    /**
     * todo: swagger list
     * @param string $lang
     * @param string $type
     * @throws BadRequestException
     * @throws \Exception
     * @out-200 Translate::toArray
     * @methods GET
     */
    public function actionTranslates(string $lang, string $type)
    {
        $out = [];
        $data = $this->model->translate
            ->findAll()
            ->findByApi($this->getJsonBodyParameter('conditions', []))
            ->orderByApi($this->getJsonBodyParameter('sort', []))
            ->findBy(['translateLocale->lang' => $lang])
            ->findBy(['type' => strtoupper($type)]);

        /** @var Translate $row */
        foreach ($data as $row) {
            /** @var TranslateLocale $translateLocale */
            $translateLocale = $row->translateLocale->toCollection()->findBy(['lang' => $lang])->fetch();
            $out[$row->slug] = $translateLocale->value;
        }
        $this->sendJson($out);
    }


    /**
     * @param int $translateId
     * @param string $lang
     * @throws BadRequestException
     * @throws \Exception
     * @methods PUT
     * @in TranslateLocale
     * @out-200 TranslateLocale::toArray
     */
    public function actionUpdateTranslate(int $translateId, string $lang)
    {
        $this->acl->checkSectionPermission('Translate', 'u');

        /** @var Translate $translateEntity */
        $translateEntity = $this->model->translate->getByIdWithValidation($translateId);
        $value = trim($this->getJsonBodyParameter('value'));

        if (empty($value)) {
            throw new BadRequestException('Value cannot be empty', 400);
        }

        $translate = $this->model->translateLocale
            ->getBy([
                'lang' => $lang,
                'translate->id' => $translateEntity->id,
            ]);

        if ($translate === null) {
            $entity = $this->model->translateLocale
                ->patchNewEntity([
                    'lang' => $lang,
                    'value' => $value,
                    'translate' => $translateEntity->id,
                ]);
        } else {
            $entity = $this->model->translateLocale->patchEntity(['value' => $value], $translate);
        }

        $this->sendJson($this->model->prepareAndFlush($entity));
    }


    /**
     * @param int $id
     * @throws BadRequestException
     * @throws \Exception
     * @methods DELETE
     * @out-200 Translate was deleted
     */
    public function actionDelete(int $id)
    {
        $this->acl->checkSectionPermission('Translate', 'd');
        $this->model->translate->removeAndFlush($this->model->translate->getByIdWithValidation($id), false);
        $this->sendJson('Translate was deleted');
    }
}
