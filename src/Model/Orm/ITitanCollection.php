<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 08/02/2019
 * Time: 9:41 PM
 */

namespace Titan\Model\Orm;

use Nextras\Orm\Collection\ICollection;

interface ITitanCollection extends ICollection
{
    /**
     * @param string $condition
     * @param $parameters
     * @return ICollection|ITitanCollection
     */
    public function whereSql(string $condition, $parameters): ICollection;


    /**
     * @param string $condition
     * @param $parameters
     * @return ICollection|ITitanCollection
     */
    public function where(string $condition, $parameters): ICollection;


    /**
     * @param array $where
     * @return ICollection|ITitanCollection
     */
    public function findBy(array $where): ICollection;


    /**
     * @param callable $callback
     * @return ICollection|ITitanCollection
     */
    public function customSql(callable $callback): ICollection;


    /**
     * @param string $column
     * @param string $value
     * @param string $type
     * @param int $minWordLength
     * @param string $conjunction
     * @param bool $explodeSearchPhrase
     * @return ICollection|ITitanCollection
     */
    public function findLike(
        string $column,
        string $value,
        string $type = '%_like_',
        int $minWordLength = 3,
        string $conjunction = 'AND',
        bool $explodeSearchPhrase = true
    ): ICollection;


    /**
     * @param string $column
     * @param string $value
     * @param string $type
     * @param int $minWordLength
     * @param string $conjunction
     * @param bool $explodeSearchPhrase
     * @return ICollection|ITitanCollection
     */
    public function findNotLike(
        string $column,
        string $value,
        string $type = '%_like_',
        int $minWordLength = 3,
        string $conjunction = 'AND',
        bool $explodeSearchPhrase = true
    ): ICollection;


    /**
     * @param array $conditions
     * @param array $disabledProperties
     * @return ICollection|ITitanCollection
     */
    public function findByApi(array $conditions, array $disabledProperties = []): ICollection;


    /**
     * @param array $sort
     * @param array $disabledProperties
     * @return ICollection|ITitanCollection
     */
    public function orderByApi(array $sort, array $disabledProperties = []): ICollection;


    /**
     * @param array|string $parameters
     * @return ICollection|ITitanCollection
     */
    public function groupBy($parameters): ICollection;


    /**
     * @param string $parameters
     * @return ICollection|ITitanCollection
     */
    public function groupBySql(string $parameters): ICollection;


    /**
     * @param string $column
     * @return float
     */
    public function sumStored(string $column): float;


    /**
     * @param string $column
     * @return float
     */
    public function avgStored(string $column): float;
}