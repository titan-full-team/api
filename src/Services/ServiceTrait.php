<?php

namespace Titan\Services;

use App\Model\Model;

trait ServiceTrait
{
    public function __construct(protected Model $model)
    {
        $this->onStartup();
    }

    protected function onStartup(): void
    {
    }
}