CREATE TABLE `translate_content` (
  `key` int(10) unsigned NOT NULL,
  `table` varchar(255) NOT NULL,
  `column` varchar(255) NOT NULL,
  `lang` char(2) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`key`,`table`,`column`,`lang`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;