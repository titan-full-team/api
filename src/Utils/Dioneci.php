<?php
/**
 * Created by PhpStorm.
 * User: norbertspanik
 * Date: 23/11/2017
 * Time: 11:53
 */

namespace Titan\Utils;

use InvalidArgumentException;
use Nette\Application\BadRequestException;
use Nette\Utils\Finder;

class Dioneci
{
    /**
     * @var string
     */
    protected $authKey;

    /**
     * @param string $authKey
     */
    public function __construct(string $authKey)
    {
        $this->authKey = $authKey;
    }

    /**
     * TODO: detect auth by self
     * @param string $auth
     * @throws \Exception
     */
    public function auth(string $auth)
    {
        if (is_null($auth)) {
            throw new BadRequestException('Auth key not found.', 401);
        } elseif ($this->authKey !== $auth) {
            throw new BadRequestException('Auth key is not valid.', 401);
        }
    }

    /**
     * @param string $dirPath
     */
    public function deleteDir(string $dirPath)
    {
        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

    /**
     * @param string $dir
     * @param array $fileContainsString
     * @return array
     */
    public function getFileList(string $dir, array $fileContainsString): array
    {
        $out = [];
        foreach (Finder::findFiles('*')->from($dir) as $file) {
            $name = str_replace($dir, '', $file);
            foreach ($fileContainsString as $type) {
                if (stristr($name, $type) !== false) {
                    $out[] = [
                        'type' => $type,
                        'name' => $name,
                        'file' => $file->getPath() . '/' . $name
                    ];
                }
            }
        }
        sort($out);
        return $out;
    }

    /**
     * @param string $dir
     * @param array $fileContainsString
     */
    public function deleteFileList(string $dir, array $fileContainsString)
    {
        $logFiles = $this->getFileList($dir, $fileContainsString);
        foreach ($logFiles as $logFile) {
            unlink($logFile['file']);
        }
    }

    /**
     * @param string $fileLocation
     * @param string $fileName
     * @return array|bool
     */
    public function getFile(string $fileLocation, string $fileName)
    {
        if (is_file($fileLocation)) {
            $fileSize = filesize($fileLocation);
            $openedFile = fopen($fileLocation, 'r');
            $fileData = fread($openedFile, $fileSize);
            fclose($openedFile);
            return [
                'name' => $fileName,
                'size' => $fileSize,
                'mimeContentType' => mime_content_type($fileLocation),
                'data' => $fileData,
            ];
        }
        return false;
    }
}