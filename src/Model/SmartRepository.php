<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 10/10/2018
 * Time: 7:12 PM
 */

namespace Titan\Model;

use Nextras\Orm\Entity\IEntity;

class SmartRepository
{
    /**
     * todo: disabled properties
     * @param string $entityClassName
     * @param array $disabledProperties
     * @return array
     */
    protected static function getAvailableProperties(string $entityClassName, array $disabledProperties = []): array
    {
        $out = [];
        foreach (self::getEntity($entityClassName)->getMetadata()->getProperties() as $property) {
            if (is_null($property->relationship)
                && $property->isVirtual === false
                && in_array($property->name, $disabledProperties) === false
            ) {
                $out[] = $property;
            }
        }
        return $out;
    }

    /**
     * @param string $entityClassName
     * @param array $disabledProperties
     * @return array
     */
    protected static function getAllAvailableProperties(string $entityClassName, array $disabledProperties = []): array
    {
        $out = [];
        foreach (self::getEntity($entityClassName)->getMetadata()->getProperties() as $property) {
            if ($property->isVirtual === false
                && in_array($property->name, $disabledProperties) === false
            ) {
                $out[] = $property;
            }
        }
        return $out;
    }

    /**
     * @param string $entityClassName
     * @return IEntity
     */
    protected static function getEntity(string $entityClassName): IEntity
    {
        return new $entityClassName();
    }
}