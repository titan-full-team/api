<?php

namespace Titan\Router;

use Nette\Http\IRequest;

class RestRoute extends \Nette\Application\Routers\Route
{
    /**
     * @var string
     */
    private $method;

    /**
     * @param string $mask
     * @param string $metadata
     * @param $method
     * @param int $flags
     */
    public function __construct(string $mask, string $metadata, string $method = 'GET', $flags = 0)
    {
        $this->method = strtoupper($method);
        return parent::__construct($mask, $metadata, $flags);
    }

    /**
     * @param IRequest $httpRequest
     * @return \Nette\Application\Request|null
     */
    public function match(IRequest $httpRequest): ?array
    {
        $request = parent::match($httpRequest);

        if ($request) {
            $requestMethod = strtoupper($httpRequest->getMethod());
            if ($requestMethod === $this->method || $requestMethod === 'OPTIONS') {
                return $request;
            }
        }
        return NULL;
    }
}
