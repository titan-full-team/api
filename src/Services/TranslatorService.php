<?php

namespace Titan\Services;

use Nextras\Orm\Model\IModel;
use Titan\Utils\Translator;

class TranslatorService
{
    protected ?Translator $translator = null;

    public function __construct(private IModel $model, private string $defaultLanguage = 'en')
    {

    }

    protected function getLocale(): string
    {
        return Translator::getLanguage(self::getDefaultLanguage());
    }

    public function getDefaultLanguage(): string
    {
        return $this->defaultLanguage;
    }

    public function getTranslator(?string $locale = null, bool $reload = false): \Nette\Localization\ITranslator
    {
        if (is_null($this->translator) || $reload === true) {
            $locale = is_null($locale) ? $this->getLocale() : $locale;
            $this->translator = new Translator($this->model, $locale);
        }
        return $this->translator;
    }

    public function i18n(string $message, ?int $count = null): string
    {
        return $this->getTranslator()->translate($message, $count);
    }
}
