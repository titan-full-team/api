<?php

namespace Titan\Utils\Input;

use Nette\Application\BadRequestException;

class InputFloat implements IInput
{
    use InputTrait;

    protected function __construct()
    {
        $this->type = 'float';
    }

    public function get()
    {
        return $this->hasValue === true ? (float)$this->value : $this->default;
    }

    /**
     * @throws BadRequestException
     */
    public function min(int $limit = 0): self
    {
        if ($this->hasValue === true && is_float($this->value) && $this->value < $limit) {
            $this->throwException('has to be at least ' . $limit);
        }

        return $this;
    }

    /**
     * @throws BadRequestException
     */
    public function max(int $limit = 255): self
    {
        if ($this->hasValue === true && is_float($this->value) && $this->value > $limit) {
            $this->throwException('has is to high. Maximum is ' . $limit);
        }
        return $this;
    }

    public function round(int $precision = 2): self
    {
        if ($this->hasValue === true && is_float($this->value)) {
            $this->value = round($this->value, $precision);
        }
        return $this;
    }

    public function floor(): self
    {
        if ($this->hasValue === true && is_float($this->value)) {
            $this->value = floor($this->value);
        }
        return $this;
    }

    public function ceil(): self
    {
        if ($this->hasValue === true && is_float($this->value)) {
            $this->value = ceil($this->value);
        }
        return $this;
    }
}