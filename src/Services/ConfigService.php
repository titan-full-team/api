<?php

namespace Titan\Services;

readonly class ConfigService
{
    public function __construct(private array $configParameters)
    {
    }

    public function get(string $parameter, $default = NULL): mixed
    {
        return array_key_exists($parameter, $this->configParameters) ? $this->configParameters[$parameter] : $default;
    }
}
