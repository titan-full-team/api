<?php

namespace Titan\Utils;

use App\Model\Template\Template;
use Nette\Application\BadRequestException;
use Nette\Mail\Message;
use Nextras\Orm\Entity\IEntity;
use Nextras\Orm\Repository\IRepository;
use Titan\Model\Helper;

class Mailer
{
    /**
     * @var
     */
    protected $mailRepository;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var string
     */
    private $templateFile;

    /**
     * @param array $config
     * @param IRepository $repository
     * @param string $templateFile
     */
    public function __construct(array $config, IRepository $repository, string $templateFile)
    {
        $this->config = $config;
        $this->mailRepository = $repository;
        $this->templateFile = $templateFile;
    }

    /**
     * @param string $type
     * @param string $to
     * @param array $entities
     * @param array $customReplacements
     * @return Message
     * @throws BadRequestException
     */
    public function send(string $type, string $to, array $entities = [], array $customReplacements = []): ?Message
    {
        $mail = $this->getMessage($type, $to, $entities, $customReplacements);
        $this->sendMessage($mail);
        return $mail;
    }


    /**
     * @param string $type
     * @param string $to
     * @param array $entities
     * @param array $customReplacements
     * @return Message
     * @throws BadRequestException
     */
    public function getMessage(string $type, string $to, array $entities = [], array $customReplacements = []): Message
    {
        $template = $this->getTemplate($type);
        $mail = (new Message())
            ->setFrom($this->config['mail'])
            ->addTo($to)
            ->setSubject($this->replaceStrings($template->name, $entities, $customReplacements))
            ->setHtmlBody($this->prepareTemplate($template->name, $template->text, $entities, $customReplacements));

        return $mail;
    }


    /**
     * @param string $subject
     * @param string $template
     * @param array $entities
     * @param array $customReplacements
     * @return string
     */
    public function prepareTemplate(
        string $subject,
        string $template,
        array $entities = [],
        array $customReplacements = []
    ): string
    {
        $replacements = $this->getReplacements($entities, $customReplacements);
        $keys = array_keys($replacements);
        $values = array_values($replacements);

        return (new \Latte\Engine)->renderToString($this->templateFile, [
            'projectName' => $this->config['projectName'],
            'subject' => str_replace($keys, $values, $subject),
            'mailFolder' => $this->getHost(),
            'body' => $this->replaceStrings($template, $entities, $customReplacements),
            'footer' => str_replace($keys, $values, ''),
        ]);
    }

    /**
     * @param string $subject
     * @param string $template
     * @param array $entities
     * @param array $customReplacements
     * @return string
     */
    public function replaceStrings(
        string $template,
        array $entities = [],
        array $customReplacements = []
    ): string
    {
        $replacements = $this->getReplacements($entities, $customReplacements);
        $keys = array_keys($replacements);
        $values = array_values($replacements);
        return str_replace($keys, $values, $template);
    }

    /**
     * @return string
     */
    protected function getHost(): string
    {
        return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . '/mail/';
    }

    /**
     * @param Message $mail
     */
    public function sendMessage(Message $mail)
    {
        $config = $this->config;
        $config['username'] = $this->config['mail'];

        $mailer = new \Nette\Mail\SmtpMailer($config);
        if (empty($mail->getFrom())) {
            $mail->setFrom($this->config['mail']);
        }
        $mailer->send($mail);
    }

    /**
     * @param array $entities
     * @param array $customReplacements
     * @param callable|null $valueCallback
     * @return array
     */
    public function getReplacements(
        array $entities,
        array $customReplacements = [],
        callable $valueCallback = null
    ): array
    {
        $out = [];
        /** @var IEntity $entity */
        foreach ($entities as $entity) {
            $className = $entity->getMetadata()->getClassName();
            $className = substr($className, strrpos($className, '\\') + 1);
            $notAllowedProperties = Helper::getNotAllowedEntityProperties($entity);

            foreach ($entity->getMetadata()->getProperties() as $property) {
                if (is_null($property->relationship) && in_array($property->name, $notAllowedProperties) === false) {
                    if ((isset($property->types['int']) && $property->types['int'] === true)
                        || (isset($property->types['string']) && $property->types['string'] === true)
                        || (isset($property->types['float']) && $property->types['float'] === true)
                    ) {
                        $value = empty($entity->{$property->name}) ? '' : $entity->{$property->name};
                        $out['#' . $className . '-' . $property->name . '#'] = is_callable($valueCallback)
                            ? call_user_func($valueCallback, $value)
                            : $value;
                    } else if (isset($property->types['Nextras\Dbal\Utils\DateTimeImmutable'])
                        && $property->types['Nextras\Dbal\Utils\DateTimeImmutable'] === true
                    ) {
                        $out['#' . $className . '-' . $property->name . '#'] = is_null($entity->{$property->name})
                            ? ''
                            : $entity->{$property->name}->format('d.m.Y');
                    }
                }
            }
        }
        foreach ($customReplacements as $key => $value) {
            $out['#Global-' . $key . '#'] = is_callable($valueCallback)
                ? call_user_func($valueCallback, $value)
                : $value;;
        }
        return $out;
    }

    /**
     * @param string $type
     * @return IEntity|Template
     * @throws BadRequestException
     */
    protected function getTemplate(string $type): IEntity
    {
        $template = $this->mailRepository->getBy(['identificator' => $type, 'active' => 1]);
        if (is_null($template)) {
            throw new BadRequestException('Cannot find template:' . $type, 404);
        }
        return $template;
    }

}
