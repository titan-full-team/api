<?php

namespace Titan\Utils\auth;

use App\Model\User\Token\UserToken;
use Nette\Application\BadRequestException;
use Nette\Security\Passwords;
use Nette\Utils\Random;
use Nextras\Orm\Entity\IEntity;
use Titan\Utils\User;

class Credentials extends Auth implements IAuth
{
    /**
     * PASSWORD TOKEN KEY
     */
    const PASSWORD_TOKEN = 'password';

    /**
     * @var string
     */
    protected $loginColumn = 'login';

    /**
     * @param IEntity $user
     * @param array $data
     * @param bool $autoLogin
     * @param string $tokenExpiration
     * @return bool|mixed|string
     * @throws BadRequestException
     */
    public function register(IEntity $user, array $data, bool $autoLogin = true, $tokenExpiration = '+1 day')
    {
        try {
            $this->setNewToken($user, (new Passwords())->hash($data['password']), self::PASSWORD_TOKEN, null, false);
            $this->userModel->flush();

            if ($autoLogin === true) {
                return $this->login($data, $tokenExpiration);
            }
        } catch (\Nextras\Dbal\UniqueConstraintViolationException $exception) {
            throw new BadRequestException('Login is not available', 403);
        }

        return true;
    }

    /**
     * @param IEntity $user
     * @param string $password
     */
    public function generatePassword(IEntity $user, string $password)
    {
        $this->setNewToken($user, (new Passwords())->hash($password), self::PASSWORD_TOKEN, null, false);
    }

    /**
     * @param array $data
     * @param string|null $tokenExpiration
     * @return string
     * @throws BadRequestException
     */
    public function login(array $data, $tokenExpiration = '+1 day'): string
    {
        $login = $data['login'];
        $password = $data['password'];

        /** @var \App\Model\User\User $account */
        $account = $this->userModel->getBy([$this->loginColumn => $login]);

        if (!is_null($account)) {
            /** @var UserToken|null $passwordToken */
            $passwordToken = $account->userToken->get()->getBy(['type' => self::PASSWORD_TOKEN]);

            if (is_null($passwordToken)) {
                throw new BadRequestException('Password for account is not set, use another login method', 403);
            }


            if ((new Passwords())->verify($password, $passwordToken->token) === true) {
                return $this->setJWT($account, get_class(), $tokenExpiration);
            }
        }

        throw new BadRequestException('Login credentials are invalid', 401);
    }

    /**
     * @param IEntity|\App\Model\User\User $account
     * @param array $data
     * @return string
     * @throws BadRequestException
     */
    public function changeCredentials(IEntity $account, array $data): string
    {
        if (!isset($data['oldPassword']) || !isset($data['newPassword'])) {
            throw new BadRequestException('Required paramter oldPassword or/and newPassword are missing', 400);
        }
        return $this->setCredentials($account, true, $data['newPassword'], $data['oldPassword']);
    }

    /**
     * @param IEntity $account
     * @param bool $validateOldToken
     * @param string $newToken
     * @param string|null $oldToken
     * @return string
     * @throws BadRequestException
     */
    public function setCredentials(
        IEntity $account,
        bool $validateOldToken,
        string $newToken,
        string $oldToken = null
    ): string
    {
        /** @var UserToken|null $passwordToken */
        $passwordToken = $account->userToken->get()->getBy(['type' => self::PASSWORD_TOKEN]);

        if (is_null($passwordToken)) {
            throw new BadRequestException('Password for account is not set', 403);
        } else if ($validateOldToken === true && (new Passwords())->verify($oldToken, $passwordToken->token) === false) {
            throw new BadRequestException('Old password is invalid', 403);
        }

        $passwordToken->token = (new Passwords())->hash($newToken);
        $this->userTokenModel->persistAndFlush($passwordToken);

        return 'Password changed';
    }

    public function logout()
    {
        parent::logout();
    }
}
