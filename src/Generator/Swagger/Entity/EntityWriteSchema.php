<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 04/11/2018
 * Time: 11:58 AM
 */

namespace Titan\Generator\Swagger\Entity;

use Nextras\Orm\Model\IModel;

class EntityWriteSchema extends EntitySchema implements IEntitySchema
{
    protected string $entity;

    protected string $inputType;

    protected bool $generated = false;

    protected array $data = [];

    /**
     * @throws \Exception
     */
    public function __construct(string $entity, string $inputType)
    {
        if (empty($entity)) {
            throw new \Exception('Entity cannot be empty', 500);
        }
        $this->entity = $entity;
        $this->inputType = $inputType;
    }

    /**
     * @param IModel $model
     * @throws \Exception
     */
    public function generate(IModel $model): void
    {
        if (class_exists($this->entity) === false) {
            throw new \InvalidArgumentException('Class ' . $this->entity . ' not found in ' . $this->foundInPresenter);
        }

        $class = new \ReflectionClass($this->entity);


        $this->data['type'] = 'object';
        $this->data['required'] = [];

        foreach ($class->getProperties() as $property) {
            $types = $property->hasType() ? [$property->getType()->getName() => ''] : ['string' => ''];

            $this->data['properties'][$property->getName()] = [
                'type' => Entity::getPropertyType($types),
                'nullable' => $property->getType()->allowsNull(),
                'readOnly' => false,
                'format' => Entity::getPropertyFormat($types),
            ];
            if ($property->getType()->allowsNull() === false) {
                $this->data['required'][] = $property->getName();
            }
        }

        $this->generated = true;
    }

    public function getDependencies(): array
    {
        return [];
    }

    public function isGenerated(): bool
    {
        return $this->generated;
    }

    public function getEntityName(): string
    {
        $entity = explode('\\', $this->entity);

        return $entity[sizeof($entity)-1] . '-' . $this->inputType;
    }

    public function getIdentificator(): string
    {
        return md5($this->getEntityName());
    }

    public function getEntityFullName(): string
    {
        return $this->entity;
    }

    public function getData(): array
    {
        return $this->data;
    }
}