<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 2019-07-09
 * Time: 18:36
 */

namespace Titan\Generator\Swagger;

use Nette\PhpGenerator\Method;
use Titan\Generator\Annotation;

class Security
{
    public const USER_ACCOUNT = 'LoggedUserToken';

    protected Method $method;

    public function __construct(Method $method)
    {
        $this->method = $method;
    }

    public function get(): array
    {
        $annotation = Annotation::getBoolValue($this->method->getComment(), 'swagger-user', null);

        if (!is_null($annotation)) {
            return $annotation === true ? $this->generateSecurity([self::USER_ACCOUNT]) : [];
        }

        $annotation = Annotation::getValue($this->method->getComment(), 'swagger-security', null);

        if (!is_null($annotation)) {
            return $this->generateSecurity([$annotation]);
        }

        if (stristr($this->method->getBody(), '$this->acl->checkSectionPermission') !== false
            || stristr($this->method->getBody(), '$this->acl->checkLogin') !== false
        ) {
            return $this->generateSecurity([self::USER_ACCOUNT]);
        }

        return [];
    }

    protected function generateSecurity(array $types): array
    {
        $out = [];
        foreach ($types as $type) {
            $out[$type] = [];
        }
        return [$out];
    }


    public static function getSecurityDefinitions(array $swaggerConfig): array
    {
        $out = $swaggerConfig['securityDefinitions'] ?? [];

        $out[self::USER_ACCOUNT] = [
            'type' => 'apiKey',
            'name' => 'Authorization',
            'in' => 'header',
        ];

        $out['DioneCi'] = [
            'type' => 'apiKey',
            'name' => 'Authorization',
            'in' => 'header',
        ];

        return $out;
    }

}