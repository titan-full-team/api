<?php

namespace Titan\Presenter;

use App\Model\BaseEntity;
use App\Model\Model;
use Hashids\Hashids;
use Nette\Application\BadRequestException;
use Nette\Application\Responses;
use Nextras\Orm\Collection\ICollection;
use PdfResponse\PdfResponse;
use Titan\Model\Helper;
use Titan\Utils\ApiHelper;
use Titan\Utils\Mailer;
use Titan\Utils\Translator;

/** @deprecated */
abstract class BasePresenter extends \Nette\Application\UI\Presenter
{
    protected \Titan\Utils\User $user;

    /**
     * @var mixed|null
     */
    protected mixed $account = null;

    /**
     * @inject
     */
    public Model $model;

    public \Titan\Services\AclService $acl;

    protected Mailer $mailer;

    protected bool $renderTemplate = false;

    protected ?Translator $translator = null;

    protected Hashids $hashIds;

    /** @var BasePresenter */
    protected static $staticPresenter;

    /**
     * @throws BadRequestException
     */
    public function startup()
    {
        parent::startup();
        $this->resolveOptions();
        $this->setUser();
        $this->setAcl();
        $this->setMailer();

        $this->hashIds = new Hashids($this->getConfigParameter('hashIds'), 55);
        $this->model->setHashIds($this->hashIds);
        self::$staticPresenter = $this;

        $this->model->lang = $this->getLocale();
        $this->model->link = function (string $destination, array $args): string {
            return $this->link($destination, $args);
        };

        if ($this->user->isLoggedIn() === true) {
            $this->model->userId = $this->getAccount()->id;
        }
    }


    protected function setMailer(): void
    {
        $this->mailer = new Mailer(
            $this->getConfigParameter('mailer'),
            $this->model->template,
            $this->getConfigParameter('appDir') . '/templates/mail.latte'
        );
    }


    protected function setUser(): void
    {
        $this->user = new \Titan\Utils\User(
            $this->model->user,
            $this->model->userToken,
            $this->getConfigParameter('jwtSecret'),
            $this->getConfigParameter('jwtName', 'jwt'),
            $this->getConfigParameter('debugMode') === false
        );
    }


    protected function setAcl(): void
    {
        $this->acl = new \Titan\Services\AclService($this->user->getAccount(), $this->model->acl);
    }


    /**
     * Action for OPTIONS method
     * Just run startup to send access-control header and terminate
     */
    public function resolveOptions(): void
    {
        if (strtoupper($this->request->getMethod()) === 'OPTIONS') {
            exit;
        }
    }


    /**
     * @throws BadRequestException
     */
    public function beforeRender(): void
    {
        if ($this->renderTemplate === false) {
            throw new BadRequestException('Page not found', 404);
        }
    }


    /**
     * @throws BadRequestException
     */
    public function sendJson($data, null|array|string $method = 'toMain', int $statusCode = 200): void
    {
        $this->getHttpResponse()->setCode($statusCode);
        $this->sendResponse(new Responses\JsonResponse(ApiHelper::prepareResponse($data, $method)));
    }

    /**
     * @throws BadRequestException
     */
    public function sendPaginatedListJson(
        ICollection $collection,
        int $page = 1,
        int $perPage = 10,
        array|string $method = 'toMain'
    ): void
    {
        $this->sendJson($this->paginateList($collection, $page, $perPage, $method));
    }

    protected function paginateList(
        ICollection $collection,
        int $page = 1,
        int $perPage = 10,
        array|string $method = 'toMain'
    ): array
    {
        return Helper::paginateList($collection, $page, $perPage, $method);
    }


    /**
     * @return \App\Model\User\User|null
     * @throws BadRequestException
     */
    public function getAccount(): ?\App\Model\User\User
    {
        if (is_null($this->account)) {
            $this->account = $this->user->getAccount();
        }
        return $this->account;
    }


    /**
     * @throws BadRequestException
     */
    public function loggedOnly(): void
    {
        $this->getAccount();
    }


    /**
     * @param string $parameter
     * @param null $default
     * @return mixed
     */
    protected function getConfigParameter(string $parameter, $default = NULL)
    {
        $parameters = $this->context->getParameters();
        return array_key_exists($parameter, $parameters) ? $parameters[$parameter] : $default;
    }


    /**
     * @param string $parameter
     * @param null $default
     * @return mixed
     */
    protected function getJsonBodyParameter(string $parameter, $default = null)
    {
        $data = $this->getJsonBody([$parameter], false);
        return array_key_exists($parameter, $data) ? $data[$parameter] : $default;
    }


    /**
     * @param array|null $params
     * @param bool $nullUndefined
     * @return array
     */
    protected function getJsonBody(array $params = NULL, bool $nullUndefined = false): array
    {
        $body = $this->getHttpRequest()->getRawBody();
        $data = json_decode($body, true);
        $data = is_array($data) ? $data : [];

        if (is_null($params)) {
            return $data;
        }
        $out = [];
        foreach ($params as $param) {
            if (array_key_exists($param, $data) !== false) {
                $out[$param] = $data[$param];
            } elseif ($nullUndefined === true) {
                $out[$param] = NULL;
            }
        }
        return $out;
    }


    /**
     * @param string $dirName
     * @return string
     */
    protected function getTempDir(string $dirName): string
    {
        $dir = $this->getConfigParameter('tempDir') . '/' . $dirName . '/';
        if (!is_dir($dir)) {
            mkdir($dir);
        }
        return $dir;
    }


    /**
     * @param string $dirName
     * @return string
     */
    protected function getFileDir(string $dirName): string
    {
        $dir = $this->getConfigParameter('appDir') . '/../../file/' . $dirName . '/';
        if (!is_dir($dir)) {
            mkdir($dir);
        }
        return $dir;
    }


    /**
     * @return string
     */
    protected function getLocale(): string
    {
        return Translator::getLanguage(self::getDefaultLanguage());
    }


    /**
     * @param null|string $locale
     * @param bool $reload
     * @return Translator
     */
    protected function getTranslator(?string $locale = null, bool $reload = false): \Nette\Localization\ITranslator
    {
        if (is_null($this->translator) || $reload === true) {
            $locale = is_null($locale) ? $this->getLocale() : $locale;
            $this->translator = new Translator($this->model, $locale);
        }
        return $this->translator;
    }


    /**
     * @param string $message
     * @param int|null $count
     * @return string
     */
    public function i18n(string $message, ?int $count = null): string
    {
        return $this->getTranslator()->translate($message, $count);
    }


    /**
     * @param string $hash
     * @param int|null $timeParameter
     * @param string $message
     * @return array
     * @throws BadRequestException
     */
    protected function validateHash(string $hash, int $timeParameter = null, string $message = 'Link expired'): array
    {
        $params = $this->hashIds->decode($hash);
        if (empty($params) || (!is_null($timeParameter) && $params[$timeParameter] <= time())) {
            throw new BadRequestException($message, 403);
        }
        return $params;
    }


    /**
     * @param string $template
     * @param string $documentTitle
     * @throws \Nette\Application\AbortException
     */
    public function renderPdf(string $template, string $documentTitle)
    {
        $tempDir = $this->getTempDir('mpdf');

        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        define('_MPDF_TTFONTDATAPATH', $tempDir);
        $pdf = new PdfResponse($template);
        $pdf->tempDir = $tempDir;
        $pdf->pageMargins = '8,8,8,8,9,9';
        $pdf->pageFormat = "A4";
        $pdf->displayLayout = "continuous";
        $pdf->displayZoom = "fullwidth";
        $pdf->documentTitle = $documentTitle;
        $pdf->documentAuthor = 'Titancms';
        $this->sendResponse($pdf);
    }


    /**
     * @param string $destination
     * @param array $args
     * @return string
     * @throws \Nette\Application\UI\InvalidLinkException
     */
    public static function generateLink(string $destination, array $args): string
    {
        return self::$staticPresenter->link($destination, $args);
    }


    public static function getLanguage(): string
    {
        return self::$staticPresenter->getLocale();
    }

    public static function getDefaultLanguage(): string
    {
        return 'en';
    }
}
