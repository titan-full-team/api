<?php

namespace Titan\Presenter;

use Nette\Application\AbortException;
use Nette\Application\BadRequestException;
use Nextras\Orm\Collection\ICollection;
use PdfResponse\PdfResponse;
use Titan\Model\Helper;
use Titan\Utils\ApiHelper;
use Titan\Utils\JsonResponse;

trait TitanPresenterTrait
{
    protected bool $renderTemplate = false;

    /** @inject */
    public JsonResponse $json;

    public function startup()
    {
        parent::startup();
        $this->resolveOptions();
    }

    /**
     * @throws BadRequestException|AbortException
     */
    public function beforeRender(): void
    {
        if ($this->renderTemplate === false) {
            if (empty($this->json->getData())) {
                throw new BadRequestException('Page not found', 404);

            }

            $this->getHttpResponse()->setCode($this->json->getStatus());
            $this->sendResponse($this->json);
        }
    }

    /**
     * @deprecated
     * @throws BadRequestException
     */
    protected function serialize($data, string $method = 'toMain'): array
    {
        return ApiHelper::prepareResponse($data, $method);
    }

    public function renderPdf(string $tempDir, string $template, string $documentTitle): void
    {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        define('_MPDF_TTFONTDATAPATH', $tempDir);
        $pdf = new PdfResponse($template);
        $pdf->tempDir = $tempDir;
        $pdf->pageMargins = '8,8,8,8,9,9';
        $pdf->pageFormat = "A4";
        $pdf->displayLayout = "continuous";
        $pdf->displayZoom = "fullwidth";
        $pdf->documentTitle = $documentTitle;
        $pdf->documentAuthor = 'Titancms';
        $this->sendResponse($pdf);
    }


    /**
     * Action for OPTIONS method
     * Just run startup to send access-control header and terminate
     */
    public function resolveOptions(): void
    {
        if (strtoupper($this->request->getMethod()) === 'OPTIONS') {
            exit;
        }
    }

    /**
     * @deprecated
     * @throws BadRequestException
     */
    public function sendJson($data, null|array|string $method = 'toMain', int $statusCode = 200): void
    {
        $this->json->array(ApiHelper::prepareResponse($data, $method));
        $this->json->setStatus($statusCode);
    }

    /**
     * @deprecated
     */
    public function sendPaginatedListJson(
        ICollection $collection,
        int $page = 1,
        int $perPage = 10,
        array|string $method = 'toMain'
    ): void
    {
        $this->json->paginatedCollection($collection, $page, $perPage, $method);
    }

    /**
     * @deprecated
     */
    protected function paginateList(
        ICollection $collection,
        int $page = 1,
        int $perPage = 10,
        array|string $method = 'toMain'
    ): array
    {
        return Helper::paginateList($collection, $page, $perPage, $method);
    }
}