<?php
/**
 * Created by PhpStorm.
 * User: norbertspanik
 * Date: 2019-03-08
 * Time: 14:04
 */

namespace Titan\Generator\Swagger\Entity;

use Nette\Application\BadRequestException;

abstract class EntitySchema
{
    public string $foundInPresenter;

    /**
     * @throws BadRequestException
     */
    protected function createEntity()
    {
        try {
            return new $this->entity();
        } catch (\Error $error) {
            throw new BadRequestException('Entity ' . $this->entity . ' not found in presenter ' . $this->foundInPresenter, 501);
        }
    }
}