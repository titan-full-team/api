<?php

namespace Titan\Utils\Input;

class InputManyHasOne extends InputInteger implements IInput
{
    protected function setValue(IInput $instance, string $name, array $data): void
    {
        array_key_exists($name, $data) && is_array($data[$name])
            ? parent::setValue($instance, 'id', $data[$name])
            : parent::setValue($instance, $name, $data);
    }
}