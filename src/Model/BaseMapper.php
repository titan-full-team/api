<?php

namespace Titan\Model;

use App\Model\Model;
use Nextras\Dbal\QueryBuilder\QueryBuilder;
use Nextras\Orm\Collection\Helpers\DbalQueryBuilderHelper;
use Nextras\Orm\Collection\ICollection;
use Nextras\Orm\Entity\IEntity;
use Nextras\Orm\Entity\Reflection\EntityMetadata;
use Nextras\Orm\Entity\Reflection\PropertyMetadata;
use Titan\Model\Orm\Mapper;
use Titan\Model\Orm\TitanCollection;

abstract class BaseMapper extends Mapper
{
    /** @var DbalQueryBuilderHelper */
    protected $helper;


    /**
     * @return QueryBuilder
     */
    public function builder(): QueryBuilder
    {
        $builder = parent::builder();
        foreach ($this->getEntityMetadata()->getProperties() as $property) {
            if ($property->encrypted === true) {
                $propertyExpr = $this->getHelper()->processPropertyExpr($builder, $property->name);
                $builder->addSelect("%ex AS %s", $propertyExpr->args, $property->name);
            }
        }
        return $builder;
    }


    /**
     * @return EntityMetadata
     */
    protected function getEntityMetadata(): EntityMetadata
    {
        $entityName = $this->getRepository()->getEntityClassName([]);
        /** @var IEntity $entity */
        $entity = new $entityName();
        return $entity->getMetadata();
    }


    /**
     * @return DbalQueryBuilderHelper
     */
    protected function getHelper(): DbalQueryBuilderHelper
    {
        if ($this->helper === null) {
            $repository = $this->getRepository();
            $this->helper = new DbalQueryBuilderHelper($repository->getModel(), $repository, $this);
        }
        return $this->helper;
    }


    /**
     * @return string
     */
    public static function getEncryptionHash(): string
    {
        return 'HFrUldo1sjxLtFvYPxecHFrUldo1ec';
    }


    /**
     * @return \Nextras\Dbal\IConnection
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @return Model|\Nextras\Orm\Model\IModel
     */
    public function getModel(): Model
    {
        return $this->getRepository()->getModel();
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @return array|\Nextras\Dbal\Result\Row[]
     */
    public function makeQuery(QueryBuilder $queryBuilder)
    {
        return $this->getConnection()->queryByQueryBuilder($queryBuilder)->fetchAll();
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @return int
     */
    public function makeCountQuery(QueryBuilder $queryBuilder): int
    {
        return $this->getConnection()->queryByQueryBuilder($queryBuilder)->count();
    }

    /**
     * @param IEntity|BaseEntity $entity
     * @return array
     */
    protected function entityToArray(\Nextras\Orm\Entity\IEntity $entity): array
    {
        $out = parent::entityToArray($entity);

        /** @var PropertyMetadata $property */
        foreach ($entity->getMetadata()->getProperties() as $property) {
            if ($property->encrypted === true && array_key_exists($property->name, $out)) {
                $out[$property->name] = [
                    'value' => $out[$property->name],
                    '_titanEncrypt' => 'AES',
                ];
            }
        }
        return $out;
    }
}