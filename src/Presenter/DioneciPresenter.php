<?php
/**
 * Created by PhpStorm.
 * User: norbertspanik
 * Date: 9/20/14
 * Time: 10:21 PM
 */

namespace Titan\Presenter;


use Nette\Application\BadRequestException;
use Titan\Utils\Dioneci;

/** @deprecated */
class DioneciPresenter extends CorePresenter
{
    /**
     * @var Dioneci
     */
    protected $dioneci;

    /**
     * @throws \Exception
     */
    public function startup()
    {
        parent::startup();
        $this->dioneci = new Dioneci($this->getDioneciConfigParameter('authKey'));
        $this->dioneci->auth($this->getAuthorization());
    }

    /**
     * todo: swagger
     * @out-200 []
     * @methods get
     * @swagger-security DioneCi
     */
    public function actionLogList()
    {
        $this->sendJson($this->dioneci->getFileList($this->getLogFolder(), ['warning', 'error', 'exception']));
    }

    /**
     * @throws \Nette\Application\AbortException
     * todo: swagger
     * @out-200 []
     * @methods get
     * @swagger-security DioneCi
     */
    public function actionStatus()
    {
        $this->sendJson($this->getStatusData());
    }

    /**
     * @return array
     * todo: swagger
     * @out-200 []
     * @methods get
     */
    protected function getStatusData(): array
    {
        return [
            'online' => true,
            'warnings' => sizeof($this->dioneci->getFileList($this->getLogFolder(), ['warning'])),
            'errors' => sizeof($this->dioneci->getFileList($this->getLogFolder(), ['error', 'exception'])),
        ];
    }

    /**
     * @param string $logName
     * @throws BadRequestException
     * @throws \Nette\Application\AbortException
     * todo: swagger
     * @out-200 []
     * @methods get
     * @swagger-security DioneCi
     */
    public function actionLog(string $logName)
    {
        $fileData = $this->dioneci->getFile(($this->getLogFolder() . $logName), $logName);
        if ($fileData === false) {
            throw new BadRequestException('Log not found', 404);
        }
        $this->sendJson($fileData);
    }

    /**
     * @throws \Nette\Application\AbortException
     * todo: swagger
     * @out-200 []
     * @methods delete
     * @swagger-security DioneCi
     */
    public function actionDeleteCache()
    {
        $cacheDir = $this->context->getParameters()['tempDir'] . '/cache';
        $this->dioneci->deleteDir($cacheDir);
        $this->sendJson(['status' => 'success']);
    }

    /**
     * @throws \Nette\Application\AbortException
     * todo: swagger
     * @out-200 []
     * @methods delete
     * @swagger-security DioneCi
     */
    public function actionDeleteLogs()
    {
        $this->dioneci->deleteFileList($this->getLogFolder(), ['warning', 'error', 'exception']);
        $this->sendJson(['status' => 'success']);
    }

    /**
     * @return string
     */
    protected function getLogFolder(): string
    {
        $parameters = $this->context->getParameters();
        return $parameters['appDir'] . '/../log/';
    }

    /**
     * @return string
     * @throws BadRequestException
     */
    protected function getAuthorization(): string
    {
        $auth = $this->getHttpRequest()->getHeader('Authorization');
        if (is_null($auth) && !isset($_SERVER['Authorization'])) {
            throw new BadRequestException('Unauthorized, Authorization code is missing', 401);
        }
        return is_null($auth) ? $_SERVER['Authorization'] : $auth;
    }

    /**
     * @param string $parameter
     * @param null $default
     * @return null
     */
    protected function getDioneciConfigParameter(string $parameter, $default = NULL)
    {
        $parameters = $this->context->getParameters();
        if (isset($parameters['dioneci']) && isset($parameters['dioneci'][$parameter])) {
            return $parameters['dioneci'][$parameter];
        }
        return $default;
    }
}
