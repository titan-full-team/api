<?php

namespace Titan\Presenter;

use App\Model\User\User;
use Nette\Application\BadRequestException;
use Nette\Utils\Random;
use Nextras\Dbal\UniqueConstraintViolationException;
use Titan\Model\Helper;
use Titan\Utils\Auth\Credentials;

/** @deprecated */
class UserPresenter extends CorePresenter
{
    /**
     * @var string
     */
    protected $permissionName = 'user';

    /**
     * @throws BadRequestException
     * @throws \Exception
     * @out-200 User::toGetArray
     * @methods post
     */
    public function actionCreate()
    {
        $this->acl->checkSectionPermission($this->permissionName, 'c');
        /** @var User $entity */
        $entity = $this->model->user->patchNewEntity($this->getJsonBody());

        $password = Random::generate(10);

        /** @var Credentials $authClass */
        $authClass = $this->user->getAuthClass(\Titan\Utils\User::USER_DEFAULT_AUTH_CLASS);
        $authClass->generatePassword($entity, $password);

        try {
            $this->model->user->persistAndFlush($entity);
            $this->model->refreshAll(true);
            $this->mailer->send('newuser', $entity->login, [$entity], ['password' => $password]);

            $this->sendJson($entity->toGetArray());
        } catch (UniqueConstraintViolationException $exception) {
            throw new BadRequestException('Login already exists', 409);
        }
    }

    /**
     * @param int $page
     * @param int $perPage
     * @throws BadRequestException
     * @out-200 User::toArray
     * @methods POST,GET
     */
    public function actionList(int $page = 1, int $perPage = 10)
    {
        $this->acl->checkSectionPermission($this->permissionName, 'r');
        $data = $this->model->user->findList($this->getJsonBody());
        $this->sendPaginatedListJson($data, $page, $perPage);
    }

    /**
     * @param int $id
     * @throws BadRequestException
     * @throws \Exception
     * @out-200 User::toGetArray
     * @methods get
     */
    public function actionDetail(int $id)
    {
        $this->acl->checkSectionPermission($this->permissionName, 'r');
        $this->sendJson($this->model->user->getByIdWithValidation($id)->toGetArray());
    }

    /**
     * @param int $id
     * @throws \Exception
     * @out-200 User::toGetArray
     * @methods put
     */
    public function actionUpdate(int $id)
    {
        $this->acl->checkSectionPermission($this->permissionName, 'u');
        /** @var User $entity */
        $entity = $this->model->user->patchEntity(
            $this->getJsonBody(),
            $this->model->user->getByIdWithValidation($id)
        );
        try {
            $this->model->user->persistAndFlush($entity);
            $this->sendJson($entity->toGetArray());
        } catch (UniqueConstraintViolationException $exception) {
            throw new BadRequestException('Login already exists', 409);
        }
    }

    /**
     * @param int $id
     * @throws \Exception
     * @out-200 User was deleted
     * @methods delete
     */
    public function actionDelete(int $id)
    {
        $this->acl->checkSectionPermission($this->permissionName, 'd');
        $entity = $this->model->user->getByIdWithValidation($id);
        $this->model->remove($entity, false);
        $this->model->flush();
        $this->sendJson('User was deleted');
    }
}
