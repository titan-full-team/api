<?php

namespace Titan\Model;

use App\Model\Model;
use Nette\Application\UI\InvalidLinkException;
use Nextras\Orm\Collection\DbalCollection;
use Nextras\Orm\Collection\ICollection;
use Nextras\Orm\Entity\Reflection\PropertyMetadata;
use Nextras\Orm\Entity\ToArrayConverter;
use Nextras\Orm\Model\IModel;
use Titan\Model\Orm\ITitanCollection;
use Titan\Presenter\CorePresenter;

abstract class BaseEntity extends \Nextras\Orm\Entity\Entity
{
    /**
     * @var array
     */
    public $notAllowedParameters = [];


    /**
     * @var array
     */
    public $codeBookSort = ['name' => 'ASC'];


    /**
     * @var array
     */
    public $entityAddOnly = [];


    /**
     * @var array
     */
    public $saveFromParent = [];


    /**
     * @return array
     */
    public function toCodeBookArray(): array
    {
        return Helper::entityToArray($this);
    }


    /**
     * @param int $mode
     * @return array
     */
    public function toArray(int $mode = ToArrayConverter::RELATIONSHIP_AS_IS): array
    {
        return $this->toMain();
    }


    /**
     * @param array $contain
     * @param array|null $notAllowedParameters
     * @return array
     */
    public function toMain(array $contain = [], array $notAllowedParameters = null): array
    {
        return Helper::entityToArray(
            $this,
            $contain,
            is_null($notAllowedParameters) ? $this->notAllowedParameters : $notAllowedParameters
        );
    }


    /**
     * @return array
     */
    protected function entityToArray(array $contain = []): array
    {
        return Helper::entityToArray($this, $contain, $this->notAllowedParameters);
    }


    /**
     * @param DbalCollection|ITitanCollection|ICollection $collection
     * @param string $method
     * @return array
     */
    protected function collectionToArray(DbalCollection $collection, string $method = 'toMain'): array
    {
        return Helper::collectionToArray($collection, $method);
    }


    /**
     * @return Model|IModel
     */
    protected function getModel(): Model
    {
        return $this->getRepository()->getModel();
    }

    /**
     * @param string $parameter
     * @param mixed|null $default
     * @return mixed
     */
    protected function getTranslatedValue(string $parameter, $default = null)
    {
        $translateEntity = $this->{$parameter}->get()->getBy(['lang' => CorePresenter::getLanguage()]);
        return is_null($translateEntity) ? $default : $translateEntity->value;
    }


    /**
     * @param string $destination
     * @param $params
     * @return mixed
     * @throws InvalidLinkException
     */
    protected function link(string $destination, $params)
    {
        return CorePresenter::generateLink($destination, $params);
    }


    /**
     * @param array $out
     * @return array
     */
    public function appendLocaleParameters(array $out = [], array $ignore = []): array
    {
        foreach ($this->getMetadata()->getProperties() as $property) {
            if ($this->isPropertyLocale($property) === true && in_array($property->name, $ignore) === false) {
                $out[] = $property->name;
            }
        }
        return $out;
    }


    /**
     * @param array $out
     * @return array
     */
    public function getTranslations(array $out = []): array
    {
        /** @var \Nextras\Orm\Entity\Reflection\PropertyMetadata $property */
        foreach ($this->getMetadata()->getProperties() as $property) {
            if ($this->isPropertyLocale($property) === true) {
                foreach ($this->{$property->name} as $value) {
                    $propertyName = strtolower(substr($property->name, 6));
                    $out[EntityGenerator::LOCALE_KEY][$value->lang][$propertyName] = $value->value;
                }
            }
        }
        return $out;
    }


    /**
     * @param PropertyMetadata $propertyMetadata
     * @return bool
     */
    protected function isPropertyLocale(PropertyMetadata $propertyMetadata): bool
    {
        $localeKey = 'key-table-column-lang';
        return substr($propertyMetadata->name, 0, 6) === 'locale'
            && $propertyMetadata->relationship !== null
            && implode('-', $propertyMetadata->relationship->entityMetadata->getPrimaryKey()) === $localeKey;
    }
}