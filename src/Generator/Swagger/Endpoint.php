<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 04/11/2018
 * Time: 10:46 AM
 */

namespace Titan\Generator\Swagger;


use Titan\Generator\Scanner;

class Endpoint
{
    protected array $presenters = [];

    public function __construct(string $dir)
    {
        $this->presenters = Scanner::findPresenters($dir);
    }

    /**
     * @throws \Exception
     */
    public function paths(array $swaggerConfig): array
    {
        $out = [];
        foreach ($this->presenters as $presenter) {
            $presenter = new Presenter($presenter);
            if ($presenter->isAbstract() === false && $presenter->isDisabled() === false) {
                $out = array_merge($out, $presenter->generator($swaggerConfig));
            }
        }
        return $out;
    }

    /**
     * @throws \Exception
     */
    public function getRequiredModels(): array
    {
        $out = [];
        foreach ($this->presenters as $presenter) {
            $presenter = new Presenter($presenter);
            $models = $presenter->getRequiredModels();
            foreach ($models as $key => $model) {
                $out[$key] = $model;
            }
        }
        return $out;
    }

    /**
     * @throws \Exception
     */
    public function tags(): array
    {
        $out = [];
        foreach ($this->presenters as $presenter) {
            $presenter = new Presenter($presenter);
            if ($presenter->isAbstract() === false && $presenter->isDisabled() === false) {
                $out[] = $presenter->getTag();
            }
        }

        return $out;
    }
}
