<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 08/11/2018
 * Time: 11:06 PM
 */

namespace Titan\Generator;

use Nette\PhpGenerator\ClassType;
use Nette\Reflection\Method;
use ReflectionClass;

class Scanner
{
    public static function findPresenters(
        string $dir,
        array $out = [],
        array $ignoredPresenters = ['ErrorPresenter.php', 'GeneratorPresenter.php']
    ): array
    {
        foreach (scandir($dir) as $item) {
            if (!str_starts_with($item, '.')) {
                if (is_dir($item)) {
                    return self::findPresenters($dir . '/' . $item, $out);
                } else if (in_array($item, $ignoredPresenters) === false) {
                    $out[] = $dir . '/' . $item;
                }
            }
        }
        return $out;
    }

    /**
     * @throws \ReflectionException
     */
    public static function getClassTypeFromFile(string $file): ClassType
    {
        return ClassType::from(self::getClassNameFromFile($file));
    }

    /**
     * @param string $file
     * @return string
     */
    public static function getClassNameFromFile(string $file)
    {
        $fileData = self::getSourceCode($file);

        preg_match('#namespace (.*);#', $fileData, $matches);
        $namespace = $matches[1];

        preg_match('#class ([a-zA-Z0-9]*)#', $fileData, $matches);
        $className = $matches[1];

        return '\\' . $namespace . '\\' . $className;
    }

    public static function getSourceCode(string $file): string
    {
        $f = fopen($file, 'r') or die('Unable to open file!');
        $fileData = fread($f, filesize($file));
        fclose($f);
        return $fileData;
    }

    /**
     * @throws \ReflectionException
     */
    public static function getUsesOfFile(string $file): array
    {
        return self::getUsesOfClass(self::getClassNameFromFile($file));
    }

    /**
     * @throws \ReflectionException
     */
    public static function getUsesOfClass(string $className, array $out = []): array
    {
        if (stristr($className, 'Nette\Application\UI\Presenter') === false) {
            $annotatedClass = new ReflectionClass($className);
            $fileData = Scanner::getSourceCode($annotatedClass->getFileName());

            // set uses
            $sourceCode = substr($fileData, 0, strpos($fileData, 'class'));
            preg_match_all('#use (.*);#', $sourceCode, $matches);
            $out = array_unique(array_merge($out, $matches[1]));

            // check uses of extends classes
            $classType = ClassType::from($className);
            return self::getUsesOfClass($classType->getExtends(), $out);
        }
        return $out;
    }

    public static function getMethodNonCommentedBody(\Nette\PhpGenerator\Method $method): string
    {
        return $method->getBody();
    }
}