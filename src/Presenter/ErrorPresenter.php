<?php

namespace Titan\Presenter;

use Nette;
use Titan\Errors\UserException;
use Titan\Utils\ApiHelper;

/** @deprecated */
class ErrorPresenter extends BasePresenter
{
    /**
     * @var string
     */
    protected $templateFile = __DIR__ . '/../Templates/user-error.latte';


    /**
     * @param $exception
     * @return string
     * @throws Nette\Application\AbortException
     * @throws Nette\Application\BadRequestException
     */
    public function actionDefault($exception)
    {
        if ($exception instanceof UserException) {
            $template = $this->createTemplate();
            $template->setTranslator($this->getTranslator());
            $template->setFile($this->templateFile);
            $template->exception = $exception;
            echo (string)$template;
            $this->terminate();
            exit;
        } else {
            $exception = ApiHelper::handleError($exception);
            $this->sendJson($exception, 'toMain', $exception['code']);
        }
    }

}
