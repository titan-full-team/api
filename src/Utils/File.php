<?php

namespace Titan\Utils;

use JetBrains\PhpStorm\NoReturn;
use Nette\Application\BadRequestException;
use Nette\Utils\ImageException;
use Titan\Services\FileService;

/**
 * @deprecated
 */
class File
{
    /**
     * @throws BadRequestException
     */
    #[NoReturn] public static function printFile(string $folder, string $name)
    {
        FileService::printFile($folder, $name);
    }

    public static function cleanFolderName(string $folder = null):? string
    {
        return FileService::cleanFolderName($folder);
    }

    /**
     * @throws BadRequestException
     * @throws \Nette\Utils\UnknownImageFileException
     * @throws ImageException
     */
    #[NoReturn] public static function printResizedFile(
        string $folder,
        string $name,
        int $width = null,
        int $height = null,
        string $mode = null,
        bool $webp = false
    ): void
    {
        FileService::printResizedFile($folder, $name, strtotime('2023-08-01 10:00:00'), $width, $height, $mode, $webp);
    }


    /**
     * @param string $file
     * @param string $content
     */
    public static function createFile(string $file, string $content): void
    {
        FileService::createFile($file, $content);
    }


    /**
     * @param string $dir
     * @param string $base64File
     * @param string|null $fileName
     * @return array
     */
    public static function saveBase64file(string $dir, string $base64File, string $fileName = null): array
    {
        return FileService::saveBase64file($dir, $base64File, $fileName);
    }


    /**
     * @param string $dir
     * @param string $base64File
     * @param string $fileName
     * @param string|null $subFolder
     * @return array
     */
    public static function saveFile(string $dir, string $base64File, string $fileName, string $subFolder = null): array
    {
        return FileService::saveFile($dir, $base64File, $fileName, $subFolder);
    }
}
