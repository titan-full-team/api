<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 2019-04-05
 * Time: 07:45
 */

namespace Titan\Utils;

use App\Model\Translate\Translate;
use Tracy\Debugger;
use Tracy\ILogger;

class Translator implements \Nette\Localization\ITranslator
{
    /**
     * @var array
     */
    protected $translations = [];

    /**
     * @var \App\Model\Model
     */
    protected $model;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $locale;

    /**
     * @param \App\Model\Model $model
     * @param string $locale
     * @param string $type
     */
    public function __construct(\App\Model\Model $model, string $locale, string $type = 'API')
    {
        $this->type = $type;
        $this->model = $model;
        $this->locale = $locale;
        $this->translations = $model->translateLocale
            ->findBy(['translate->type' => $type])
            ->findBy(['lang' => $locale])
            ->fetchPairs('translate->slug');
    }


    /**
     * @param string|null $default
     * @return string|null
     */
    public static function getLanguage(?string $default = 'gb'):? string
    {
        return $_GET['lang'] ?? $default;
    }


    /**
     * todo:
     * @param mixed $message
     * @param int|null ...$parameters
     * @return string
     */
    public function translate($message, ...$parameters): string
    {
        $count = array_key_exists(0, $parameters) ? $parameters[0] : null;
        if (isset($this->translations[$message])
            && !empty($this->translations[$message])
            && !empty($this->translations[$message]->value)
        ) {
            return $this->doPluralization($this->translations[$message]->value, $count);
        } else if (is_null($this->model->translate->getBy(['slug' => $message]))) {
            $translateEntity = new Translate();
            $translateEntity->type = $this->type;
            $translateEntity->html = false;
            $translateEntity->slug = $message;
            $this->model->translate->persistAndFlush($translateEntity);
        }
        return $message;
    }

    /**
     * @param string $value
     * @param int|null $count
     * @return string
     */
    protected function doPluralization(string $value, int $count = null): string
    {
        if (!is_null($count)) {
            try {
                $translates = explode('|', $value);

                if (in_array($this->locale, ['sk', 'cz', 'cs']) === true) {
                    if (substr_count($value, '|') === 3) {
                        if ($count === 0) {
                            return trim($translates[0]);
                        } else if ($count === 1) {
                            return trim($translates[1]);
                        } else if ($count >= 2 && $count < 5) {
                            return str_replace('{count}', $count, trim($translates[2]));
                        } else {
                            return str_replace('{count}', $count, trim($translates[3]));
                        }
                    }
                } else if (substr_count($value, '|') === 2) {
                    if ($count === 0) {
                        return trim($translates[0]);
                    } else if ($count === 1) {
                        return trim($translates[1]);
                    }
                    return str_replace('{count}', $count, trim($translates[2]));
                }
            } catch (\Throwable $exception) {
                Debugger::log("Cannot translate string $value, count is $count", ILogger::WARNING);
            }
        }
        return $value;
    }
}