<?php

namespace Titan\Utils\Input;

use Nette\Application\BadRequestException;

class InputBoolean implements IInput
{
    use InputTrait;

    protected function __construct()
    {
        $this->type = 'boolean';
    }
}