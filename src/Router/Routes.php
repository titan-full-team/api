<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 08/11/2018
 * Time: 11:03 PM
 */

namespace Titan\Router;

use Titan\Generator\Scanner;

class Routes
{
    /**
     * @var array
     */
    protected $routes = [];

    /**
     * @param string $presenterDir
     * @throws \Exception
     */
    public function __construct(string $presenterDir)
    {
        $this->initList($presenterDir);
    }

    /**
     * @param string $presenterDir
     * @throws \Exception
     */
    protected function initList(string $presenterDir)
    {
        $presenters = Scanner::findPresenters($presenterDir);

        foreach ($presenters as $presenter) {
            if (is_file($presenter)) {
                $class = Scanner::getClassTypeFromFile($presenter);

                if ($class->isAbstract() === false) {
                    foreach ($class->getMethods() as $method) {
                        $route = new Route($method, $class->getName());

                        if (!is_null($route->getMask())) {
                            $this->routes[] = $route;
                        }
                    }
                }
            }
        }
    }

    /**
     * @return array
     */
    public function getRoutes(): array
    {
        return $this->routes;
    }
}