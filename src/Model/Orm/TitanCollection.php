<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 08/02/2019
 * Time: 9:27 PM
 */

namespace Titan\Model\Orm;

use App\Model\BaseMapper;
use Nextras\Orm\Collection\DbalCollection;
use Nextras\Orm\Collection\ICollection;
use Nextras\Orm\Entity\IEntity;
use Titan\Model\Functions\LikeFilterFunction;
use Titan\Model\SmartCondition;
use Titan\Model\SmartSort;

class TitanCollection extends DbalCollection implements ICollection
{
    /** @var float|null */
    protected $resultSum;


    /** @var float|null */
    protected $resultAvg;


    /**
     * todo: parameters
     * @param string $condition
     * @param $parameters
     * @return ICollection|ITitanCollection
     */
    public function whereSql(string $condition, $parameters): ICollection
    {
        $this->queryBuilder->andWhere($condition, $parameters);
        return $this;
    }

    /**
     * @param callable $callback
     * @return ICollection|ITitanCollection
     */
    public function customSql(callable $callback): ICollection
    {
        call_user_func($callback, $this->queryBuilder);
        return $this;
    }


    /**
     * @return int
     * @throws \Nextras\Dbal\QueryException
     */
    protected function getIteratorCount(): int
    {
        return $this->getAggregationIterator('count', '*', 0);
    }


    /**
     * @param string $column
     * @param string $value
     * @param string $type
     * @param int $minWordLength
     * @param string $conjunction
     * @param bool $explodeSearchPhrase
     * @return ICollection|ITitanCollection
     */
    public function findNotLike(
        string $column,
        string $value,
        string $type = '%_like_',
        int $minWordLength = 3,
        string $conjunction = 'AND',
        bool $explodeSearchPhrase = true
    ): ICollection
    {
        return $this->findBy([
                LikeFilterFunction::class,
                $column,
                $value,
                $type,
                $minWordLength,
                $conjunction,
                $explodeSearchPhrase,
                'NOT LIKE'
            ]
        );
    }


    /**
     * @param string $column
     * @param string $value
     * @param string $type
     * @param int $minWordLength
     * @param string $conjunction
     * @param bool $explodeSearchPhrase
     * @return ICollection|ITitanCollection
     */
    public function findLike(
        string $column,
        string $value,
        string $type = '%_like_',
        int $minWordLength = 3,
        string $conjunction = 'AND',
        bool $explodeSearchPhrase = true
    ): ICollection
    {
        return $this->findBy([
            LikeFilterFunction::class,
            $column,
            $value,
            $type,
            $minWordLength,
            $conjunction,
            $explodeSearchPhrase,
            'LIKE'
        ]);
    }


    /**
     * @param $parameters
     * @return ICollection|ITitanCollection
     * @deprecated use groupBySql instead
     */
    public function groupBy($parameters): ICollection
    {
        return $this->groupBySql($parameters);
    }


    /**
     * @param string $parameters
     * @return ICollection|ITitanCollection
     */
    public function groupBySql(string $parameters): ICollection
    {
        $this->queryBuilder->groupBy($parameters);
        return $this;
    }


    /**
     * @param string $condition
     * @param $parameters
     * @return ICollection|ITitanCollection
     */
    public function where(string $condition, $parameters): ICollection
    {
        return $this->findBy([$condition => $parameters]);
    }


    /**
     * @param array $conditions
     * @param array $disabledProperties
     * @return ICollection|ITitanCollection
     * @throws \Nette\Application\BadRequestException
     */
    public function findByApi(array $conditions, array $disabledProperties = []): ICollection
    {
        $collection = clone $this;
        if (!empty($conditions)) {
            $collection = SmartCondition::set($collection, $conditions, $this->getEntityClassName(), $disabledProperties);
        }
        return $collection;
    }

    /**
     * @param array $sort
     * @param array $disabledProperties
     * @return ICollection|TitanCollection
     * @throws \Nette\Application\BadRequestException
     */
    public function orderByApi(array $sort, array $disabledProperties = []): ICollection
    {
        return SmartSort::set(clone $this, $sort, $this->getEntityClassName(), $disabledProperties);
    }


    /**
     * @return string
     */
    private function getEntityClassName(): string
    {
        return $this->mapper->getRepository()->getEntityClassName([]);
    }


    /**
     * @param string $column
     * @return float
     * @throws \Nextras\Dbal\QueryException
     */
    public function sumStored(string $column): float
    {
        if ($this->relationshipParent !== null && $this->relationshipMapper !== null) {
            // todo: maybe create for many to many relationship mapper
            return $this->relationshipMapper->getIteratorSum($this->relationshipParent, $this, $column);
        }
        return $this->getAggregationIterator('sum', $column, 0);
    }


    /**
     * @param string $type
     * @param string $column
     * @param int $default
     * @return mixed
     * @throws \Nextras\Dbal\QueryException
     */
    protected function getAggregationIterator(string $type, string $column, $default = 0)
    {
        $property = 'result' . ucfirst(strtolower($type));

        if ($this->{$property} === null) {
            $builder = clone $this->queryBuilder;
            if (!$builder->hasLimitOffsetClause()) {
                $builder->orderBy(null);
            }

            $builder->select(null);
            $column = $column === '*' ? $this->mapper->getConventions()->getStoragePrimaryKey()[0] : $column;
            // foreach ($this->mapper->getConventions()->getStoragePrimaryKey() as $column) {
                $builder->addSelect('%table.%column AS agrColumn', $builder->getFromAlias(), $column);
            // }
            $sql = 'SELECT ' . strtoupper($type) . '(agrColumn) AS value FROM (' . $builder->getQuerySql() . ') temp';
            $args = $builder->getQueryParameters();
            $resultSum = $this->connection->queryArgs($sql, $args)->fetchField();
            $this->{$property} = is_null($resultSum) ? $default : $resultSum;
        }

        return $this->{$property};
    }


    /**
     * @param string $column
     * @return float
     * @throws \Nextras\Dbal\QueryException
     */
    public function avgStored(string $column): float
    {
        if ($this->relationshipParent !== null && $this->relationshipMapper !== null) {
            // todo: maybe create for many to many relationship mapper
            return $this->relationshipMapper->getIteratorAvg($this->relationshipParent, $this, $column);
        }
        return $this->getAggregationIterator('avg', $column, 0);
    }
}
