<?php

namespace Titan\Presenter;

use Nette\Application\BadRequestException;
use Titan\Generator\Swagger\Swagger;

/**
 * @deprecated
 * @package Titan\Presenter
 * @swagger-disabled
 */
class SwaggerPresenter extends BasePresenter
{
    /**
     * @ignore-swagger
     * @param string $hash
     * @throws \Exception
     * @methods get
     * @route swagger/<hash>.json
     */
    public function actionDetail(string $hash)
    {
        $config = $this->getConfigParameter('swagger', []);
        $key = $this->getKey($hash);

        $swagger = new Swagger(
            $this->model,
            $this->getConfigParameter('appDir') . '/presenters',
            $config,
            $key['endpoints']
        );
        $this->sendJson($swagger->generate());
    }

    /**
     * @param string $hash
     * @return array
     * @throws BadRequestException
     */
    protected function getKey(string $hash): array
    {
        $config = $this->getConfigParameter('swagger', []);
        foreach ($config['keys'] as $item) {
            if ($item['key'] === $hash) {
                return $item;
            }
        }
        throw new BadRequestException('Forbidden! Invalid key', 403);
    }
}
