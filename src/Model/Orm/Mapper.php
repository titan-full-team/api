<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 08/02/2019
 * Time: 9:27 PM
 */

namespace Titan\Model\Orm;

use Nextras\Orm\Entity\Reflection\PropertyMetadata;
use Nextras\Orm\Mapper\Dbal\DbalMapperCoordinator;
use Nextras\Orm\Mapper\IMapper;
use Nextras\Orm\Mapper\IRelationshipMapper;
use Nextras\Orm\Entity\Reflection\PropertyRelationshipMetadata as Relationship;
use Titan\Model\BaseEntity;
use Nextras\Orm\Collection\ICollection;

class Mapper extends \Nextras\Orm\Mapper\Mapper
{
    /** @var DbalMapperCoordinator */
    private $mapperCoordinator;

    
    /**
     * @return ICollection|ITitanCollection
     */
    public function findAll(): ICollection
    {
        return new TitanCollection($this, $this->connection, $this->builder());
    }


    /**
     * @param $type
     * @param PropertyMetadata $metadata
     * @param IMapper|null $otherMapper
     * @return IRelationshipMapper
     */
    protected function createRelationshipMapper(
        $type,
        PropertyMetadata $metadata,
        IMapper $otherMapper = null
    ): IRelationshipMapper
    {
        if ($type === Relationship::ONE_HAS_MANY) {
            return new \Titan\Model\Orm\Mapper\RelationshipMapperOneHasMany($this->connection, $this, $metadata);
        }
        return parent::createRelationshipMapper($type, $metadata, $otherMapper);
    }
}