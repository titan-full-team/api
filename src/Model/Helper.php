<?php
/**
 * Created by PhpStorm.
 * User: norbertspanik
 * Date: 13/03/2018
 * Time: 16:31
 */

namespace Titan\Model;

use Nette\Application\BadRequestException;
use Nette\Security\IIdentity;
use Nextras\Dbal\Utils\DateTimeImmutable;
use Nextras\Orm\Collection\DbalCollection;
use Nextras\Orm\Collection\ICollection;
use Nextras\Orm\Entity\IEntity;
use Nextras\Orm\Entity\Reflection\PropertyRelationshipMetadata;
use Nextras\Orm\Relationships\ManyHasMany;
use Nextras\Orm\Relationships\OneHasMany;
use Titan\Model\Orm\ITitanCollection;
use Titan\Utils\ApiHelper;
use Titan\Utils\Paginator;

class Helper
{
    public static function oneToManyToArray(OneHasMany $oneToMany, array|string $method = 'toMain'): array
    {
        return self::prepareCollectionData($oneToMany, $method);
    }

    public static function getManyToManyToArray(ManyHasMany $manyToMany, array|string $method = 'toMain'): array
    {
        return self::prepareCollectionData($manyToMany, $method);
    }

    public static function collectionToArray(
        DbalCollection|ICollection|ITitanCollection $collection,
        array|string $method = 'toMain'
    ): array
    {
        return self::prepareCollectionData($collection, $method);
    }

    protected static function prepareCollectionData($data, array|string $method = 'toMain'): array
    {
        $out = [];
        foreach ($data as $item) {
            $out[] = ApiHelper::entityToArray($item, $method);
        }
        return $out;
    }

    public static function convertDateTime(DateTimeImmutable $item = NULL): ?string
    {
        if (!is_null($item) && date('Y', $item->getTimestamp()) > 1970) {
            return $item->format('c');
        }
        return NULL;
    }

    /**
     * @param BaseEntity|IIdentity $entity
     * @param array $contain
     * @param array $notAllowedProperties
     * @return array|null
     */
    public static function entityToArray($entity, array $contain = [], array $notAllowedProperties = []): ?array
    {
        if ($entity === null) {
            return null;
        }

        $out = [];
        $notAllowedProperties = self::getNotAllowedEntityProperties($entity, $notAllowedProperties);

        // properties of entity
        foreach ($entity->getMetadata()->getProperties() as $property) {
            if (is_null($property->relationship) && array_search($property->name, $notAllowedProperties) === false) {
                if ($entity->{$property->name} instanceof DateTimeImmutable) {
                    $out[$property->name] = self::convertDateTime($entity->{$property->name});
                } else {
                    $out[$property->name] = $entity->{$property->name};
                }
            }
        }

        // contain properties (relationship properties)
        foreach ($contain as $property => $value) {
            $meta = $entity->getMetadata()->getProperty($property);

            if ($meta->relationship->type === PropertyRelationshipMetadata::MANY_HAS_ONE
                || $meta->relationship->type === PropertyRelationshipMetadata::ONE_HAS_ONE
            ) {
                $out[$property] = is_null($entity->{$property}) ? null : call_user_func([$entity->{$property}, $value]);
            } else if ($meta->relationship->type === PropertyRelationshipMetadata::ONE_HAS_MANY) {
                $out[$property] = self::oneToManyToArray($entity->{$property}, $value);
            } else if ($meta->relationship->type === PropertyRelationshipMetadata::MANY_HAS_MANY) {
                $out[$property] = self::getManyToManyToArray($entity->{$property}, $value);
            }
        }

        return $out;
    }

    /**
     * @param IEntity|BaseEntity $entity
     * @param array $notAllowedProperties
     * @return array
     */
    public static function getNotAllowedEntityProperties(IEntity $entity, array $notAllowedProperties = []): array
    {
        return array_unique(array_merge($entity->notAllowedParameters, $notAllowedProperties));
    }

    /**
     * @param IEntity $entity
     * @param array $notAllowedProperties
     * @return array
     */
    public static function getAllowedEntityToEditProperties(IEntity $entity, array $notAllowedProperties = []): array
    {
        $notAllowedProperties = self::getNotAllowedEntityProperties($entity, $notAllowedProperties);
        $out = [];
        foreach ($entity->getMetadata()->getProperties() as $property) {
            if (in_array($property->name, $notAllowedProperties) === false
                && $property->isReadonly === false
                && $property->isVirtual === false
            ) {
                $out[] = $property;
            }
        }
        return $out;
    }

    public static function getManyToOneToArray($entity, string $property, array|string $method = 'toMain'):? array
    {
        return is_null($entity->getRawValue($property)) ? NULL : ApiHelper::entityToArray($entity, $method);
    }

    /**
     * @throws BadRequestException
     */
    public static function validateEntity($entity = NULL, string $entityName = '')
    {
        if (is_null($entity)) {
            throw new BadRequestException($entityName . ' not found', 404);
        }
        return $entity;
    }

    /**
     * @param DbalCollection|OneHasMany $items
     * @param string $childrenColumn
     * @param array|string $method
     * @param int|null $maxDepth
     * @param int $currentDepth
     * @return array
     */
    public static function collectionToTree(
        $items,
        string $childrenColumn = 'children',
        array|string $method = 'toMain',
        int $maxDepth = null,
        ?int $currentDepth = 0
    ): array
    {
        $out = [];
        foreach ($items as $item) {
            $out[] = self::entityToTree($item, $childrenColumn, $method, $maxDepth, $currentDepth);
        }
        return $out;
    }

    /**
     * @param IEntity $item
     * @param string $childrenColumn
     * @param array|string $method
     * @param int|null $maxDepth
     * @param int|null $currentDepth
     * @return array
     */
    public static function entityToTree(
        IEntity $item,
        string $childrenColumn = 'children',
        array|string $method = 'toMain',
        int $maxDepth = null,
        ?int $currentDepth = 0
    ): array
    {
        $data = ApiHelper::entityToArray($item, $method);
        if (is_null($maxDepth) || ($maxDepth > $currentDepth)) {
            $data['children'] = self::collectionToTree(
                $item->{$childrenColumn}, // ->get()->findBy(['public' => 1]) todo: why ?
                $childrenColumn,
                $method,
                $maxDepth,
                $currentDepth + 1
            );
        }
        return $data;
    }

    public static function paginateList(
        DbalCollection|ICollection|ITitanCollection $collection,
        int $page = 1,
        int $perPage = 10,
        array|string $collectionToArrayMethod = 'toMain'
    ): array
    {
        $paginator = new Paginator($page, $perPage, $collection->countStored());
        $data = $collection->limitBy($paginator->getLimit(), $paginator->getOffset());

        return [
            'paginator' => $paginator->getPaginatorData(),
            'items' => Helper::collectionToArray($data, $collectionToArrayMethod),
        ];
    }
}