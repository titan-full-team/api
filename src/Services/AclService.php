<?php

namespace Titan\Services;

use App\Model\User\Acl\UserAcl;
use Nette\Application\BadRequestException;
use Nextras\Orm\Entity\IEntity;
use Nextras\Orm\Repository\IRepository;
use Titan\Model\Acl\AclRepository;
use Tracy\Debugger;
use Tracy\ILogger;

class AclService
{
    CONST DEFAULT_GROUP = 'titan';

    protected IEntity $user;

    protected array $permissions = [];

    protected bool $loadedUserPermissions = false;

    protected IRepository|AclRepository $acl;

    public function __construct(IEntity $user, IRepository $acl)
    {
        $this->user = $user;
        $this->acl = $acl;

        /** @var \Titan\Model\acl\Acl $item */
        foreach ($acl->findAll() as $item) {
            if (!is_null($item->name)) {
                $this->permissions[$item->group][$item->name] = false;
            }
        }
    }

    protected function getPermissions(): array
    {
        if ($this->loadedUserPermissions === false) {
            /** @var UserAcl $permission */
            foreach ($this->user->userAcl as $permission) {
                $this->permissions[$permission->acl->group][$permission->acl->name] = strtolower($permission->value);
            }
        }
        return $this->permissions;
    }

    /**
     * @throws BadRequestException
     */
    public function checkGroupPermissions(string $group = self::DEFAULT_GROUP): bool
    {
        $permissions = $this->getPermissions();
        if (isset($permissions[$group])) {
            foreach ($permissions[$group] as $permission) {
                if ($permission !== false) {
                    return true;
                }
            }
        } else {
            $this->createPermission($group);
        }

        throw new BadRequestException('Action is forbidden', 403);
    }

    protected function createPermission(string $group, string $name = null): void
    {
        try {
            $count = $this->acl->findBy(['group' => $group, 'name' => $name])->countStored();
            if ($count === 0) {
                $entityClass = $this->acl->getEntityClassName([]);
                /** @var \Titan\Model\acl\Acl $entity */
                $entity = new $entityClass();
                $entity->name = $name;
                $entity->group = $group;
                $entity->allowedCrud = !is_null($name);
                $this->acl->persistAndFlush($entity);

            }
        } catch (\Throwable $e) {
            Debugger::log('Bug with creating permission', ILogger::WARNING);
        }
    }

    /**
     * @throws BadRequestException
     */
    public function checkSectionPermission(
        string $name,
        string $rights = 'crud',
        string $group = self::DEFAULT_GROUP
    ): bool
    {
        if (empty($rights)) {
            throw new BadRequestException('Rights cannot be empty', 500);
        }

        $permissions = $this->getPermissions();
        if (isset($permissions[$group]) && isset($permissions[$group][$name]) && $permissions[$group][$name] !== false) {
            foreach (str_split($rights) as $right) {
                if (stristr($permissions[$group][$name], strtolower($right)) === false) {
                    throw new BadRequestException('Action is forbidden', 403);
                }
            }
            return true;
        } else {
            $this->createPermission($group, $name);
        }
        throw new BadRequestException('Action is forbidden', 403);
    }

    public function haveSectionPermission(
        string $name,
        string $rights = 'crud',
        string $group = self::DEFAULT_GROUP
    ): bool
    {
        try {
            $this->checkSectionPermission($name, $rights, $group);
            return true;
        } catch (BadRequestException $exception) {
            return false;
        }
    }
}
