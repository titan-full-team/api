<?php

namespace Titan\Presenter;

use App\Model\Template\Template;
use Nette\Application\BadRequestException;

/** @deprecated */
class TemplatePresenter extends CorePresenter
{
    /**
     * @throws BadRequestException
     * @throws \Exception
     * @out-200 Template::toArray
     * @methods post
     */
    public function actionCreate()
    {
        $this->acl->checkSectionPermission('template', 'c');
        /** @var Template $entity */
        $entity = $this->model->template->patchNewEntity($this->getJsonBody());
        $entity->identificator = empty($entity->identificator) ? uniqid('template_') : $entity->identificator;
        $this->sendJson($this->model->prepareAndFlush($entity));
    }


    /**
     * @param int $page
     * @param int $perPage
     * @throws BadRequestException
     * @out-200 Template::toArray
     * @methods post
     */
    public function actionList(int $page = 1, int $perPage = 10)
    {
        $this->acl->checkSectionPermission('template', 'r');
        $data = $this->model->template
            ->findAll()
            ->findByApi($this->getJsonBodyParameter('conditions', []))
            ->orderByApi($this->getJsonBodyParameter('sort', []));
        $this->sendPaginatedListJson($data, $page, $perPage);
    }


    /**
     * @param int $id
     * @throws BadRequestException
     * @throws \Exception
     * @out-200 Template::toArray
     * @methods get
     */
    public function actionDetail(int $id)
    {
        $this->acl->checkSectionPermission('template', 'r');
        $this->sendJson($this->model->template->getByIdWithValidation($id));
    }


    /**
     * @param int $id
     * @throws \Exception
     * @out-200 Template::toArray
     * @methods PUT
     */
    public function actionUpdate(int $id)
    {
        $this->acl->checkSectionPermission('template', 'u');
        /** @var Template $entity */
        $entity = $this->model->template->patchEntity(
            $this->getJsonBody(),
            $this->model->template->getByIdWithValidation($id),
            ['history']
        );
        $this->sendJson($this->model->prepareAndFlush($entity));
    }


    /**
     * @param int $id
     * @throws \Exception
     * @out-200 Template was deleted
     * @methods delete
     */
    public function actionDelete(int $id)
    {
        $this->acl->checkSectionPermission('template', 'd');
        $entity = $this->model->template->getByIdWithValidation($id);
        $this->model->template->removeAndFlush($entity);
        $this->sendJson('Template was deleted');
    }
}
