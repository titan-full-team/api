<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 09/11/2018
 * Time: 5:20 PM
 */

namespace Titan\Generator\Swagger\Entity;

use Nette\PhpGenerator\Method;
use Nextras\Orm\Entity\Reflection\PropertyMetadata;
use Titan\Generator\Annotation;
use Titan\Utils\Strings;
use Tracy\Debugger;
use Tracy\ILogger;

class Entity
{
    /**
     * @param array $type
     * @return string
     * @throws \Exception
     */
    public static function getPropertyType(array $type): string
    {
        if (array_key_exists('int', $type) !== false) {
            return 'integer';
        } elseif (array_key_exists('float', $type) !== false) {
            return 'number';
        } elseif (array_key_exists('double', $type) !== false) {
            return 'number';
        } elseif (array_key_exists('string', $type) !== false) {
            return 'string';
        } elseif (array_key_exists('Nextras\\Dbal\\Utils\\DateTimeImmutable', $type)) {
            return 'string';
        } elseif (array_key_exists('boolean', $type) !== false || array_key_exists('bool', $type) !== false) {
            return 'boolean';
        } elseif (array_key_exists('DateInterval', $type) !== false) {
            return 'string';
        }
        Debugger::log('Swagger:: Add type `' . json_encode($type) . '` to swagger generator', ILogger::ERROR);
        return 'string';
    }

    /**
     * @throws \Exception
     */
    public static function getPropertyFormat(array $type): ?string
    {
        if (array_key_exists('Nextras\\Dbal\\Utils\\DateTimeImmutable', $type)) {
            return 'date-time';
        } elseif (array_key_exists('float', $type) !== false) {
            return 'float';
        } elseif (array_key_exists('double', $type) !== false) {
            return 'double';
        }
        return null;
    }

    /**
     * @throws \Exception
     */
    public static function getPropertyData(PropertyMetadata $property): array
    {
        $out['type'] = self::getPropertyType($property->types);
        $out['nullable'] = $property->isNullable;

        if ($property->isVirtual === true) {
            $out['readOnly'] = $property->isVirtual;
        }

        $format = self::getPropertyFormat($property->types);
        if (!is_null($format)) {
            $out['format'] = $format;
        }
        return $out;
    }

    public static function getEntityName(string $entity, string $methodName = null): string
    {
        $name = $entity;
        if (stristr($entity, '\\') !== false) {
            $explodedNameSpace = explode('\\', $entity);
            end($explodedNameSpace);
            $name = $explodedNameSpace[key($explodedNameSpace)];
        }

        $out = $name;

        if (!is_null($methodName)) {
            $method = $methodName === 'toArray' ? 'Detail' : $methodName;
            $method = str_replace(['array', 'Array'], '', $method);
            $method = str_starts_with($method, 'to') ? substr($method, 2) : $method;
            $out .= ucfirst($method);
        }
        return $out;
    }

    public static function getEntityWithNameSpace(array $useStatements, string $entity): ?string
    {
        if (stristr($entity, '\\') === false) {
            $useStatement = self::findUseStatement($useStatements, $entity);
            return is_null($useStatement) ? $entity : $useStatement;
        }
        return $entity;
    }

    protected static function findUseStatement(array $useStatements, string $item): ?string
    {
        foreach ($useStatements as $classUse) {
            $classUseExploded = explode('\\', $classUse);
            if (!empty($classUseExploded) && $classUseExploded[sizeof($classUseExploded) - 1] === $item) {
                return $classUse;
            }
        }
        return null;
    }

    public static function getEntityIdentificator(string $entity, string $methodName = null): string
    {
        $entity = md5($entity);
        $method = is_null($methodName) ? '' : md5($methodName);
        return md5($entity . $method);
    }

    protected static function getIgnoredParameters(Method $method): array
    {
        $input = Annotation::getValue($method->getComment(), 'in-ignore');
        return empty($input) ? [] : array_map('trim', explode(',', $input));
    }

    /**
     * @throws \Exception
     */
    public static function getEndpointsEntityInput(Method $method): ?EntityWriteSchema
    {
        $input = Annotation::getValue($method->getComment(), 'in-body');


        if (!is_null($input)) {
            return new EntityWriteSchema($input, 'in-body');
        }

        return null;
    }
}