<?php

namespace Titan\Utils\Input;

class InputArray implements IInput
{
    use InputTrait;

    protected function __construct()
    {
        $this->type = 'array';
    }
}