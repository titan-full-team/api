<?php

namespace Titan\Utils;

use Nette\Application\BadRequestException;
use Nextras\Orm\Model\Model;
use Nextras\Orm\Repository\Repository;
use Titan\Model\BaseEntity;

class CodeBook
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $codeBooks
     * @param array $allowed
     * @return array
     * @throws BadRequestException
     */
    public function getCodeBooks(array $codeBooks, array $allowed = []): array
    {
        $out = [];
        foreach ($codeBooks as $codeBook) {
            if (in_array($codeBook, $allowed) === true) {
                try {
                    /** @var Repository $repository */
                    $repository = $this->model->getRepositoryByName($codeBook);
                    $entityClass = $repository->getEntityClassName([]);
                    /** @var BaseEntity $entity */
                    $entity = new $entityClass();

                    $out[$codeBook] = [];

                    /** @var BaseEntity $item */
                    foreach ($repository->findAll()->orderBy($entity->codeBookSort) as $item) {
                        $out[$codeBook][] = $item->toCodeBookArray();
                    }
                } catch (\Nextras\Orm\Exception\InvalidArgumentException $exception) {
                    throw new BadRequestException('Codebook ' . $codeBook . ' not found', 404);
                }
            }
        }
        return $out;
    }
}
