<?php

namespace Titan\Utils\Input;

use Nette\Application\BadRequestException;

class InputInteger implements IInput
{
    use InputTrait;

    protected function __construct()
    {
        $this->type = 'integer';
    }

    /**
     * @throws BadRequestException
     */
    public function min(int $limit = 0): self
    {
        if ($this->hasValue === true && is_int($this->value) && $this->value < $limit) {
            $this->throwException('has to be at least ' . $limit);
        }
        return $this;
    }

    /**
     * @throws BadRequestException
     */
    public function max(int $limit = 255): self
    {
        if ($this->hasValue === true && is_int($this->value) && $this->value > $limit) {
            $this->throwException('has is to high. Maximum is ' . $limit);
        }
        return $this;
    }
}