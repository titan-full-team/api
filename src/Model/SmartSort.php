<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 10/10/2018
 * Time: 7:12 PM
 */

namespace Titan\Model;

use Nette\Application\BadRequestException;
use Nextras\Dbal\QueryBuilder\QueryBuilder;
use Nextras\Orm\Collection\ICollection;
use Titan\Model\Orm\ITitanCollection;

class SmartSort extends SmartRepository
{
    /**
     * @var array
     */
    public static $allowedSorting = [
        'RAND',
        ICollection::ASC,
        ICollection::DESC,
        ICollection::ASC_NULLS_FIRST,
        ICollection::ASC_NULLS_LAST,
        ICollection::DESC_NULLS_FIRST,
        ICollection::DESC_NULLS_LAST,
    ];


    /**
     * @param array $parameters
     * @param string $entityClassName
     * @param array $disabledProperties
     * @return array
     * @throws BadRequestException
     */
    public static function get(array $parameters, string $entityClassName, array $disabledProperties = []): array
    {
        foreach ($parameters as $key => $parameter) {
            $parameter = strtoupper($parameter);

            if (in_array($parameter, self::$allowedSorting) === false) {
                throw new BadRequestException('Invalid sort direction', 400);
            }
            $property = SmartCondition::getProperty($entityClassName, $key, $disabledProperties);
            if (is_null($property)) {
                unset($parameters[$key]);
            }
        }
        return $parameters;
    }


    /**
     * @param ICollection|ITitanCollection $collection
     * @param array $parameters
     * @param string $entityClassName
     * @param array $disabledProperties
     * @return ICollection|ITitanCollection
     * @throws BadRequestException
     */
    public static function set(
        ICollection $collection,
        array $parameters,
        string $entityClassName,
        array $disabledProperties = []
    ): ICollection
    {
        $conditions = self::get($parameters, $entityClassName, $disabledProperties);
        foreach ($conditions as $key => $value) {
            if ($value === 'RAND') {
                $collection = $collection->customSql(function (QueryBuilder $builder): QueryBuilder {
                    return $builder->orderBy('rand()');
                });
            } else {
                $collection = $collection->orderBy($key, $value);
            }
        }
        return $collection;
    }
}