<?php

namespace Titan\Presenter;

use App\Model\User\Token\UserToken;
use Nette\Application\BadRequestException;
use App\Model\User\User;
use Nette\Utils\Random;
use Nextras\Orm\Entity\IEntity;

/** @deprecated */
class AccountPresenter extends CorePresenter
{
    public const ACTIVATION_TOKEN = 'activationToken';


    /**
     * @var string
     */
    protected $outputUserMethod = 'toArray';

    /**
     * @throws \Exception
     * @in App\Model\User\User
     * @out-200 App\Model\User\User::toArray
     * @methods post
     */
    public function actionRegister()
    {
        if (!$this->user->register($this->getJsonBody())) {
            throw new BadRequestException('User was not created', 400);
        }
        $this->sendJson($this->getAccount(), $this->outputUserMethod);
    }

    /**
     * @methods post
     * @param string|null $type
     * @swagger-body {"login":{"type":"string","nullable":false},"password":{"type":"string","nullable":false}}
     * @swagger-body-required ["login", "password"]
     * @out-200 App\Model\User\User::toArray
     * @throws \Nette\Application\BadRequestException
     */
    public function actionLogin(string $type = null)
    {
        if (is_null($type)) {
            $this->sendJson($this->user->login($this->getJsonBody()));
        }

        throw new BadRequestException('Login type ' . $type . ' is not implemented', 501);
    }

    /**
     * @throws \Exception
     * @out-200 App\Model\User\User::toGetArray
     * @methods get
     * @route account
     * @swagger-user true
     */
    public function actionGet()
    {
        $this->sendJson($this->getAccount(), $this->outputUserMethod);
    }


    /**
     * @methods PUT
     * @swagger-body {"lang":{"type":"string","nullable":false}}
     * @swagger-body-required ["lang"]
     * @swagger-user true
     * @out-200 Language changed
     * @throws \Exception
     * @throws \Nette\Application\BadRequestException
     */
    public function actionSetLanguage()
    {
        $account = $this->getAccount();
        $account->lang = $this->getJsonBodyParameter('lang', 'en');
        $account = $this->model->user->persistAndFlush($account);
        $this->sendJson('Language changed');
    }


    /**
     * @throws \Exception
     * @throws \Nette\Application\BadRequestException
     * @out-200 App\Model\User\User::toArray
     * @methods PUT
     * @swagger-user true
     */
    public function actionUpdateProfile()
    {
        $this->sendJson($this->user->update($this->getJsonBody()), $this->outputUserMethod);
    }

    /**
     * @throws BadRequestException
     * @methods POST
     * @out-200 Done, please check your e-mail account
     */
    public function actionLostPassword()
    {
        /** @var User $account */
        $account = $this->model->user->getByWithValidation(['login' => $this->getJsonBodyParameter('login')]);

        $userToken = new UserToken();
        $userToken->user = $account;
        $userToken->type = UserToken::LOST_PASSWORD;
        $userToken->token = Random::generate(64);
        $userToken->expiration = new \DateTimeImmutable('+4 hours');

        $this->mailer->send('lostPassword', $account->login, [$account], ['token' => $userToken->token]);

        $this->model->userToken->persistAndFlush($userToken);
        $this->sendJson('Done, please check your e-mail account');
    }

    /**
     * @throws BadRequestException
     * @methods GET
     * @out-200 App\Model\User\User::toArray
     */
    public function actionGetLostPassword(int $id, string $hash)
    {
        $account = $this->getAccountByLostPasswordToken($id, $hash);
        $this->sendJson($account, $this->outputUserMethod);
    }

    /**
     * @param int $id
     * @param string $hash
     * @return User|IEntity
     * @throws BadRequestException
     */
    protected function getAccountByLostPasswordToken(int $id, string $hash): User
    {
        return $this->model->user->getByWithValidation([
                'id' => $id,
                'userToken->token' => $hash,
                'userToken->type' => UserToken::LOST_PASSWORD,
                'userToken->expiration>=' => new \DateTimeImmutable('now'),
            ]
        );
    }

    /**
     * @param int $id
     * @param string $hash
     * @methods PUT
     * @out-200 Password changed, please log in.
     * @throws BadRequestException
     */
    public function actionResetPassword(int $id, string $hash)
    {
        $account = $this->getAccountByLostPasswordToken($id, $hash);
        $newPassword = $this->getJsonBodyParameter('newPassword');
        $this->user->setCredentials($account, $newPassword, \Titan\Utils\User::USER_DEFAULT_AUTH_CLASS);

        // remove all lost password tokens
        $tokens = $this->model->userToken->findBy(['type' => UserToken::LOST_PASSWORD]);
        /** @var UserToken $token */
        foreach ($tokens as $token) {
            $token->expiration = new \DateTimeImmutable('-1 minute');
            $this->model->userToken->persist($token);
        }
        $this->model->userToken->flush();
        $this->sendJson('Password changed, please log in.');
    }

    /**
     * @throws \Exception
     * @out-200 Password was changed
     * @methods put
     * @swagger-user true
     */
    public function actionChangePassword()
    {
        $this->user->changeCredentials($this->getJsonBody());
        $this->sendJson('Password was changed');
    }

    /**
     * @throws \Exception
     * @out-200 You have been logged out
     * @methods POST
     * @swagger-user true
     */
    public function actionLogout()
    {
        $this->user->logout();
        $this->sendJson('You have been logged out');
    }


    /**
     * @throws BadRequestException
     */
    protected function sendActivationMail(User $account)
    {
        // Activation token
        $token = Random::generate('50');
        $activationToken = new UserToken();
        $activationToken->type = self::ACTIVATION_TOKEN;
        $activationToken->token = $token;
        $activationToken->user = $account;
        $activationToken->expiration = null;
        $this->model->userToken->persistAndFlush($activationToken);
        $this->mailer->send('activationMail', $account->login, [$account], ['token' => $token]);
    }


    /**
     * @param int $userId
     * @param string $token
     * @param string $activeColumn
     * @param bool $activeValue
     * @throws BadRequestException
     */
    protected function activateAccount(
        int $userId,
        string $token,
        string $activeColumn = 'blocked',
        bool $activeValue = true
    )
    {
        /** @var User $user */
        $user = $this->model->user->getByWithValidation([
            'userToken->token' => $token,
            'userToken->type' => self::ACTIVATION_TOKEN,
            'id' => $userId,
        ]);
        $user->{$activeColumn} = $activeValue;
        $this->model->user->persistAndFlush($user);

        // remove token
        $token = $this->model->userToken
            ->getBy([
                'type' => self::ACTIVATION_TOKEN,
                'token' => $token,
                'user->id' => $userId,
            ]);
        $this->model->userToken->removeAndFlush($token);
    }
}
