<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 03/11/2018
 * Time: 11:38 AM
 */

namespace Titan\Utils;


class Strings extends \Nette\Utils\Strings
{
    /**
     * @param $str
     * @return string
     */
    public static function convertToCamel(string $str) : string
    {
        $exploded_str = explode('_', $str);
        $exploded_str_camel = array_map('ucwords', $exploded_str);
        return lcfirst(implode('', $exploded_str_camel));
    }

    /**
     * @param string $string
     * @param $start
     * @param $end
     * @return string
     */
    public static function stringBetween(string $string, string $start, string $end): string
    {
        $string = substr($string, strpos($string, $start) + strlen($start));
        $string = substr($string, 0, strpos($string, $end));
        return $string;
    }
}