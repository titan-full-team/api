<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 09/11/2018
 * Time: 5:16 PM
 */

namespace Titan\Generator\Swagger\Entity;

use Nextras\Orm\Model\IModel;

interface IEntitySchema
{
    public function generate(IModel $model): void;

    public function getDependencies(): array;

    public function isGenerated(): bool;

    public function getIdentificator(): string;

    public function getEntityFullName(): string;

    public function getData(): array;
}