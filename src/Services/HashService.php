<?php

namespace Titan\Services;

use Hashids\Hashids;
use Nette\Application\BadRequestException;

class HashService
{
    private Hashids $hashIds;

    /**
     * @throws \Exception
     */
    public function __construct(string $hashIds)
    {
        $this->hashIds = new Hashids($hashIds, 55);
    }

    public function get(): Hashids
    {
        return $this->hashIds;
    }

    public function encode(array $params): string
    {
        return $this->hashIds->encode($params);
    }

    /**
     * @throws BadRequestException
     */
    public function validateHash(string $hash, int $timeParameter = null, string $message = 'Link expired'): array
    {
        $params = $this->hashIds->decode($hash);
        if (empty($params) || (!is_null($timeParameter) && $params[$timeParameter] <= time())) {
            throw new BadRequestException($message, 403);
        }
        return $params;
    }
}
