<?php
/**
 * Created by PhpStorm.
 * User: norbertspanik
 * Date: 13/03/2018
 * Time: 16:31
 */

namespace Titan\Model\Functions;

use App\Model\BaseMapper;
use Nette\Application\BadRequestException;
use Nette\Utils\Strings;
use Nextras\Dbal\QueryBuilder\QueryBuilder;
use Nextras\Orm\Collection\Functions\IArrayFunction;
use Nextras\Orm\Collection\Functions\IQueryBuilderFunction;
use Nextras\Orm\Collection\Helpers\ArrayCollectionHelper;
use Nextras\Orm\Collection\Helpers\DbalExpressionResult;
use Nextras\Orm\Collection\Helpers\DbalQueryBuilderHelper;
use Nextras\Orm\Entity\IEntity;
use Titan\Model\SmartCondition;

/**
 * $users->findBy([DateFunction::class, 'name', 'Jon']);
 */
class DateFunction extends BaseFunction implements IArrayFunction, IQueryBuilderFunction
{
    /**
     * Returns true if entity should stay in the result collection; the condition is evaluated in database and this
     * method just returns appropriate Nextras Dbal's filtering expression for passed args.
     * @phpstan-param array<int|string, mixed> $args
     * @param DbalQueryBuilderHelper $helper
     * @param QueryBuilder $builder
     * @param array $args
     * @return DbalExpressionResult
     * @throws BadRequestException
     */
    public function processQueryBuilderExpression(
        DbalQueryBuilderHelper $helper,
        QueryBuilder $builder,
        array $args
    ): DbalExpressionResult
    {
        $property = $helper->processPropertyExpr($builder, $args[0])->args;
        $value = (new \DateTime($args[1]))->format('Y-m-d');
        $operator = SmartCondition::validateOperator($args[2] ?? '=');
        return new DbalExpressionResult(['DATE(%column) ' . $operator . ' %s', $property[1], $value]);
    }

    /**
     * Returns a value depending on values of entity; the expression passed by args is evaluated during this method
     * execution.
     * Usually returns a boolean for filtering evaluation.
     * @phpstan-param array<int|string, mixed> $args
     * @param ArrayCollectionHelper $helper
     * @param IEntity $entity
     * @param array $args
     * @return mixed
     */
    public function processArrayExpression(ArrayCollectionHelper $helper, IEntity $entity, array $args)
    {
        return true;
        // todo: implement array function
        $value = $helper->getValue($entity, $args[0])->value;
        return Strings::contains($value, $args[1]);
    }
}