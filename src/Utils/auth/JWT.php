<?php

namespace Titan\Utils\auth;

use Lindelius\JWT\Algorithm\HMAC\HS256;

class JWT extends \Lindelius\JWT\JWT
{
    use HS256;
}