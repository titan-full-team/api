<?php

namespace Titan\Services;

use Nextras\Orm\Entity\IEntity;

class AuthService implements IAuthService
{
    private ?IEntity $user = null;

    private array $skipValidation = [];

    public function setUser(?IEntity $user): void
    {
        $this->user = $user;
    }

    public function disableValidation(string $class): void
    {
        $this->skipValidation[] = $class;
    }

    public function isValidationDisabled(string $class): bool
    {
        return in_array($class, $this->skipValidation);
    }

    public function enableValidation(string $class): void
    {
        $this->skipValidation = array_filter($this->skipValidation, function ($item) use ($class) {
            return $item !== $class;
        });
    }

    public function isLogged(): bool
    {
        return $this->user !== null;
    }

    public function getUser(): ?IEntity
    {
        return $this->user;
    }
}