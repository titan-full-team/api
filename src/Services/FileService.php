<?php

namespace Titan\Services;

use JetBrains\PhpStorm\NoReturn;
use Nette\Application\BadRequestException;
use Nette\Utils\Image;
use Nette\Utils\ImageException;
use Nette\Utils\UnknownImageFileException;
use Titan\Utils\ContentType;

readonly class FileService
{
    public function __construct(private string $tempDir, private string $appDir)
    {

    }

    public function getTempDir(string $dirName): string
    {
        $dir = $this->tempDir . '/' . $dirName . '/';
        if (!is_dir($dir)) {
            mkdir($dir);
        }
        return $dir;
    }

    public function getFileDir(string $dirName): string
    {
        $dir = $this->appDir . '/../../file/' . $dirName . '/';
        if (!is_dir($dir)) {
            mkdir($dir);
        }
        return $dir;
    }

    /**
     * @throws BadRequestException
     */
    #[NoReturn] public static function printFile(string $folder, string $name): void
    {
        $file = self::getFile($folder, $name);
        $mimeType = substr($name, -4) === 'webp' ? 'image/webp' : mime_content_type($file);
        $handle = fopen($file, 'rb');
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache"); //keeps ie happy
        header('Content-disposition: filename="' . $name . '"');
        header('Content-Type: ' . $mimeType);
        header("Content-Length: " . filesize($file));
        header('Content-Transfer-Encoding: binary');
        fpassthru($handle);
        exit;
    }

    public static function cleanFolderName(string $folder = null): ?string
    {
        return is_null($folder) ? null : str_replace(['.', '/', '\\', '|'], '', $folder);
    }

    /**
     * @throws BadRequestException
     */
    protected static function getFile(string $folder, string $name): string
    {
        $file = $folder . $name;
        if (file_exists($file) === false) {
            throw new BadRequestException('File not found', 404);
        }
        return $file;
    }

    protected static function getMode(string $mode = null): ?string
    {
        return $mode === 'shrinkOnly' ? Image::SHRINK_ONLY : $mode;
    }

    protected static function getTemp(string $folder): string
    {
        if (!is_dir($folder)) {
            mkdir($folder);
        }
        return $folder;
    }

    /**
     * @throws ImageException
     * @throws UnknownImageFileException
     * @throws BadRequestException
     */
    #[NoReturn] protected static function printOriginalFile(
        string $tmpFolder,
        string $folder,
        string $name,
        int    $timeKey,
        bool   $webp = false
    ): void
    {
        if ($webp === true) {
            try {
                $newName = md5($name . '--original-webp') . '-' . $timeKey . '-original.webp';
                self::printFile($folder, $newName);
            } catch (BadRequestException $exception) {
                $image = Image::fromFile(self::getFile($folder, $name));
                $image->save($tmpFolder . $newName, 100, Image::WEBP);
                $image->send(Image::WEBP);
                exit;
            }
        } else {
            self::printFile($folder, $name);
        }
    }

    /**
     * @throws BadRequestException
     * @throws ImageException
     * @throws UnknownImageFileException
     */
    #[NoReturn] protected static function printResizedFiles(
        string $tmpFolder,
        string $folder,
        string $name,
        int    $timeKey,
        bool   $webp = false,
        int    $width = null,
        int    $height = null,
        string $mode = null
    ): void
    {
        $mode = self::getMode($mode);

        $sizeExtension = '--' . (is_null($width) ? 0 : $width);
        $sizeExtension .= 'x' . (is_null($height) ? 0 : $height);
        $sizeExtension .= is_null($mode) ? '' : '--' . $mode;
        $newFileName = md5($name . $sizeExtension . ($webp === true ? '--.webp' : '')) . '-resized';
        $newFileName .= '--' . $timeKey;
        $newFileName .= $webp === true ? '.webp' : '';

        try {
            self::printFile($tmpFolder, $newFileName);
        } catch (BadRequestException $exception) {
            $image = Image::fromFile(self::getFile($folder, $name));

            !is_null($width) && !is_null($height) && is_null($mode)
                ? $image->resize($width, $height, Image::EXACT)
                : $image->resize($width, $height, is_null($mode) ? Image::FILL : $mode);

            $mime = mime_content_type(self::getFile($folder, $name));
            if (in_array($mime, ['image/jpeg', 'image/jpg'])) {
                $image->save($tmpFolder . $newFileName, 100, Image::JPEG);
                $image->send(Image::JPEG);
            } else if (in_array($mime, ['image/webp'])) {
                $image->save($tmpFolder . $newFileName, 100, Image::WEBP);
                $image->send(Image::WEBP);
            } else {
                $image->save($tmpFolder . $newFileName, 100, Image::PNG);
                $image->send(Image::PNG);
            }
        }
        exit;
    }


    /**
     * @throws BadRequestException
     * @throws \Nette\Utils\UnknownImageFileException|ImageException
     */
    #[NoReturn] public static function printResizedFile(
        string $folder,
        string $name,
        int    $timeKey,
        int    $width = null,
        int    $height = null,
        string $mode = null,
        bool   $webp = false
    ): void
    {
        $tmpFolder = self::getTemp($folder . 'tmp/');
        is_null($width) && is_null($height)
            ? self::printOriginalFile($tmpFolder, $folder, $name, $timeKey, $webp)
            : self::printResizedFiles($tmpFolder, $folder, $name, $timeKey, $webp, $width, $height, $mode);
    }

    public static function createFile(string $file, string $content)
    {
        $f = fopen($file, 'w') or die('Unable to open file!');
        fwrite($f, $content);
        fclose($f);
    }

    public static function saveBase64file(string $dir, string $base64File, string $fileName = null): array
    {
        // prepare file
        $explodedBase64File = explode(',', $base64File);

        if (array_key_exists(1, $explodedBase64File)) {
            $mimeType = explode(':', substr($explodedBase64File[0], 0, strpos($explodedBase64File[0], ';')))[1];
            $fileToSave = base64_decode($explodedBase64File[1]);
        } else {
            $fileToSave = base64_decode($base64File);
            $mimeType = finfo_buffer(finfo_open(), $fileToSave, FILEINFO_MIME_TYPE);
        }

        $extension = ContentType::mimeType2ext($mimeType);
        $fileName = is_null($fileName) ? uniqid() . '-' . $fileName . '.' . $extension : $fileName;
        $fileDir = $dir . '/' . $fileName;

        file_put_contents($fileDir, $fileToSave);

        return [
            'dir' => $fileDir,
            'name' => $fileName,
        ];
    }

    public static function saveFile(string $dir, string $base64File, string $fileName, string $subFolder = null): array
    {
        $dir = is_null($subFolder) ? $dir : ($dir . '/' . $subFolder);

        if (!is_dir($dir)) {
            mkdir($dir);
        }

        // prepare file
        $file = self::saveBase64file($dir, $base64File, $fileName);

        return [
            'name' => $file['name'],
            'path' => $subFolder,
            'location' => is_null($subFolder) ? $file['name'] : ($subFolder . '/' . $file['name'])
        ];
    }
}
