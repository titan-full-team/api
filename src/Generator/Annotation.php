<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 08/11/2018
 * Time: 11:22 PM
 */

namespace Titan\Generator;

class Annotation
{

    public static function getValue(string $comment, string $type, $default = null): ?string
    {
        $type = '@' . $type;
        $typeSize = strlen($type);
        foreach (explode(PHP_EOL, $comment) as $comment) {
            if (substr($comment, 0, $typeSize) === $type) {
                return trim(substr($comment, $typeSize));
            }
        }
        return $default;
    }

    public static function getBoolValue(string $comment, string $type, bool $default = null): ?bool
    {
        $value = self::getValue($comment, $type, $default);
        if ($value === 'true') {
            return true;
        } else if ($value === 'false') {
            return false;
        }
        return is_null($value) ? null : (bool)$value;
    }
}