<?php
declare(strict_types=1);

namespace Titan\Generator;

use Nette;
use Nette\PhpGenerator\Constant;
use Nette\PhpGenerator\Helpers;
use Nette\PhpGenerator\Method;
use Nette\PhpGenerator\PhpNamespace;
use Nette\PhpGenerator\Property;
use Nette\Utils\Strings;


/**
 * Class/Interface/Trait description.
 *
 * @property Method[] $methods
 * @property Property[] $properties
 */
final class ClassType
{
    use \Nette\SmartObject;
    use \Nette\PhpGenerator\Traits\CommentAware;

    const TYPE_CLASS = 'class';

    const TYPE_INTERFACE = 'interface';

    const TYPE_TRAIT = 'trait';

    private ?PhpNamespace $namespace;

    private ?string $name;

    /** class|interface|trait */
    private string $type = 'class';

    private bool $final = false;

    private bool $abstract = false;

    /** @var string|string[] */
    private string|array $extends = [];

    /** @var string[] */
    private array $implements = [];

    private array $traits = [];

    /** @var Constant[] name => Constant */
    private array $consts = [];

    /** @var Property[] name => Property */
    private array $properties = [];

    /** @var Method[] name => Method */
    private array $methods = [];


    /**
     * @throws \ReflectionException
     */
    public static function from($class): self
    {
        if ($class instanceof \ReflectionClass) {
            trigger_error(__METHOD__ . '() accepts only class name or object.', E_USER_DEPRECATED);
        }
        return (new Factory)->fromClassReflection(
            $class instanceof \ReflectionClass ? $class : new \ReflectionClass($class)
        );
    }


    public function __construct(string $name = null, PhpNamespace $namespace = null)
    {
        $this->setName($name);
        $this->namespace = $namespace;
    }


    public function __toString(): string
    {
        $resolver = $this->namespace ? [$this->namespace, 'unresolveName'] : function ($s) { return $s; };

        $traits = [];
        foreach ($this->traits as $trait => $resolutions) {
            $traits[] = 'use ' . $resolver($trait)
                . ($resolutions ? " {\n\t" . implode(";\n\t", $resolutions) . ";\n}" : ';');
        }

        $consts = [];
        foreach ($this->consts as $const) {
            $consts[] = Helpers::formatDocComment((string) $const->getComment())
                . ($const->getVisibility() ? $const->getVisibility() . ' ' : '')
                . 'const ' . $const->getName() . ' = ' . Helpers::dump($const->getValue()) . ';';
        }

        $properties = [];
        foreach ($this->properties as $property) {
            $properties[] = Helpers::formatDocComment((string) $property->getComment())
                . ($property->getVisibility() ?: 'public') . ($property->isStatic() ? ' static' : '') . ' $' . $property->getName()
                . ($property->getValue() === null ? '' : ' = ' . Helpers::dump($property->getValue()))
                . ';';
        }

        return Strings::normalize(
                Helpers::formatDocComment($this->comment . "\n")
                . ($this->abstract ? 'abstract ' : '')
                . ($this->final ? 'final ' : '')
                . ($this->name ? "$this->type $this->name " : '')
                . ($this->extends ? 'extends ' . implode(', ', array_map($resolver, (array) $this->extends)) . ' ' : '')
                . ($this->implements ? 'implements ' . implode(', ', array_map($resolver, $this->implements)) . ' ' : '')
                . ($this->name ? "\n" : '') . "{\n"
                . Strings::indent(
                    ($traits ? implode("\n", $traits) . "\n\n" : '')
                    . ($consts ? implode("\n", $consts) . "\n\n" : '')
                    . ($properties ? implode("\n\n", $properties) . "\n\n\n" : '')
                    . ($this->methods ? implode("\n\n\n", $this->methods) . "\n" : ''))
                . '}'
            ) . ($this->name ? "\n" : '');
    }

    public function getNamespace(): ?PhpNamespace
    {
        return $this->namespace;
    }

    public function setName(?string $name): self
    {
        if ($name !== null && !Helpers::isIdentifier($name)) {
            throw new Nette\InvalidArgumentException("Value '$name' is not valid class name.");
        }
        $this->name = $name;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setType(string $type): self
    {
        if (!in_array($type, ['class', 'interface', 'trait'], true)) {
            throw new Nette\InvalidArgumentException('Argument must be class|interface|trait.');
        }
        $this->type = $type;
        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setFinal(bool $state = true): self
    {
        $this->final = $state;
        return $this;
    }

    public function isFinal(): bool
    {
        return $this->final;
    }

    public function setAbstract(bool $state = true): self
    {
        $this->abstract = $state;
        return $this;
    }

    public function isAbstract(): bool
    {
        return $this->abstract;
    }

    public function setExtends($names): self
    {
        if (!is_string($names) && !is_array($names)) {
            throw new Nette\InvalidArgumentException('Argument must be string or string[].');
        }
        $this->validate((array) $names);
        $this->extends = $names;
        return $this;
    }

    public function getExtends(): array|string
    {
        return $this->extends;
    }

    public function addExtend(string $name): self
    {
        $this->validate([$name]);
        $this->extends = (array) $this->extends;
        $this->extends[] = $name;
        return $this;
    }

    public function setImplements(array $names): self
    {
        $this->validate($names);
        $this->implements = $names;
        return $this;
    }

    public function getImplements(): array
    {
        return $this->implements;
    }

    public function addImplement(string $name): self
    {
        $this->validate([$name]);
        $this->implements[] = $name;
        return $this;
    }

    public function setTraits(array $names): self
    {
        $this->validate($names);
        $this->traits = array_fill_keys($names, []);
        return $this;
    }

    public function getTraits(): array
    {
        return array_keys($this->traits);
    }

    public function addTrait(string $name, array $resolutions = []): self
    {
        $this->validate([$name]);
        $this->traits[$name] = $resolutions;
        return $this;
    }

    /**
     * @deprecated  use setConstants()
     * @return static
     */
    public function setConsts(array $consts): self
    {
        trigger_error(__METHOD__ . '() is deprecated, use setConstants()', E_USER_DEPRECATED);
        return $this->setConstants($consts);
    }

    /**
     * @deprecated  use getConstants()
     */
    public function getConsts(): array
    {
        trigger_error(__METHOD__ . '() is deprecated, use similar getConstants()', E_USER_DEPRECATED);
        return array_map(function ($const) { return $const->getValue(); }, $this->consts);
    }


    /**
     * @deprecated  use addConstant()
     * @return static
     */
    public function addConst(string $name, $value): self
    {
        trigger_error(__METHOD__ . '() is deprecated, use similar addConstant()', E_USER_DEPRECATED);
        $this->addConstant($name, $value);
        return $this;
    }


    /**
     * @param  Constant[]|mixed[]  $consts
     * @return static
     */
    public function setConstants(array $consts): self
    {
        $this->consts = [];
        foreach ($consts as $k => $v) {
            $const = $v instanceof Constant ? $v : (new Constant($k))->setValue($v);
            $this->consts[$const->getName()] = $const;
        }
        return $this;
    }

    public function getConstants(): array
    {
        return $this->consts;
    }

    public function addConstant(string $name, $value): Constant
    {
        return $this->consts[$name] = (new Constant($name))->setValue($value);
    }

    public function setProperties(array $props): self
    {
        $this->properties = [];
        foreach ($props as $v) {
            if (!$v instanceof Property) {
                throw new Nette\InvalidArgumentException('Argument must be Nette\PhpGenerator\Property[].');
            }
            $this->properties[$v->getName()] = $v;
        }
        return $this;
    }

    public function getProperties(): array
    {
        return $this->properties;
    }

    public function getProperty(string $name): Property
    {
        if (!isset($this->properties[$name])) {
            throw new Nette\InvalidArgumentException("Property '$name' not found.");
        }
        return $this->properties[$name];
    }

    public function addProperty(string $name, $value = null): Property
    {
        return $this->properties[$name] = (new Property($name))->setValue($value);
    }

    public function setMethods(array $methods): self
    {
        $this->methods = [];
        foreach ($methods as $v) {
            if (!$v instanceof Method) {
                throw new Nette\InvalidArgumentException('Argument must be Nette\PhpGenerator\Method[].');
            }
            $this->methods[$v->getName()] = $v;
        }
        return $this;
    }

    public function getMethods(): array
    {
        return $this->methods;
    }

    public function getMethod(string $name): Method
    {
        if (!isset($this->methods[$name])) {
            throw new Nette\InvalidArgumentException("Method '$name' not found.");
        }
        return $this->methods[$name];
    }

    public function addMethod(string $name): Method
    {
        $method = (new Method($name));
        if ($this->type === 'interface') {
            $method->setBody(null);
        } else {
            $method->setVisibility('public');
        }
        return $this->methods[$name] = $method;
    }


    private function validate(array $names): void
    {
        foreach ($names as $name) {
            if (!Helpers::isNamespaceIdentifier($name, true)) {
                throw new Nette\InvalidArgumentException("Value '$name' is not valid class name.");
            }
        }
    }
}
