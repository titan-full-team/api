<?php

namespace Titan\Utils\auth;

use App\Model\User\Token\UserToken;
use DateTimeImmutable;
use Lindelius\JWT\Exception\JwtException;
use Lindelius\JWT\Exception\UnsupportedAlgorithmException;
use Nette\Application\BadRequestException;
use Nette\Utils\Random;
use Nextras\Orm\Entity\IEntity;
use Titan\Utils\ApiHelper;
use Titan\Utils\User;

abstract class Auth
{
    const LOGIN_TOKEN = 'loginToken';

    /**
     * @var \Nextras\Orm\Repository\Repository
     */
    protected $userModel;

    /**
     * @var \Nextras\Orm\Repository\Repository
     */
    protected $userTokenModel;

    /**
     * @var string
     */
    protected $userTokenEntityClass;

    /**
     * @var string
     */
    private $jwtSecret;

    /**
     * @var string
     */
    private $jwtName = 'jwt';

    /**
     * @param \Nextras\Orm\Repository\Repository $userModel
     * @param \Nextras\Orm\Repository\Repository $userTokenModel
     * @param string $userTokenEntityClass
     * @param string $jwtSecret
     * @param string $jwtName
     */
    public function __construct(
        $userModel,
        $userTokenModel,
        string $userTokenEntityClass,
        string $jwtSecret,
        string $jwtName
    )
    {
        $this->userModel = $userModel;
        $this->userTokenModel = $userTokenModel;
        $this->userTokenEntityClass = $userTokenEntityClass;
        $this->jwtSecret = $jwtSecret;
        $this->jwtName = $jwtName;
    }

    /**
     * @param IEntity $account
     * @param string $value
     * @param string $type
     * @param string|null $expiration
     * @param bool $flushEntity
     */
    protected function setNewToken(
        IEntity $account,
        string $value,
        string $type,
        string $expiration = null,
        bool $flushEntity = true
    )
    {
        /** @var UserToken $loginToken */
        $token = new $this->userTokenEntityClass();
        $token->user = $account;
        $token->token = $value;
        $token->type = $type;
        $token->expiration = $expiration;
        $flushEntity === true ? $this->userTokenModel->persistAndFlush($token) : $this->userTokenModel->persist($token);
    }

    /**
     * @param IEntity|\App\Model\User\User $account
     * @param string $class
     * @param string|null $tokenExpiration
     * @return string CSRF token
     * @throws BadRequestException
     * @throws JwtException
     * @throws UnsupportedAlgorithmException
     */
    public function setJWT(IEntity $account, string $class, $tokenExpiration = '+1 day'): string
    {
        $expiration = empty($tokenExpiration)
            ? null
            : ((new DateTimeImmutable($tokenExpiration))->getTimestamp());
        $csrf = Random::generate(64, '0-9a-zA-Z!@#$%^&*') . time();

        $jwt = JWT::create('HS256');
        $jwt->exp = $expiration;
        $jwt->iat = time();
        $jwt->sub = [
            'user_id' => $account->id,
            'csrf' => $csrf,
            'authType' => $class,
        ];
        $jwtEncoded = $jwt->encode($this->jwtSecret);
        $this->setNewToken($account, $jwtEncoded, Auth::LOGIN_TOKEN, $tokenExpiration);
        $this->setCookie($jwtEncoded, is_null($expiration) ? -1 : $expiration);
        return $csrf;
    }

    /**
     * Methods is probably not used
     * @param string $token
     * @throws BadRequestException
     * @deprecated
     */
    protected function setBearerToken(string $token)
    {
        $this->getServerName();
        $this->setCookie($token, (new DateTimeImmutable('+ 1 day'))->getTimestamp());
    }


    /**
     * @param string $value
     * @param int $expiration
     * @throws BadRequestException
     */
    protected function setCookie(string $value = '', int $expiration = -1)
    {
        $_COOKIE[$this->jwtName] = $value;
        setcookie(
            $this->jwtName,
            $value,
            $expiration,
            '/',
            '.' . $this->getServerName(),
            (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443,
            true
        );
    }

    /**
     * @return string
     * @throws BadRequestException
     */
    protected function getServerName(): string
    {
        if (array_key_exists('SERVER_NAME', $_SERVER) === false || empty($_SERVER['SERVER_NAME'])) {
            throw new BadRequestException('Server name header does not exist', 501);
        } else if (substr($_SERVER['SERVER_NAME'], 0, 3) === 'www') {
            return substr($_SERVER['SERVER_NAME'], 4);
        }
        return $_SERVER['SERVER_NAME'];
    }

    /**
     * Logout user
     */
    public function logout()
    {
        $tokens = $this->userTokenModel->findBy([
            'type' => Auth::LOGIN_TOKEN,
            'token' => User::getJWTToken($this->jwtName),
        ]);

        /** @var UserToken $token */
        foreach ($tokens as $token) {
            $this->userTokenModel->remove($token);
        }
        $this->userTokenModel->flush();
        $this->setCookie();
    }
}
