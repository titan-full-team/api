<?php

namespace Titan\Router;

use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\SimpleRouter;
use Nette\Caching\Cache;

class RouterFactory
{
    protected RouteList $router;

    protected string $linkPrefix = '';

    protected \Nette\Http\IRequest $httpRequest;

    private Cache $cache;

    protected array $parameters = [];

    /**
     * @param \Nette\Http\IRequest $httpRequest
     * @param array $parameters
     */
    public function __construct(\Nette\Http\IRequest $httpRequest, array $parameters)
    {
        $this->router = new RouteList();
        $this->httpRequest = $httpRequest;

        $storage = new \Nette\Caching\Storages\FileStorage($parameters['tempDir'] . '/cache');
        $this->cache = new Cache($storage);
        $this->parameters = $parameters;
    }

    /**
     * @return RouteList
     * @throws \Exception
     * @throws \Throwable
     */
    protected function createRouter()
    {
        $this->initRoutes($this->parameters['appDir'] . '/Presenters');
        return $this->router;
    }

    /**
     * @param $presentedDir
     * @throws \Exception
     * @throws \Throwable
     */
    protected function initRoutes($presentedDir)
    {
        $cacheKey = md5($presentedDir);
        $routes = $this->cache->load($cacheKey);

        if (is_null($routes) || $this->parameters['productionMode'] === false) {
            $router = new Routes($presentedDir);
            $routes = $router->getRoutes();
            $this->cache->save($cacheKey, $routes);
        }

        /** @var Route $route */
        foreach ($routes as $route) {
            foreach ($route->getMethods() as $method) {
                $this->addRoute(
                    $route->getMask(),
                    $route->getPresenterName() . ':' . $route->getActionName(),
                    $method
                );
            }
        }
    }

    /**
     * @description SimpleRouter::SECURED is deprecated
     * @return int
     */
    protected function getFlag(): int
    {
        return 0;
    }

    /**
     * @param string $mask
     * @param string $metaData
     * @param string $method
     */
    protected function addRoute(string $mask, string $metaData, string $method)
    {
        $this->router[] = new RestRoute(
            $this->linkPrefix . $mask,
            $metaData,
            $method,
            $this->getFlag()
        );
    }
}
