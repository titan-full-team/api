<?php

namespace Titan\Utils;

use Nextras\Orm\Collection\DbalCollection;
use Nextras\Orm\Collection\ICollection;
use Nextras\Orm\Entity\IEntity;
use Titan\Model\Helper;
use Titan\Model\Orm\ITitanCollection;
use Titan\Model\Orm\TitanCollection;

class JsonResponse implements \Nette\Application\Response
{
    private int $status = 200;

    private array $data = [];

    public function send(\Nette\Http\IRequest $httpRequest, \Nette\Http\IResponse $httpResponse): void
    {
        $httpResponse->setContentType('application/json', 'utf-8');
        echo \Nette\Utils\Json::encode($this->getData());
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status = 200): void
    {
        $this->status = $status;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function add(string $key, mixed $value): void
    {
        $this->data[$key] = $value;
    }

    public function array(array $data): void
    {
        $this->data = [...$this->data, ...$data];
    }

    public function message(string $message): void
    {
        $this->data['message'] = $message;
    }

    public function reset(): void
    {
        $this->data = [];
    }

    public function collection(
        ICollection|ITitanCollection|DbalCollection|TitanCollection $collection,
        array|string                                                $method = 'toMain',
        string                                                      $key = 'items'
    ): void
    {
        $this->data[$key] = Helper::collectionToArray($collection, $method);
    }

    public function paginatedCollection(
        ICollection|ITitanCollection $collection,
        int                          $page = 1,
        int                          $perPage = 10,
        array|string                 $method = 'toMain',
    ): void
    {
        $paginator = new Paginator($page, $perPage, $collection->countStored());
        $data = $collection->limitBy($paginator->getLimit(), $paginator->getOffset());
        $this->data['paginator'] = $paginator->getPaginatorData();
        $this->data['items'] = Helper::collectionToArray($data, $method);
    }

    public function entity(?IEntity $entity, array|string $method = 'toMain', string $key = 'item'): void
    {
        $this->data[$key] = $entity === null ? null : ApiHelper::entityToArray($entity, $method);
    }

}