<?php

namespace Titan\Trait;

use Nette\Application\BadRequestException;
use Titan\Services\ConfigService;
use Titan\Utils\Dioneci;

trait DioneciTrait
{
    protected Dioneci $dioneci;

    /** @var ConfigService */
    public ConfigService $configService;

    /**
     * @throws \Exception
     */
    public function startup()
    {
        parent::startup();
        $this->dioneci = new Dioneci($this->getDioneciConfigParameter('authKey'));
        $this->dioneci->auth($this->getAuthorization());
    }

    protected function getLogFolder(): string
    {
        return $this->configService->get('appDir') . '/../log/';
    }

    /**
     * @throws BadRequestException
     */
    protected function getAuthorization(): string
    {
        $auth = $this->getHttpRequest()->getHeader('Authorization');
        if (is_null($auth) && !isset($_SERVER['Authorization'])) {
            throw new BadRequestException('Unauthorized, Authorization code is missing', 401);
        }
        return is_null($auth) ? $_SERVER['Authorization'] : $auth;
    }

    protected function getDioneciConfigParameter(string $parameter, $default = NULL): mixed
    {
        $dioneci = $this->configService->get('dioneci', []);
        return $dioneci[$parameter] ?? $default;
    }
}