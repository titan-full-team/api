<?php

namespace Titan\Services;

use App\Model\User\User;
use Lindelius\JWT\Exception\JwtException;
use Lindelius\JWT\Exception\UnsupportedAlgorithmException;
use Lindelius\JWT\JWT;
use Nette\Application\BadRequestException;
use Nextras\Dbal\UniqueConstraintViolationException;
use Nextras\Dbal\Utils\DateTimeImmutable;
use Nextras\Orm\Collection\ICollection;
use Nextras\Orm\Entity\IEntity;
use Nextras\Orm\Repository\IRepository;
use Titan\Utils\auth\Auth;
use Titan\Utils\auth\IAuth;

class AccountService
{
    public const USER_DEFAULT_AUTH_CLASS = '\Titan\Utils\auth\Credentials';

    protected string $userTokenEntityClass = 'App\Model\User\Token\UserToken';

    protected ?IEntity $user = null;

    protected bool $validateCsrf = true;

    /**
     * @throws \Exception
     */
    public function __construct(
        protected IRepository $userModel,
        protected IRepository $userTokenModel,
        protected IAuthService $authService,
        protected string      $jwtSecret,
        protected string      $jwtName = 'jwt',
        bool                  $debugMode = false
    )
    {
        $this->validateCsrf = $debugMode === false;

        if (empty($jwtSecret)) {
            throw new \Exception('Setup jwt secret', 500);
        }
    }

    /**
     * @throws BadRequestException
     */
    public function checkLogin(): void
    {
        if ($this->isLoggedIn() === false) {
            throw new BadRequestException('You are not logged in', 401);
        }
    }

    public function isLoggedIn(): bool
    {
        try {
            $this->getAccount();
            return true;
        } catch (BadRequestException $e) {
            return false;
        }
    }

    /**
     * @throws BadRequestException
     */
    public function get(): IEntity|User|null
    {
        return $this->getAccount(false);
    }

    /**
     * @throws BadRequestException
     */
    public function getAccount(bool $forceLoad = false): IEntity|User|null
    {
        if ($forceLoad === true || is_null($this->user)) {
            $this->validateToken();

            $this->user = $this->userModel->findBy([
                'userToken->type' => Auth::LOGIN_TOKEN,
                'userToken->token' => $this->getJWTToken($this->jwtName),
            ])->findBy([
                ICollection:: OR,
                'userToken->expiration>=' => new DateTimeImmutable('now'),
                'userToken->expiration' => null,
            ])->limitBy(1)
                ->fetch();

            if (is_null($this->user)) {
                throw new BadRequestException('Account not exists or login token expired', 401);
            }

            $this->authService->setUser($this->user);
        }

        return $this->user;
    }

    /**
     * @throws BadRequestException
     */
    protected function validateToken(): JWT
    {
        $token = $this->getJWTToken($this->jwtName);
        if (is_null($token)) {
            throw new BadRequestException('Token is not available, log-in', 403);
        }

        try {
            $decodedJwt = \Titan\Utils\auth\JWT::decode($token);
            if ($decodedJwt->verify($this->jwtSecret) === false) {
                throw new BadRequestException('JWT token is invalid', 401);
            }
        } catch (\Throwable $e) {
            throw new BadRequestException('JWT token is broken', 401);
        }

        if ($this->validateCsrf === true && $decodedJwt->sub->csrf !== $this->getCSRFToken()) {
            throw new BadRequestException('CSRF token is invalid', 401);
        }

        return $decodedJwt;
    }

    /**
     * @throws BadRequestException
     */
    public function getCSRFToken(): ?string
    {
        if (!isset($_SERVER['HTTP_AUTHORIZATION']) && !isset($_SERVER['Authorization'])) {
            throw new BadRequestException('CSRF token not found, maybe cookie is missing', 400);
        }
        $auth = $_SERVER['HTTP_AUTHORIZATION'] ?? null;
        if (is_null($auth)) {
            $auth = $_SERVER['Authorization'];
        }
        if (empty($auth)) {
            throw new BadRequestException('CSRF token is empty or null.', 400);
        }
        return $auth;
    }

    public function setCsrf(string $token): void
    {
        $_SERVER['Authorization'] = $token;
        $_SERVER['HTTP_AUTHORIZATION'] = $token;
    }

    public static function getJWTToken(string $jwtName): ?string
    {
        if (isset($_COOKIE[$jwtName]) && !empty($_COOKIE[$jwtName])) {
            return $_COOKIE[$jwtName];
        }
        // todo: from request headers
        return null;
    }

    public function setJWTToken(string $token): void
    {
        $_COOKIE[$this->jwtName] = $token;
    }

    public function login(
        array   $data,
        string  $class = self::USER_DEFAULT_AUTH_CLASS,
        ?string $tokenExpiration = '+1 day'
    ): string
    {
        $csrf = $this->getAuthClass($class)->login($data, $tokenExpiration);
        $_SERVER['HTTP_AUTHORIZATION'] = $csrf;
        return $csrf;
    }


    /**
     * @throws BadRequestException
     * @throws JwtException
     * @throws UnsupportedAlgorithmException
     */
    public function loginWithoutValidation(int $userId): string
    {
        /** @var \App\Model\User\User|IEntity $account */
        $account = $this->userModel->getByIdWithValidation($userId);
        $this->authService->setUser($account);
        $csrf = $this->getAuthClass(self::USER_DEFAULT_AUTH_CLASS)->setJWT($account, self::USER_DEFAULT_AUTH_CLASS);
        $_SERVER['HTTP_AUTHORIZATION'] = $csrf;
        return $csrf;
    }


    /**
     * @throws BadRequestException
     */
    public function register(
        array   $data,
        bool    $autoLogin = true,
        string  $class = self::USER_DEFAULT_AUTH_CLASS,
        ?string $tokenExpiration = '+1 day'
    ): bool
    {
        try {
            /** @var \App\Model\User\User $userEntity */
            $userEntity = $this->userModel->patchNewEntity($data);
            $this->userModel->persist($userEntity);
            $created = $this->getAuthClass($class)->register($userEntity, $data, $autoLogin, $tokenExpiration);
        } catch (UniqueConstraintViolationException $exception) {
            throw new BadRequestException('Account already exists', 409);
        }
        if ($autoLogin === true) {
            $this->login($data, $class);
        }
        return $created;
    }

    public function getAuthClass(string $class): IAuth
    {
        return new $class(
            $this->userModel,
            $this->userTokenModel,
            $this->userTokenEntityClass,
            $this->jwtSecret,
            $this->jwtName
        );
    }

    /**
     * @throws BadRequestException
     * @throws \Exception
     */
    public function update(array $data): IEntity
    {
        $user = $this->userModel->patchEntity($data, $this->getAccount());
        $this->userModel->persistAndFlush($user);
        return $user;
    }

    /**
     * @return string
     * @throws BadRequestException
     */
    protected function getClassFromToken(): string
    {
        $jwt = $this->validateToken();
        return $jwt->sub->authType;
    }

    /**
     * @throws BadRequestException
     */
    public function logout(): void
    {
        $this->authService->setUser(null);
        $this->getAuthClass($this->getClassFromToken())->logout();
    }

    /**
     * @throws BadRequestException
     */
    public function changeCredentials(array $data): string
    {
        return $this->getAuthClass($this->getClassFromToken())->changeCredentials($this->getAccount(), $data);
    }

    public function setCredentials(IEntity $account, string $token, string $authType): string
    {
        return $this->getAuthClass($authType)->setCredentials($account, false, $token);
    }

    public function setValidateCsrf(bool $validateCsrf): void
    {
        $this->validateCsrf = $validateCsrf;
    }

    /**
     * @warning decide if it's safe way for your auth - may cause CSRF and other potential security risks
     * @throws \Lindelius\JWT\Exception\InvalidJwtException|JwtException
     */
    public static function switchAuthorizationTokens(\Titan\Utils\User $user): bool
    {
        try {
            $jwt = $user->getCSRFToken();
        } catch (BadRequestException $exception) {
            return false;
        }

        $decodedJwt = JWT::decode($jwt);
        $user->setCsrf($decodedJwt->getPayload()['sub']->csrf);
        $user->setJWTToken($jwt);
        return true;
    }
}
