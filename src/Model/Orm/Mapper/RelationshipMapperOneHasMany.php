<?php declare(strict_types=1);

/**
 * This file is part of the Nextras\Orm library.
 * @license    MIT
 * @link       https://github.com/nextras/orm
 */

namespace Titan\Model\Orm\Mapper;

use Iterator;
use Nextras\Dbal\IConnection;
use Nextras\Dbal\QueryBuilder\QueryBuilder;
use Nextras\Orm\Collection\DbalCollection;
use Nextras\Orm\Collection\ICollection;
use Nextras\Orm\Collection\MultiEntityIterator;
use Nextras\Orm\Entity\IEntity;
use Nextras\Orm\Entity\IEntityHasPreloadContainer;
use Nextras\Orm\Entity\Reflection\PropertyMetadata;
use Nextras\Orm\Entity\Reflection\PropertyRelationshipMetadata;
use Nextras\Orm\Mapper\IRelationshipMapper;
use Titan\Model\Orm\ITitanCollection;
use Titan\Model\Orm\TitanCollection;


/**
 * Class RelationshipMapperOneHasMany
 * @extended because of sum and average function
 * @package Titan\Model\Orm\Mapper
 */
class RelationshipMapperOneHasMany extends \Nextras\Orm\Mapper\Dbal\RelationshipMapperOneHasMany implements IRelationshipMapper
{
    /**
     * @var
     */
    private $cacheSums;

    /**
     * @var
     */
    private $cacheAvgs;


    /**
     * @param IEntity $parent
     * @param TitanCollection $collection
     * @param string $column
     * @return int
     * @throws \Nextras\Dbal\QueryException
     */
    public function getIteratorSum(IEntity $parent, TitanCollection $collection, string $column)
    {
        assert($collection instanceof DbalCollection);
        $counts = $this->executeSums($collection, $parent, $column);
        $id = $parent->getValue('id');
        return $counts[$id] ?? 0;
    }

    /**
     * @phpstan-return array<int|string, int>
     * @param DbalCollection $collection
     * @param IEntity $parent
     * @param string $column
     * @return array
     * @throws \Nextras\Dbal\QueryException
     */
    protected function executeSums(DbalCollection $collection, IEntity $parent, string $column): array
    {
        $preloadContainer = $parent instanceof IEntityHasPreloadContainer ? $parent->getPreloadContainer() : null;
        $values = $preloadContainer !== null ? $preloadContainer->getPreloadValues('id') : [$parent->getValue('id')];
        $builder = $collection->getQueryBuilder();
        $cacheKey = $this->calculateCacheKey($builder, $values);
        /** @var array<int|string, int>|null $data */
        $data = &$this->cacheSums[$cacheKey];
        return $data !== null ? $data : $this->fetchSums($builder, $values, $column);
    }


    /**
     * @param QueryBuilder $builder
     * @param array $values
     * @param string $column
     * @return array
     * @throws \Nextras\Dbal\QueryException
     */
    private function fetchSums(QueryBuilder $builder, array $values, string $column)
    {
        $sourceTable = $builder->getFromAlias();

        $builder = clone $builder;
        $builder->select('%column', "{$sourceTable}.{$this->joinStorageKey}");

        if ($builder->hasLimitOffsetClause()) {
            $result = $this->processMultiCountResult($builder, $values);

        } else {
            $builder->orderBy(null);
            $builder->addSelect('SUM(%column) AS [sum]', "{$sourceTable}.{$column}");
            $builder->andWhere('%column IN %any', "{$sourceTable}.{$this->joinStorageKey}", $values);
            $builder->groupBy('%column', "{$sourceTable}.{$this->joinStorageKey}");
            $result = $this->connection->queryArgs($builder->getQuerySql(), $builder->getQueryParameters());
        }

        $counts = [];
        foreach ($result as $row) {
            $counts[$row->{$this->joinStorageKey}] = $row->sum;
        }
        return $counts;
    }


    /**
     * @param IEntity $parent
     * @param TitanCollection $collection
     * @param string $column
     * @return int
     * @throws \Nextras\Dbal\QueryException
     */
    public function getIteratorAvg(IEntity $parent, TitanCollection $collection, string $column)
    {
        assert($collection instanceof DbalCollection);
        $counts = $this->executeAvgs($collection, $parent, $column);
        $id = $parent->getValue('id');
        return $counts[$id] ?? 0;
    }


    /**
     * @param TitanCollection $collection
     * @param IEntity $parent
     * @param string $column
     * @return array|int|null
     * @throws \Nextras\Dbal\QueryException
     */
    protected function executeAvgs(TitanCollection $collection, IEntity $parent, string $column)
    {
        $preloadContainer = $parent instanceof IEntityHasPreloadContainer ? $parent->getPreloadContainer() : null;
        $values = $preloadContainer ? $preloadContainer->getPreloadValues('id') : [$parent->getValue('id')];
        $builder = $collection->getQueryBuilder();

        $cacheKey = $this->calculateCacheKey($builder, $values);
        /** @var int|null $data */
        $data = &$this->cacheAvgs[$cacheKey];

        if ($data !== null) {
            return $data;
        }

        return $this->fetchAvgs($collection->getQueryBuilder(), $values, $column);
    }

    /**
     * @param QueryBuilder $builder
     * @param array $values
     * @return array
     * @throws \Nextras\Dbal\QueryException
     */
    private function fetchAvgs(QueryBuilder $builder, array $values, string $column)
    {
        $sourceTable = $builder->getFromAlias();

        $builder = clone $builder;
        $builder->select('%column', "{$sourceTable}.{$this->joinStorageKey}");

        if ($builder->hasLimitOffsetClause()) {
            $result = $this->processMultiCountResult($builder, $values);

        } else {
            $builder->orderBy(null);
            $builder->addSelect('AVG(%column) AS [avg]', "{$sourceTable}.{$column}");
            $builder->andWhere('%column IN %any', "{$sourceTable}.{$this->joinStorageKey}", $values);
            $builder->groupBy('%column', "{$sourceTable}.{$this->joinStorageKey}");
            $result = $this->connection->queryArgs($builder->getQuerySql(), $builder->getQueryParameters());
        }

        $counts = [];
        foreach ($result as $row) {
            $counts[$row->{$this->joinStorageKey}] = $row->avg;
        }
        return $counts;


    }
}
