<?php

namespace Titan\Utils\Input;

use Nette\Application\BadRequestException;

trait InputTrait
{
    protected mixed $name;

    protected mixed $value;

    protected bool $hasValue = false;

    protected mixed $default;

    protected string $type = 'string';

    protected function __construct()
    {
    }

    /**
     * @throws BadRequestException
     */
    public static function fromArray(string $name, array $data, mixed $default = null): self
    {
        $instance = new self();
        $instance->name = $name;
        $instance->default = $default;
        $instance->setValue($instance, $name, $data);

        return $instance;
    }

    /**
     * @throws BadRequestException
     */
    protected function setValue(IInput $instance, string $name, array $data): void
    {
        if (array_key_exists($name, $data)) {
            $instance->value = $data[$name];
            $instance->hasValue = true;
            $instance->validateType();
        }
    }

    public function get()
    {
        return $this->hasValue === true ? $this->value : $this->default;
    }

    /**
     * @throws BadRequestException
     */
    public function required(): self
    {
        if ($this->hasValue === false) {
            $this->throwException('is required');
        }

        return $this;
    }

    /**
     * @throws BadRequestException
     */
    public function onlyOptions(array $options, bool $strict = true): self
    {
        if ($this->hasValue === true && in_array($this->get(), $options, $strict) === false) {
            $this->throwException('is invalid. Valid options are: ' . implode(',', $options));
        }
        return $this;
    }

    /**
     * @throws BadRequestException
     */
    public function notNull(): self
    {
        if ($this->get() === null) {
            $this->throwException('cannot be null');
        }
        return $this;
    }

    /**
     * @throws BadRequestException
     */
    protected function validateType(): void
    {
        if ($this->hasValue === false) {
            return;
        }

        if ($this->value === null) {
            return;
        }

        if (in_array($this->type, ['datetime', 'string']) && is_string($this->value) === false) {
            $this->throwException('has incorrect date type, only ' . $this->type . ' is allowed');
        } else if ($this->type === 'integer' && is_int($this->value) === false) {
            $this->throwException('has incorrect date type, only integer is allowed');
        } else if ($this->type === 'float' && is_float($this->value) === false) {
            $this->throwException('has incorrect date type, only float is allowed');
        } else if ($this->type === 'boolean' && is_bool($this->value) === false) {
            $this->throwException('has incorrect date type, only boolean is allowed');
        } else if ($this->type === 'array' && is_array($this->value) === false) {
            $this->throwException('has incorrect date type, only array is allowed');
        }
    }

    /**
     * @throws BadRequestException
     */
    protected function throwException(string $exception)
    {
        throw new BadRequestException('Parameter ' . $this->name . ' ' . $exception, 400);
    }
}