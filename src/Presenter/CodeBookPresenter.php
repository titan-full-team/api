<?php

namespace Titan\Presenter;

use Nette\Application\BadRequestException;
use Titan\Utils\CodeBook;

/** @deprecated */
class CodeBookPresenter extends CorePresenter
{
    /**
     * @param string $codeBooks
     * @throws BadRequestException
     * @throws \Exception
     * @out-200 []
     * @methods GET
     */
    public function actionList(string $codeBooks)
    {
        $codeBooks = explode(',', $codeBooks);
        $codeBook = new CodeBook($this->model);
        $out = $codeBook->getCodeBooks($codeBooks, $this->allowedCodeBook());
        $this->sendJson($out);
    }


    /**
     * @return array
     */
    protected function allowedCodeBook(): array
    {
        return [];
    }
}
