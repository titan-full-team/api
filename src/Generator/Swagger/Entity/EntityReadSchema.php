<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 04/11/2018
 * Time: 11:58 AM
 */

namespace Titan\Generator\Swagger\Entity;

use Nette\Application\BadRequestException;
use Nextras\Orm\Entity\Reflection\PropertyMetadata;
use Nextras\Orm\Entity\Reflection\PropertyRelationshipMetadata;
use Nextras\Orm\Model\IModel;
use PHP_CodeSniffer\Tokenizers\PHP;
use Titan\Generator\ClassType;
use Titan\Model\BaseEntity;
use Titan\Utils\Strings;

class EntityReadSchema extends EntitySchema implements IEntitySchema
{
    protected string $entity;

    protected BaseEntity $entityClass;

    protected string $method;

    protected bool $generated = false;

    protected array $data = [];

    protected array $dependencies = [];

    /**
     * @throws \Exception
     */
    public function __construct(string $entity, string $method)
    {
        if (empty($entity)) {
            throw new \Exception('Entity cannot be empty', 500);
        }
        $this->entity = $entity;
        $this->method = $method;

        $this->validateEntity();
        $this->entityClass = new $this->entity();
    }

    private function validateEntity()
    {
        if (class_exists($this->entity) === false) {
            throw new \InvalidArgumentException('Class ' . $this->entity . ' not found in ' . $this->foundInPresenter);
        }
    }

    /**
     * @throws \Exception
     */
    public function generate(IModel $model): void
    {
        $method = $this->getMethod($this->entity);
        $methodBody = $method->getBody();
        $methodAnnotation = $method->getComment();

        # Reset object type
        $this->data['type'] = 'object';
        $this->data['required'] = [];

        # Loop thru annoatitons
        $annotations = explode(PHP_EOL, $methodAnnotation);

        foreach ($annotations as $annotation) {
            $annotation = trim($annotation);

            if ($annotation === '@swagger-entity') {
                $this->toMainParameters($model);
            } else if (str_starts_with($annotation, '@swagger-add')) {
                $value = explode('::', trim(str_replace('@swagger-add', '', $annotation)));
                $this->addRefValue($model, $value[0], $value[1] ?? null);
            } else if (stristr($methodAnnotation, '@swagger-properties')) {
                $value = trim(str_replace('@swagger-properties', '', $annotation));
                $this->generateFromAnnotation($value);
            }
        }

        $this->generated = true;
    }

    /**
     * @throws \Exception
     */
    public function getMethod(string $class): \Nette\PhpGenerator\Method
    {
        $class = ClassType::from($class);
        $methods = $class->getMethods();

        if (isset($methods[$this->method])) {
            return $class->getMethod($this->method);
        }

        if (is_array($class->getExtends())) {
            throw new \Exception('Cannot find method ' . $this->method . ' in ' . $this->entity, 500);
        }

        return $this->getMethod('\\' . $class->getExtends());
    }

    /**
     * @throws \Exception
     */
    protected function addRefValue(IModel $model, string $propertyName, ?string $method): void
    {
        $repository = $model->getRepositoryForEntity($this->entityClass);
        $notAllowedParameters = $this->entityClass->notAllowedParameters;

        $property = $repository->getEntityMetadata()->getProperty($propertyName);

        if ($property->relationship !== null && $method !== null) {
            $dependencyEntity = new EntityReadSchema('\\' . $property->relationship->entity, $method);
            $definition = '#/components/schemas/' . $dependencyEntity->getEntityName();
            $arrayValues = [PropertyRelationshipMetadata::ONE_HAS_MANY, PropertyRelationshipMetadata::MANY_HAS_MANY];

            if (in_array($property->relationship->type, $arrayValues)) {
                $this->data['properties'][$propertyName] = [
                    'type' => 'array',
                    'items' => [
                        '$ref' => $definition
                    ],
                ];
            } else {
                $this->data['properties'][$propertyName]['$ref'] = $definition;
            }

            $this->data['required'][] = $propertyName;
            $this->dependencies[] = $dependencyEntity;
        } else {
            $this->addProperty($property);
        }
    }

    protected function toMainParameters(IModel $model): void
    {
        $repository = $model->getRepositoryForEntity($this->entityClass);
        $properties = $repository->getEntityMetadata()->getProperties();
        $notAllowedParameters = $this->entityClass->notAllowedParameters;

        foreach ($properties as $propertyName => $property) {
            $this->addProperty($property, $notAllowedParameters);
        }
    }

    protected function addProperty(PropertyMetadata $property): void
    {
        $propertyName = $property->name;

        if (is_null($property->relationship)
            && array_search($propertyName, $this->entityClass->notAllowedParameters) === false
        ) {
            $this->data['properties'][$propertyName] = Entity::getPropertyData($property);
            $this->data['required'][] = $propertyName;
        }
    }

    protected function generateFromAnnotation(string $value): void
    {
        $data = json_decode($value, true);

        if (is_array($data)) {
            $this->data['properties'] = array_merge($this->data['properties'], $data);
            $this->data['required'] = array_unique(array_keys($data));
            return;
        }

        throw new \InvalidArgumentException('Invalid @swagger-properties in ' . $this->entity . '::' . $this->method);
    }

    public function getDependencies(): array
    {
        return $this->dependencies;
    }

    public function isGenerated(): bool
    {
        return $this->generated;
    }

    public function getEntityName(): string
    {
        return Entity::getEntityName($this->entity, $this->method);
    }

    public function getIdentificator(): string
    {
        return Entity::getEntityIdentificator($this->entity, $this->method);
    }

    public function getEntityFullName(): string
    {
        return $this->entity;
    }

    public function getEntityReadSchema(): string
    {
        return $this->method;
    }

    public function getData(): array
    {
        return $this->data;
    }
}