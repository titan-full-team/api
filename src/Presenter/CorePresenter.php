<?php

namespace Titan\Presenter;

abstract class CorePresenter extends \Nette\Application\UI\Presenter
{
    use TitanPresenterTrait;
}