<?php

namespace Titan\Utils;

use App\Model\User\UserRepository;
use Lindelius\JWT\JWT;
use Nette\Application\BadRequestException;
use Nextras\Dbal\UniqueConstraintViolationException;
use Nextras\Dbal\Utils\DateTimeImmutable;
use Nextras\Orm\Collection\ICollection;
use Nextras\Orm\Entity\IEntity;
use Nextras\Orm\Repository\IRepository;
use Titan\Utils\auth\Auth;
use Titan\Utils\auth\IAuth;

class User
{
    CONST USER_DEFAULT_AUTH_CLASS = '\Titan\Utils\auth\Credentials';

    /**
     * @var \Nextras\Orm\Repository\Repository
     */
    protected $userModel;

    /**
     * @var \Nextras\Orm\Repository\Repository
     */
    protected $userTokenModel;

    /**
     * @var string
     */
    protected $userEntityClass = 'App\Model\User\User';

    /**
     * @var string
     */
    protected $userTokenEntityClass = 'App\Model\User\Token\UserToken';

    /**
     * @var string
     */
    protected $jwtSecret;

    /**
     * @var string
     */
    protected $jwtName;

    /**
     * @var bool
     */
    protected $validateCsrf;

    /**
     * @var IEntity
     */
    protected $user = null;

    /**
     * @param \Nextras\Orm\Repository\Repository|IRepository|UserRepository $userModel
     * @param IRepository $userTokenModel
     * @param string|null $jwtSecret
     * @param string $jwtName
     * @param bool $validateCsrf
     * @throws \Exception
     */
    public function __construct(
        IRepository $userModel,
        IRepository $userTokenModel,
        string $jwtSecret = null,
        string $jwtName = 'jwt',
        bool $validateCsrf = true
    )
    {
        $this->userModel = $userModel;
        $this->userTokenModel = $userTokenModel;
        $this->jwtSecret = $jwtSecret;
        $this->jwtName = $jwtName;
        $this->validateCsrf = $validateCsrf;

        if (empty($jwtSecret)) {
            throw new \Exception('Setup jwt secret', 500);
        }
    }

    /**
     * @return bool
     */
    public function isLoggedIn(): bool
    {
        try {
            $this->getAccount();
            return true;
        } catch (BadRequestException $e) {
            return false;
        }
    }

    /**
     * @return mixed|IEntity|null|\App\Model\User\User
     * @throws BadRequestException
     */
    public function getAccount(bool $forceLoad = false): IEntity
    {
        if ($forceLoad === true || is_null($this->user)) {
            $this->validateToken();

            $this->user = $this->userModel->findBy([
                'userToken->type' => Auth::LOGIN_TOKEN,
                'userToken->token' => $this->getJWTToken($this->jwtName),
            ])->findBy([
                ICollection:: OR,
                'userToken->expiration>=' => new DateTimeImmutable('now'),
                'userToken->expiration' => null,
            ])->limitBy(1)
                ->fetch();

            if (is_null($this->user)) {
                throw new BadRequestException('Account not exists or login token expired', 401);
            }
        }

        return $this->user;
    }

    /**
     * @throws BadRequestException
     */
    protected function validateToken(): JWT
    {
        $token = $this->getJWTToken($this->jwtName);
        if (is_null($token)) {
            throw new BadRequestException('Token is not available, log-in', 403);
        }

        try {
            $decodedJwt = \Titan\Utils\auth\JWT::decode($token);
            if ($decodedJwt->verify($this->jwtSecret) === false) {
                throw new BadRequestException('JWT token is invalid', 401);
            }
        } catch (\Throwable $e) {
            throw new BadRequestException('JWT token is broken', 401);
        }

        if ($this->validateCsrf === true && $decodedJwt->sub->csrf !== $this->getCSRFToken()) {
            throw new BadRequestException('CSRF token is invalid', 401);
        }

        return $decodedJwt;
    }

    /**
     * @return string|null
     * @throws BadRequestException
     */
    public function getCSRFToken()
    {
        if (!isset($_SERVER['HTTP_AUTHORIZATION']) && !isset($_SERVER['Authorization'])) {
            throw new BadRequestException('CSRF token not found, maybe cookie is missing', 400);
        }
        $auth = $_SERVER['HTTP_AUTHORIZATION'] ?? null;
        if (is_null($auth)) {
            $auth = $_SERVER['Authorization'];
        }
        if (empty($auth)) {
            throw new BadRequestException('CSRF token is empty or null.', 400);
        }
        return $auth;
    }


    /**
     * @param string $token
     */
    public function setCsrf(string $token)
    {
        $_SERVER['Authorization'] = $token;
        $_SERVER['HTTP_AUTHORIZATION'] = $token;
    }


    /**
     * @return null
     */
    public static function getJWTToken(string $jwtName):? string
    {
        if (isset($_COOKIE[$jwtName]) && !empty($_COOKIE[$jwtName])) {
            return $_COOKIE[$jwtName];
        }
        // todo: from request headers
        return null;
    }


    /**
     * @param string $token
     */
    public function setJWTToken(string $token)
    {
        $_COOKIE[$this->jwtName] = $token;
    }


    /**
     * @param array $data
     * @param string $class
     * @param string|null $tokenExpiration
     * @return string
     */
    public function login(
        array $data,
        string $class = self::USER_DEFAULT_AUTH_CLASS,
        $tokenExpiration = '+1 day'
    ): string
    {
        $csrf = $this->getAuthClass($class)->login($data, $tokenExpiration);
        $_SERVER['HTTP_AUTHORIZATION'] = $csrf;
        return $csrf;
    }


    /**
     * @param int $userId
     * @return string
     * @throws BadRequestException
     */
    public function loginWithoutValidation(int $userId): string
    {
        /** @var \App\Model\User\User $account */
        $account = $this->userModel->getByIdWithValidation($userId);
        $csrf = $this->getAuthClass(self::USER_DEFAULT_AUTH_CLASS)->setJWT($account, self::USER_DEFAULT_AUTH_CLASS);
        $_SERVER['HTTP_AUTHORIZATION'] = $csrf;
        return $csrf;
    }


    /**
     * todo:
     * @param array $data
     * @param bool $autoLogin
     * @param string $class
     * @param string $tokenExpiration
     * @return mixed|\Nextras\Orm\Entity\IEntity|null
     * @throws BadRequestException
     */
    public function register(
        array $data,
        bool $autoLogin = true,
        string $class = self::USER_DEFAULT_AUTH_CLASS,
        $tokenExpiration = '+1 day'
    ): bool
    {
        try {
            /** @var \App\Model\User\User $userEntity */
            $userEntity = $this->userModel->patchNewEntity($data);
            $this->userModel->persist($userEntity);
            $created = $this->getAuthClass($class)->register($userEntity, $data, $autoLogin, $tokenExpiration);
        } catch (UniqueConstraintViolationException $exception) {
            throw new BadRequestException('Account already exists', 409);
        }
        if ($autoLogin === true) {
            $this->login($data, $class);
        }
        return $created;
    }

    /**
     * @param string $class
     * @return IAuth
     */
    public function getAuthClass(string $class): IAuth
    {
        return new $class(
            $this->userModel,
            $this->userTokenModel,
            $this->userTokenEntityClass,
            $this->jwtSecret,
            $this->jwtName
        );
    }

    /**
     * todo:
     * @param array $data
     * @return mixed|\Nextras\Orm\Entity\IEntity|null
     * @throws BadRequestException
     * @throws \Exception
     */
    public function update(array $data): IEntity
    {
        $user = $this->userModel->patchEntity($data, $this->getAccount());
        $this->userModel->persistAndFlush($user);
        return $user;
    }

    /**
     * @return string
     * @throws BadRequestException
     */
    protected function getClassFromToken(): string
    {
        $jwt = $this->validateToken();
        return $jwt->sub->authType;
    }

    /**
     * @throws BadRequestException
     */
    public function logout()
    {
        $this->getAuthClass($this->getClassFromToken())->logout();
    }

    /**
     * @param array $data
     * @return string
     * @throws BadRequestException
     */
    public function changeCredentials(array $data): string
    {
        return $this->getAuthClass($this->getClassFromToken())->changeCredentials($this->getAccount(), $data);
    }

    /**
     * @param string $token
     * @param string $authType
     * @return string
     */
    public function setCredentials(IEntity $account, string $token, string $authType): string
    {
        return $this->getAuthClass($authType)->setCredentials($account, false, $token);
    }

    /**
     * @param string $userEntityClass
     */
    public function setUserEntityClass(string $userEntityClass)
    {
        $this->userEntityClass = $userEntityClass;
    }

    /**
     * @param string $userTokenEntityClass
     */
    public function setUserTokenEntityClass(string $userTokenEntityClass)
    {
        $this->userTokenEntityClass = $userTokenEntityClass;
    }

    /**
     * @param bool $validateCsrf
     */
    public function setValidateCsrf(bool $validateCsrf): void
    {
        $this->validateCsrf = $validateCsrf;
    }


    /**
     * @warning decide if it's safe way for your auth - may cause CSRF and other potential security risks
     * @param \Titan\Utils\User $user
     * @return bool
     * @throws \Lindelius\JWT\Exception\InvalidJwtException
     */
    public static function switchAuthorizationTokens(\Titan\Utils\User $user): bool
    {
        try {
            $jwt = $user->getCSRFToken();
        } catch (BadRequestException $exception) {
            return false;
        }

        $decodedJwt = JWT::decode($jwt);
        $user->setCsrf($decodedJwt->getPayload()['sub']->csrf);
        $user->setJWTToken($jwt);
        return true;
    }
}
