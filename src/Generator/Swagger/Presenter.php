<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 04/11/2018
 * Time: 6:49 PM
 */

namespace Titan\Generator\Swagger;

use Nette\PhpGenerator\Method;
use Nette\PhpGenerator\Parameter;
use Titan\Generator\Annotation;
use Titan\Generator\ClassType;
use Titan\Generator\Scanner;
use Titan\Generator\Swagger\Entity\Entity;
use Titan\Generator\Swagger\Entity\EntityReadSchema;
use Titan\Generator\Swagger\Entity\EntityWriteSchema;
use Titan\Router\Route;
use Titan\Utils\StatusCode;

class Presenter
{
    protected ClassType $class;

    protected array $classUses = [];

    public function __construct(string $file)
    {
        $this->class = $this->getClass($file);
    }

    public function isAbstract(): bool
    {
        return $this->class->isAbstract();
    }

    public function isDisabled(): bool
    {
        return !empty($this->class->getComment())
            ? stristr($this->class->getComment(), '@swagger-disabled')
            : false;
    }

    /**
     * @throws \Exception
     */
    public function generator(array $swaggerConfig): array
    {
        $out = [];
        /** @var Method $method */
        foreach ($this->getMethods() as $method) {
            $comment = $method->getComment();
            if (Annotation::getValue($comment, 'ignore-swagger', false) === false) {
                $comments = explode(PHP_EOL, $comment);
                $routeName = $this->getRoute($method);
                $requestMethods = $this->getRequestMethods($this->getCommentValue($comments, 'methods', 'GET'));

                $security = new Security($method);

                $item = [
                    'tags' => [$this->getTagName()],
                    'description' => $this->getCommentValue($comments, 'description', ''),
                    'summary' => $this->getCommentValue($comments, 'summary', ''),
                    'consumes' => ['application/json'],
                    'produces' => ['application/json'],
                    'parameters' => $this->getParameters($method),
                ];

                $securityItems = $security->get();
                if (!empty($securityItems)) {
                    $item['security'] = array_merge($swaggerConfig['security'] ?? [], $securityItems);
                }

                foreach ($requestMethods as $requestMethod) {
                    $requestMethod = strtolower($requestMethod);
                    $out[$routeName][$requestMethod] = $item;
                    $out[$routeName][$requestMethod]['operationId'] = $method->getName() . '-' . $requestMethod;

                    if (in_array($requestMethod, ['post', 'put'])) {
                        $bodyParams = $this->getBodyParameters($method);
                        if (!empty($bodyParams)) {
                            $out[$routeName][$requestMethod]['parameters'][] = $bodyParams;
                        }
                    }

                    $out[$routeName][$requestMethod]['responses'] = $this->getResponses($comments, $method);
                }
            }
        }

        return $out;
    }


    /**
     * @throws \Exception
     */
    public function getRequiredModels(): array
    {
        $out = [];
        /** @var Method $method */
        foreach ($this->getMethods() as $method) {
            # Output
            foreach (StatusCode::getAllStatusCodes() as $code) {
                $statusText = Annotation::getValue($method->getComment(), 'out-' . $code);
                $entityMethod = $this->getEndpointsEntityOutput($statusText);
                $out = $this->addEntityToRequiredModels($out, $entityMethod);
            }

            # input
            foreach (['in-body', 'in-query'] as $inputType) {
                $className = Annotation::getValue($method->getComment(), $inputType);

                if ($className !== null) {
                    $entityMethod = new EntityWriteSchema($className, $inputType);
                    $out = $this->addEntityToRequiredModels($out, $entityMethod);
                }
            }
        }
        return $out;
    }

    /**
     * @return array
     */
    private function addEntityToRequiredModels(array $data, $entityMethod): array
    {
        if (!is_null($entityMethod)) {
            if ($entityMethod instanceof EntityWriteSchema || $entityMethod instanceof EntityReadSchema) {
                $entityMethod->foundInPresenter = $this->class->getName();
            }
            $data[$entityMethod->getIdentificator()] = $entityMethod;
        }
        return $data;
    }

    /**
     * @throws \Exception
     */
    protected function getResponses(array $comments, Method $method): array
    {
        $out = [];
        foreach (StatusCode::getAllStatusCodes() as $code) {
            $response = $this->getResponse($comments, $method, $code);
            if (!is_null($response)) {
                $out[$code] = $response;
            }
        }

        if (empty($out)) {
            throw new \Exception('Add `@out-200` response ' . $this->class->getName() . '::' . $method->getName(), 501);
        }

        return $out;
    }

    /**
     * @throws \Exception
     */
    protected function getResponse(array $comments, Method $method, int $statusCode): ?array
    {
        $outImage = $this->getCommentValue($comments, 'out-image');
        $outMessageDefinition = $this->getCommentValue($comments, 'out-message-' . $statusCode);
        $outDefinition = $this->getCommentValue($comments, 'out-' . $statusCode);

        if (is_null($outDefinition) && is_null($outMessageDefinition) && is_null($outImage)) {
            return null;
        }

        // Not defined
        if (!is_null($outImage)) {
            if ($statusCode !== 200) {
                return null;
            }

            $out = [
                'type' => 'object',
                'description' => 'Image',
                'content' => [
                    'image/*' => [],
                ]
            ];

        } else if (!is_null($outMessageDefinition)) {
            $out = [
                'type' => 'object',
                'description' => $outMessageDefinition,
                'content' => [
                    'application/json' => [
                        'schema' => [
                            '$ref' => '#/components/schemas/' . Swagger::MESSAGE_MODEL_NAME
                        ]
                    ]
                ]
            ];

            // Json output (annotation)
        } elseif ((str_contains($outDefinition, '{') || str_contains($outDefinition, '[')) === true) {
            $out = json_decode($outDefinition, true);

            // Entity output
        } elseif (str_contains($outDefinition, '::')) {
            $entityMethod = $this->getEndpointsEntityOutput($outDefinition);
            $ref = '#/components/schemas/' . $entityMethod->getEntityName();

            if ($this->isPaginatedList($method) === true) {
                $out = [
                    'type' => 'object',
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'properties' => [
                                    'items' => [
                                        'type' => 'array',
                                        'items' => ['$ref' => $ref]
                                    ],
                                    'paginator' => [
                                        '$ref' => '#/components/schemas/' . Swagger::PAGINATOR_MODEL_NAME
                                    ]
                                ]
                            ]
                        ]
                    ]
                ];
            } else {
                $out = [
                    'type' => 'object',
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                '$ref' => $ref
                            ]
                        ]
                    ]
                ];
            }

            // String output
        } else if (!empty($outDefinition)) {
            $out['description'] = $outDefinition;
        }

        // Description by status code
        if (empty($out['description'])) {
            $out['description'] = StatusCode::getMessage($statusCode);
        }

        return $out;
    }

    protected function isPaginatedList(Method $method): bool
    {
        $comment = Annotation::getValue($method->getComment(), 'out-paginator');
        if (!is_null($comment)) {
            return ($comment === 'true' || $comment == 1);
        }
        $methodBody = Scanner::getMethodNonCommentedBody($method);
        return stristr($methodBody, '$this->sendPaginatedListJson(') !== false;
    }

    /**
     * @throws \Exception
     */
    protected function getEndpointsEntityOutput($output): ?EntityReadSchema
    {
        if ($output !== null && str_contains($output, '::')) {
            $explode = explode('::', $output);
            return new EntityReadSchema(Entity::getEntityWithNameSpace($this->classUses, $explode[0]), $explode[1]);
        }
        return null;
    }

    /**
     * @deprecated
     */
    protected function getEntityName(string $entity): string
    {
        return Entity::getEntityWithNameSpace($this->classUses, $entity);
    }

    protected function getTagName(): string
    {
        return substr($this->class->getName(), 0, strlen('presenter') * -1);
    }

    public function getTag(): array
    {
        $comment = $this->class->getComment();
        $comments = explode(PHP_EOL, $comment);

        return [
            'name' => $this->getTagName(),
            'description' => $this->getCommentValue($comments, 'description'),
            'externalDocs' => [
                'description' => $this->getCommentValue($comments, 'doc-description'),
                'url' => $this->getCommentValue($comments, 'doc-url'),
            ],
        ];
    }

    /**
     * @throws \Exception
     */
    protected function getParameters(Method $method): array
    {
        $out = [];
        $requiredParameters = $this->getRequiredMethodParameters($method->getParameters());

        // Link parameters
        foreach ($method->getParameters() as $name => $parameter) {
            $out[] = [
                'in' => 'path',
                'name' => $name,
                'required' => in_array($name, $requiredParameters),
                'type' => Entity::getPropertyType([$parameter->getType() => '']),
            ];
        }

        return $out;
    }

    protected function getCustomBodyParameters(Method $method): ?array
    {
        return $this->decodeJsonMethodParameter($method, '@swagger-body');
    }

    protected function getCustomRequiredBodyParameters(Method $method): ?array
    {
        return $this->decodeJsonMethodParameter($method, '@swagger-body-required', []);
    }

    protected function decodeJsonMethodParameter(Method $method, string $bodyParameter, array $default = null): ?array
    {
        if (stristr($method->getComment(), $bodyParameter)) {
            $params = substr(
                $method->getComment(),
                strpos($method->getComment(), $bodyParameter) + strlen($bodyParameter)
            );
            $params = trim(substr($params, 0, strpos($params, PHP_EOL)));
            return json_decode($params, true);
        }
        return $default;
    }

    /**
     * @throws \Exception
     */
    protected function getBodyParameters(Method $method): array
    {
        $customParameters = $this->getCustomBodyParameters($method);

        if (!is_null($customParameters)) {
            return [
                'in' => 'body',
                'name' => 'body',
                'required' => true,
                'schema' => [
                    'type' => 'object',
                    'required' => $this->getCustomRequiredBodyParameters($method),
                    'properties' => $customParameters
                ]
            ];
        }

        $entity = Entity::getEndpointsEntityInput($method);

        if (!is_null($entity)) {
            return [
                'in' => 'body',
                'name' => 'body',
                'required' => true,
                'schema' => [
                    '$ref' => '#/components/schemas/' . $entity->getEntityName()
                ]
            ];
        }
        return [];
    }

    protected function getRequiredMethodParameters(array $parameters): array
    {
        $required = [];
        $optional = [];

        /** @var Parameter $parameter */
        foreach ($parameters as $parameter) {
            if ($parameter->hasDefaultValue()) {
                $optional[] = $parameter->getName();
            } else if (!empty($optional)) {
                $required = array_merge($required, $optional);
                $required[] = $parameter->getName();
                $optional = [];
            } else {
                $required[] = $parameter->getName();
            }
        }
        return $required;
    }

    protected function getRequestMethods(string $method): array
    {
        return explode(',', $method);
    }

    protected function getCommentValue(array $comments, string $type, $default = null)
    {
        $type = '@' . $type;
        $typeSize = strlen($type);
        foreach ($comments as $comment) {
            if (substr($comment, 0, $typeSize) === $type) {
                return trim(substr($comment, $typeSize));
            }
        }
        return $default;
    }

    protected function getRoute(Method $method): string
    {
        $route = new Route($method, $this->class->getName());
        $mask = $route->getMask();
        $mask = str_replace(['[', ']'], '', $mask);
        $mask = str_replace(['<', '>'], ['{', '}'], $mask);

        if (stristr($mask, '=') !== false) {
            $exploded = explode('/', $mask);

            foreach ($exploded as $i => $value) {
                $pos = strpos($value, '=');
                if ($pos !== false) {
                    $exploded[$i] = substr($value, 0, $pos) . '}';
                }
            }

            $mask = implode('/', $exploded);
        }

        return str_starts_with($mask, '/') ? $mask : '/' . $mask;
    }

    protected function getMethods(): array
    {
        $out = [];
        $extendedClass = ClassType::from('\\' . $this->class->getExtends());
        $methods = array_merge($this->class->getMethods(), $extendedClass->getMethods());

        /**
         * @var string $methodName
         * @var Method $method
         */
        foreach ($methods as $methodName => $method) {
            if (str_starts_with($methodName, 'action') && $method->getVisibility() === 'public') {
                $out[$methodName] = $method;
            }
        }
        return $out;
    }

    protected function getClass(string $file): ClassType
    {
        $fileData = Scanner::getSourceCode($file);

        preg_match('#namespace (.*);#', $fileData, $matches);
        $namespace = $matches[1];

        preg_match('#class ([a-zA-Z0-9]*)#', $fileData, $matches);
        $className = $matches[1];

        $fullClassName = '\\' . $namespace . '\\' . $className;
        $classType = ClassType::from($fullClassName);

        $this->classUses = Scanner::getUsesOfClass($fullClassName);

        return $classType;
    }
}
