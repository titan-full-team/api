<?php

namespace Titan\Model;

use DateTimeZone;
use Exception;
use mysqli;
use Nextras\Dbal\Drivers\Exception\ConnectionException;
use Nextras\Dbal\Drivers\IDriver;
use Nextras\Dbal\Exception\InvalidArgumentException;
use Nextras\Dbal\ILogger;
use Nextras\Dbal\Utils\StrictObjectTrait;


class MysqliDriver extends \Nextras\Dbal\Drivers\Mysqli\MysqliDriver implements IDriver
{
    use StrictObjectTrait;


    /** @var mysqli|null */
    private $connection;

    /** @var DateTimeZone */
    private $connectionTz;

    /** @var ILogger */
    private $logger;

    /** @var float */
    private $timeTaken = 0.0;

    /**
     * @var array
     */
    private $hosts = null;

    /**
     * @var array
     */
    private $attempts = [];


    /**
     * @var int
     */
    private $maxAttempts = 2;

    /**
     * @param array $params
     * @param ILogger $logger
     * @throws Exception
     */
    public function connect(array $params, ILogger $logger): void
    {
        if (is_null($this->hosts)) {
            $this->hosts = $params['host'] ?? [ini_get('mysqli.default_host')];
        }

        $this->maxAttempts = (int)($params['maxAttempts'] ?? 2);

        try {
            $params['host'] = $this->getHost();
            parent::connect($params, $logger);
        } catch (ConnectionException $exception) {
            $this->connect($params, $logger);
        }
    }


    /**
     * @return string
     */
    protected function getHost(): string
    {
        if (empty($this->hosts)) {
            throw new InvalidArgumentException('Database hosts are not available');
        }
        foreach ($this->hosts as $host) {
            if (isset($this->attempts[$host]) && $this->attempts[$host] <= $this->maxAttempts) {
                $this->attempts[$host] += 1;
                return $host;
            } elseif (!isset($this->attempts[$host])) {
                $this->attempts[$host] = 1;
                return $host;
            }
        }
        throw new InvalidArgumentException('Connection error');
    }
}
