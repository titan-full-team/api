<?php
/**
 * Created by PhpStorm.
 * User: norbertspanik
 * Date: 13/03/2018
 * Time: 16:31
 */

namespace Titan\Model;

use Nette\Application\BadRequestException;
use Nextras\Dbal\Utils\DateTimeImmutable;
use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Entity\IEntity;
use Nextras\Orm\Entity\Reflection\PropertyMetadata;
use Nextras\Orm\Entity\Reflection\PropertyRelationshipMetadata;
use Nextras\Orm\Model\IModel;
use Nextras\Orm\Model\Model;
use Nextras\Orm\Relationships\OneHasMany;
use Nextras\Orm\Repository\Repository;
use Titan\Presenter\CorePresenter;

class EntityGenerator
{
    public const LOCALE_KEY = '_locale';
    public const LOCALE_SETTING_KEY = '_localeSettings';

    /**
     * @var Model
     */
    protected $model;


    /**
     * @param Model|IModel $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }


    /**
     * @param IEntity $entity
     * @param array $parameters
     * @param array $notAllowedParameters
     * @return IEntity
     * @throws BadRequestException
     * @throws \Exception
     */
    public function patchEntity(IEntity $entity, array $parameters, array $notAllowedParameters = [])
    {
        //        $properties = Helper::getAllowedEntityToEditProperties($entity, $notAllowedParameters); // todo use this
        foreach ($entity->getMetadata()->getProperties() as $property) {
            $valueExist = array_key_exists($property->name, $parameters);
            $value = $valueExist === true ? $parameters[$property->name] : null;

            // Skip not allowed to write
            if ($property->isReadonly === true
                || ($valueExist === true && array_search($property->name, $notAllowedParameters) !== false)
            ) {
                continue;

                // Time like 10:22:32 OR 10:22
            } else if (isset($property->types['DateInterval']) && $valueExist === true) {
                $entity->{$property->name} = $this->getTimeFromString($value);

                // Date time OR date
            } else if (isset($property->types['Nextras\Dbal\Utils\DateTimeImmutable']) && $valueExist === true) {
                $newValue = is_null($value) ? null : new DateTimeImmutable($value);
                $newValueCompare = is_null($newValue) ? null : $newValue->getTimestamp();
                $oldValueCompare = is_null($entity->getRawValue($property->name))
                    ? null
                    : $entity->{$property->name}->getTimestamp();
                if ($newValueCompare !== $oldValueCompare) {
                    $entity->{$property->name} = $newValue;
                }

                // Relation ship values like One to many / many to many and so on.
            } elseif (!is_null($property->relationship) && $valueExist === true) {
                if ($property->relationship->type === PropertyRelationshipMetadata::MANY_HAS_ONE) {
                    $entity->{$property->name} = self::setManyHasOneEntity(get_class($entity), $property, $value);
                } else if ($property->relationship->type === PropertyRelationshipMetadata::ONE_HAS_MANY) {
                    $issetNowAllowedParameter = isset($notAllowedParameters[$property->name])
                        && is_array($notAllowedParameters[$property->name]);
                    self::setOneHasManyEntity(
                        $property,
                        $entity,
                        $value,
                        $issetNowAllowedParameter ? $notAllowedParameters[$property->name] : []
                    );
                } else {
                    throw new \Exception('Relation ship of entity is not finished', 501);
                }

                // Values like string, boolean, float, int, ...
            } elseif ($valueExist === true) {
                if ($property->isNullable === true) {
                    if (is_bool($entity->{$property->name})) {
                        if ($entity->getRawValue($property->name) !== $value) {
                            $entity->{$property->name} = $value;
                        }
                    } else {
                        $newValue = is_string($value) && empty($value) ? NULL : $value;
                        if ($entity->getRawValue($property->name) !== $newValue) {
                            $entity->{$property->name} = $newValue;
                        }
                    }
                } else if (isset($property->types['string']) && strlen(trim($value)) === 0) {
                    $entity->{$property->name} = '';
                } else {
                    if ($entity->getRawValue($property->name) !== $value) {
                        $entity->{$property->name} = $value;
                    }
                }
            }
        }

        if (array_key_exists(self::LOCALE_KEY, $parameters) === true) {
            $localeSettingsKey = self::LOCALE_SETTING_KEY;
            $localeSettings = isset($parameters[$localeSettingsKey]) && is_array($parameters[$localeSettingsKey])
                ? $parameters[$localeSettingsKey]
                : [];
            $entity = $this->updateLocales($entity, $parameters[self::LOCALE_KEY], $localeSettings);
        }

        return $entity;
    }


    /**
     * @param IEntity $entity
     * @param array $locales
     * @return IEntity
     */
    protected function updateLocales(IEntity $entity, array $locales, array $settings)
    {
        $properties = array_keys($entity->getMetadata()->getProperties());
        $languages = array_keys($locales);
        $deleteNotSetLanguages = array_key_exists('deleteNotSet', $settings) ? $settings['deleteNotSet'] : true;
        $updateMainLanguage = array_key_exists('updateMain', $settings) ? $settings['updateMain'] : true;
        $mainLocale = CorePresenter::getDefaultLanguage();

        foreach ($locales as $locale => $columns) {
            foreach ($columns as $column => $value) {
                $entityPropertyName = 'locale' . ucfirst($column);
                // Validate invalid property names
                if (in_array($entityPropertyName, $properties) === false || in_array($column, $properties) === false) {
                    throw new BadRequestException('Cannot find locale parameter: ' . $column, 400);
                }
                // Update locale entity
                $localeEntity = $this->getLanguageEntityIndex($entity, $entityPropertyName, $locale);
                // Create new locale entity
                if ($localeEntity === null) {
                    $entityClass = $entity->getMetadata()->getProperties()[$entityPropertyName]->relationship->entity;
                    /** @var IEntity $localeEntity */
                    $localeEntity = new $entityClass();
                    $localeEntity->lang = $locale;
                    $localeEntity->value = $value;
                    $entity->{$entityPropertyName}->add($localeEntity);
                }
                // Set value for entity
                if ($localeEntity->value !== $value) {
                    $localeEntity->value = $value;
                }
                // Delete not used entities
                if ($deleteNotSetLanguages === true && $entity->isAttached()) {
                    foreach ($entity->{$entityPropertyName} as $localeEntity) {
                        if (in_array($localeEntity->lang, $languages) === false) {
                            $this->model->remove($localeEntity);
                        }
                    }
                }
                // Update main language value
                if ($updateMainLanguage === true && $locale === $mainLocale) {
                    $entity->{$column} = $value;
                }
            }
        }
        return $entity;
    }


    /**
     * @param IEntity $entity
     * @param string $property
     * @param string $language
     * @return int|null
     */
    protected function getLanguageEntityIndex(IEntity $entity, string $property, string $language): ?IEntity
    {
        if ($entity->isAttached()) {
            foreach ($entity->{$property} as $index => $languageEntity) {
                if ($languageEntity->lang === $language) {
                    return $languageEntity;
                }
            }
        }
        return null;
    }


    /**
     * @param IEntity $entity
     * @param array $parameters
     * @param array $notAllowedParameters
     * @return IEntity
     * @throws BadRequestException
     * @throws \Exception
     * @deprecated
     */
    public function setEntityData(IEntity $entity, array $parameters, array $notAllowedParameters = []): IEntity
    {
        return self::patchEntity($entity, $parameters, $notAllowedParameters);
    }


    /**
     * @param PropertyMetadata $property
     * @param IEntity|BaseEntity $entity
     * @param array $newItems
     * @param array $notAllowedParameters
     * @throws BadRequestException
     * @throws \Exception
     */
    protected function setOneHasManyEntity(
        PropertyMetadata $property,
        IEntity $entity,
        array $newItems,
        array $notAllowedParameters = []
    )
    {
        /** @var OneHasMany $oldItems */
        $oldItems = $entity->{$property->name};

        /** @var Entity $oldItem */
        foreach ($oldItems as $oldItem) {
            $entityAddOnly = false;

            if (is_array($oldItem->id)) {
                $types = array_keys($property->types);
                $type = array_key_exists(0, $types) ? $types[0] : '';
                $typeParts = explode('\\', $type);
                $typesSize = sizeof($typeParts);

                if ($typesSize > 3) {
                    $translateIndex = $typesSize - 2;

                    if (array_key_exists($translateIndex, $typeParts) === true
                        && $typeParts[$translateIndex] === 'Translate'
                    ) {
                        $foundInNewItems = $this->getIndexOfLanguageItem($newItems, $oldItem->lang);
                        $entityAddOnly = in_array($property->name, $entity->entityAddOnly);
                    } else {
                        throw new BadRequestException('Unknow entity connection', 501);
                    }
                } else {
                    throw new BadRequestException('Unknow entity connection', 501);
                }
            } else {
                $foundInNewItems = $this->getIndexOfItem($newItems, $oldItem->id);
            }

            if (is_null($foundInNewItems)) {
                if ($entityAddOnly === false) {
                    $this->model->remove($oldItem, false);
                }
            } else {
                $this->setEntityData($oldItem, $newItems[$foundInNewItems]);
                unset($newItems[$foundInNewItems]);
            }
        }

        foreach ($newItems as $newItem) {
            $newEntity = $this->setEntityData(new $property->relationship->entity(), $newItem, $notAllowedParameters);
            $oldItems->add($newEntity);
        }
    }


    /**
     * @param array $newItems
     * @param string|null $language
     * @return int|null
     */
    protected function getIndexOfLanguageItem(array $newItems, string $language = null)
    {
        if (!is_null($language)) {
            foreach ($newItems as $key => $newItem) {
                if (isset($newItem['lang']) && $newItem['lang'] === $language) {
                    return $key;
                }
            }
        }
        return null;
    }


    /**
     * @param $newItems
     * @param $id
     * @return int|null
     */
    protected function getIndexOfItem(array $newItems, int $id)
    {
        foreach ($newItems as $key => $newItem) {
            if (isset($newItem['id']) && (int)$newItem['id'] === $id) {
                return $key;
            }
        }
        return null;
    }


    /**
     * @param string|null $value
     * @return \DateInterval|null
     * @throws BadRequestException
     */
    protected function getTimeFromString(string $value = null)
    {
        if (!is_null($value) && !empty($value)) {
            $values = explode(':', $value);
            if (sizeof($values) === 2 || sizeof($values) === 3) {
                foreach ($values as $key => $time) {
                    if ($key === 0) {
                        $values[$key] .= 'hours';
                    } else if ($key === 1) {
                        $values[$key] .= 'minutes';
                    } else if ($key === 2) {
                        $values[$key] .= 'seconds';
                    }
                }
                return \DateInterval::createFromDateString(implode(' ', $values));
            } else {
                throw new BadRequestException('Cannot resolve time string', 501);
            }
        }

        return null;
    }


    /**
     * @param PropertyMetadata $property
     * @param $value
     * @return null|IEntity
     * @throws BadRequestException
     */
    protected function setManyHasOneEntity(string $parentEntityName, PropertyMetadata $property, $value)
    {
        if (!is_null($value)) {
            /** @var Repository $repository */
            $repository = $this->model->getRepository($property->relationship->repository);
            $id = $this->getIdFromValue($value);
            $entity = is_null($id) ? null : $repository->getById($this->getIdFromValue($value));
            $entityName = $repository->getEntityClassName([]);
            $parentEntityShortName = substr($parentEntityName, strrpos($parentEntityName, '\\') + 1);

            if (in_array($parentEntityShortName, (new $entityName())->saveFromParent)) {
                // Save data
                return is_null($entity)
                    ? $repository->patchNewEntity($value)
                    : $repository->patchEntity($value, $entity);

            } else {
                // Update reference
                if (!is_null($entity)) {
                    return $entity;
                } else if ($property->isNullable === false) {
                    throw new BadRequestException($entityName . '::id ' . $value . ' not found', 404);
                }
            }
        }
        return null;
    }


    /**
     * @param array|int $value
     * @return int|null
     */
    protected function getIdFromValue($value):? int
    {
        if (is_array($value) && array_key_exists('id', $value)) {
            return $value['id'];
        } else if (is_int($value) || is_string($value)) {
            return (int)$value;
        }
        return null;
    }


    /**
     * todo: preverit
     * @param IEntity $originalEntity
     * @param array $disabled
     * @return Entity
     * @throws BadRequestException
     */
    public function copyEntity(IEntity $originalEntity, array $disabled = [])
    {
        $disabled[] = 'id';
        $entityClassName = get_class($originalEntity);
        $newEntity = new $entityClassName();
        foreach ($originalEntity->getMetadata()->getProperties() as $property) {
            $propertyName = $property->name;
            if (in_array($propertyName, $disabled) === false && $property->isReadonly === false) {
                if (is_null($property->relationship)) {
                    $newEntity->{$propertyName} = $originalEntity->{$propertyName};
                } elseif ($property->relationship->type === PropertyRelationshipMetadata::MANY_HAS_ONE) {
                    $newEntity->{$propertyName} = $originalEntity->{$propertyName};
                } elseif ($property->relationship->type === PropertyRelationshipMetadata::ONE_HAS_MANY) {
                    $relationShipDisabled = isset($disabled[$propertyName]) ? $disabled[$propertyName] : [];
                    foreach ($originalEntity->{$propertyName} as $relationShipEntity) {
                        $newEntity->{$propertyName}->add(
                            $this->copyEntity($relationShipEntity),
                            $relationShipDisabled
                        );
                    }
                } elseif ($property->relationship->type === PropertyRelationshipMetadata::MANY_HAS_MANY) {
                    foreach ($originalEntity->{$propertyName} as $relationShipEntity) {
                        $newEntity->{$propertyName}->add($relationShipEntity);
                    }
                } else {
                    throw new BadRequestException('Cannot copy ' . $propertyName . ' value', 501);
                }
            }
        }
        return $newEntity;
    }
}