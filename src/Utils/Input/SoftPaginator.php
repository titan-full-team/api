<?php

namespace Titan\Utils\Input;

use Nette\Application\BadRequestException;

final class SoftPaginator extends Input
{
	private int $page;

    private int $perPage;

	public static function create(int $page, int $perPage): self
	{
		$instance = new self();
		$instance->page = $page;
        $instance->perPage = $perPage;

		return $instance;
	}

    public static function unlimited(): self
    {
        return self::create(1, 100000);
    }

    public function getLimit(): int
    {
        return $this->perPage;
    }

    public function getOffset(): int
    {
        return ($this->page - 1) * $this->perPage;
    }

    public function getPaginator(array $data): array
    {
        return [
            'page' => $this->page,
            'perPage' => $this->perPage,
            'lastPage' => sizeof($data) < $this->perPage,
            'items' => null,
            'pages' => null,
        ];
    }
}
