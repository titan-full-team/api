<?php

namespace Titan\Utils\Input;

use Nette\Application\BadRequestException;

class InputString implements IInput
{
    use InputTrait;

    /**
     * @throws BadRequestException
     */
    public function minLength(int $length = 0): self
    {
        if ($this->hasValue === true && is_string($this->value) && strlen($this->value) < $length) {
            $this->throwException('has to be at least ' . $length . ' chars long');
        }
        return $this;
    }

    /**
     * @throws BadRequestException
     */
    public function maxLength(int $length = 255): self
    {
        if ($this->hasValue === true && is_string($this->value) && strlen($this->value) > $length) {
            $this->throwException('has too much chars. Maximum is ' . $length);
        }
        return $this;
    }

    public function trim(): self
    {
        if ($this->hasValue === true && is_string($this->value)) {
            $this->value = trim($this->value);
        }
        return $this;
    }

    public function removeWhiteSpace(): self
    {
        if ($this->hasValue === true && is_string($this->value)) {
            $this->value = str_replace(' ', '', $this->value);
        }
        return $this;
    }

    /**
     * @throws BadRequestException
     */
    public function notEmpty(): self
    {
        if (empty($this->get())) {
            $this->throwException('cannot be empty');
        }
        return $this;
    }
}