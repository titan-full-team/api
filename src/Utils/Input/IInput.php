<?php

namespace Titan\Utils\Input;

interface IInput
{
    public static function fromArray(string $name, array $data, mixed $default = null): self;

//    public function get();
//
//    public function required(): void;
//
//    public function notNull(): void;
}