<?php
/**
 * Created by PhpStorm.
 * User: wero
 * Date: 09/06/2019
 * Time: 2:17 PM
 */

namespace Titan\Errors;

use Nette\Application\BadRequestException;

class UserException extends BadRequestException
{
    /**
     * @var string
     */
    private $subMessage;

    /**
     * @param string $message
     * @param int $httpCode
     * @param \Exception|null $previous
     * @param string $subMessage
     */
    public function __construct($message = '', $httpCode = 0, \Exception $previous = null, string $subMessage = '')
    {
        parent::__construct($message, $httpCode ?: $this->code, $previous);
        $this->subMessage = $subMessage;
    }

    /**
     * @return string
     */
    public function getSubMessage(): string
    {
        return $this->subMessage;
    }
}